package com.dyj.common.config;

/**
 * @author danmo
 * @date 2024-04-02 14:02
 **/
public class AgentConfiguration {

    /**
     * 租户ID
     */
    private Integer tenantId;
    /**
     * 应用Key
     */
    private String clientKey;

    /**
     * 应用秘钥
     */
    private String clientSecret;

    /**
     * 三方应用回调ticket
     */
    private String ticket;

    /**
     * 三方应用授权码
     */
    private String authorization_code;

    /**
     * 认证类型
     */
    private String authorizationType = "SHA256-RSA2048";

    /**
     * 应用私钥路径
     */
    private String applicationPrivateKeyPath;

    /**
     * 应用公钥路径
     */
    private String applicationPublicKeyPath;

    /**
     * 应用公钥版本
     */
    private String applicationPublicKeyVersion;

    /**
     * 平台私钥路径
     */
    private String platformPrivateKeyPath;

    /**
     * 平台公钥路径
     */
    private String platformPublicKeyPath;


    public Integer getTenantId() {
        return tenantId;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    public String getClientKey() {
        return clientKey;
    }

    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getAuthorization_code() {
        return authorization_code;
    }

    public void setAuthorization_code(String authorization_code) {
        this.authorization_code = authorization_code;
    }

    public String getApplicationPrivateKeyPath() {
        return applicationPrivateKeyPath;
    }

    public AgentConfiguration setApplicationPrivateKeyPath(String applicationPrivateKeyPath) {
        this.applicationPrivateKeyPath = applicationPrivateKeyPath;
        return this;
    }

    public String getApplicationPublicKeyPath() {
        return applicationPublicKeyPath;
    }

    public AgentConfiguration setApplicationPublicKeyPath(String applicationPublicKeyPath) {
        this.applicationPublicKeyPath = applicationPublicKeyPath;
        return this;
    }

    public String getPlatformPrivateKeyPath() {
        return platformPrivateKeyPath;
    }

    public AgentConfiguration setPlatformPrivateKeyPath(String platformPrivateKeyPath) {
        this.platformPrivateKeyPath = platformPrivateKeyPath;
        return this;
    }

    public String getPlatformPublicKeyPath() {
        return platformPublicKeyPath;
    }

    public AgentConfiguration setPlatformPublicKeyPath(String platformPublicKeyPath) {
        this.platformPublicKeyPath = platformPublicKeyPath;
        return this;
    }

    public String getApplicationPublicKeyVersion() {
        return applicationPublicKeyVersion;
    }

    public AgentConfiguration setApplicationPublicKeyVersion(String applicationPublicKeyVersion) {
        this.applicationPublicKeyVersion = applicationPublicKeyVersion;
        return this;
    }

    public String getAuthorizationType() {
        return authorizationType;
    }

    public AgentConfiguration setAuthorizationType(String authorizationType) {
        this.authorizationType = authorizationType;
        return this;
    }
}
