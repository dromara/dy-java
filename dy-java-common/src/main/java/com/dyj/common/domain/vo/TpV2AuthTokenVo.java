package com.dyj.common.domain.vo;

import com.dyj.common.domain.AppAuthorizePermission;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-26 16:56
 **/
public class TpV2AuthTokenVo {
    /**
     * 授权小程序接口调用凭据
     */
    private String authorizer_access_token;

    /**
     * 刷新令牌，用于刷新已授权小程序的 authorizer_access_token
     */
    private String authorizer_refresh_token;

    /**
     * authorizer_access_token 的有效期，单位：秒
     */
    private Long expires_in;
    /**
     * authorizer_refresh_token 的有效期，单位：秒
     */
    private Long refresh_expires_in;
    /**
     * 授权小程序在授权跳转页勾选的权限
     */
    private List<AppAuthorizePermission> authorize_permission;

    /**
     * 授权小程序 appid
     */
    private String authorizer_appid;

    public String getAuthorizer_access_token() {
        return authorizer_access_token;
    }

    public void setAuthorizer_access_token(String authorizer_access_token) {
        this.authorizer_access_token = authorizer_access_token;
    }

    public String getAuthorizer_refresh_token() {
        return authorizer_refresh_token;
    }

    public void setAuthorizer_refresh_token(String authorizer_refresh_token) {
        this.authorizer_refresh_token = authorizer_refresh_token;
    }

    public Long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Long expires_in) {
        this.expires_in = expires_in;
    }

    public Long getRefresh_expires_in() {
        return refresh_expires_in;
    }

    public void setRefresh_expires_in(Long refresh_expires_in) {
        this.refresh_expires_in = refresh_expires_in;
    }

    public List<AppAuthorizePermission> getAuthorize_permission() {
        return authorize_permission;
    }

    public void setAuthorize_permission(List<AppAuthorizePermission> authorize_permission) {
        this.authorize_permission = authorize_permission;
    }

    public String getAuthorizer_appid() {
        return authorizer_appid;
    }

    public void setAuthorizer_appid(String authorizer_appid) {
        this.authorizer_appid = authorizer_appid;
    }
}
