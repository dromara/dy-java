package com.dyj.common.domain.query;

import com.alibaba.fastjson.annotation.JSONField;
import com.dyj.common.enums.SubscriptionAccessTokenTypeEnum;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 基本小程序订阅消息请求
 */
public class BaseSubscriptionQuery extends BaseQuery{

    /**
     * 订阅消息accessToken类型 默认为非服务商身份获取token
     */
    private SubscriptionAccessTokenTypeEnum accessTokenTypeEnum = SubscriptionAccessTokenTypeEnum.CLIENT_TOKEN;

    @JSONField(serialize = false)
    public SubscriptionAccessTokenTypeEnum getAccessTokenTypeEnum() {
        return accessTokenTypeEnum;
    }

    public BaseSubscriptionQuery setAccessTokenTypeEnum(SubscriptionAccessTokenTypeEnum accessTokenTypeEnum) {
        this.accessTokenTypeEnum = accessTokenTypeEnum;
        return this;
    }
}
