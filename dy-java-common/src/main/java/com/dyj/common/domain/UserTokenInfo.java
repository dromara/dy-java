package com.dyj.common.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author danmo
 * @date 2024-04-07 13:47
 **/
public class UserTokenInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String accessToken;

    private String refreshToken;

    private Long expiresIn;

    private LocalDateTime expireTime;

    private Long refreshExpiresIn;

    private LocalDateTime refreshExpireTime;

    private String openId;

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }

    public void setRefreshExpireTime(LocalDateTime refreshExpireTime) {
        this.refreshExpireTime = refreshExpireTime;
    }

    public LocalDateTime getRefreshExpireTime() {
        return refreshExpireTime;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public Long getRefreshExpiresIn() {
        return refreshExpiresIn;
    }

    public void setRefreshExpiresIn(Long refreshExpiresIn) {
        this.refreshExpiresIn = refreshExpiresIn;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}
