package com.dyj.common.domain.query;

import com.dyj.common.enums.SubscriptionAccessTokenTypeEnum;

import java.util.List;
import java.util.Map;

public class DeveloperNotifyQuery extends BaseSubscriptionQuery{

    /**
     * <p>模板内容</p>
     */
    private Map<String,String> data;
    /**
     * <p>消息模板的 id，来自<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/server/subscribe-notification/add-template" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">添加模板</a>接口</p>
     */
    private String msg_id;
    /**
     * <p>接收消息目标用户的 open_id，参考 <a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/server/log-in/code-2-session/" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">code2session</a></p>
     */
    private String open_id;
    /**
     * <p>小程序页面链接，由页面的query和path参数构成；目前暂不支持跳转外部链接</p> 选填
     */
    private String page;
    /**
     * <p>发送消息模式列表；</p><ul><li>枚举说明：<br><strong>1</strong> 表示仅发送实时提醒（抖音内横幅或抖音外实时提醒消息）； <br><strong>2</strong> 表示仅发送站内信；</li><li>传入参数说明：<br>[1] 仅发送实时提醒（如订阅模板不支持实时提醒或用户未订阅提醒通知，则不支持发送）；<br>[2] 如订阅模板不支持实时提醒或用户未订阅提醒通知，则不支持发送。仅发送站内信；<br>[1,2] 同时支持发送站内信和实时提醒消息（如订阅模板不支持实时提醒或用户未订阅提醒通知，则不支持发送）。</li></ul> 选填
     */
    private List<Integer> notify_type;

    public Map<String, String> getData() {
        return data;
    }

    public DeveloperNotifyQuery setData(Map<String, String> data) {
        this.data = data;
        return this;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public DeveloperNotifyQuery setMsg_id(String msg_id) {
        this.msg_id = msg_id;
        return this;
    }

    public String getOpen_id() {
        return open_id;
    }

    public DeveloperNotifyQuery setOpen_id(String open_id) {
        this.open_id = open_id;
        return this;
    }

    public String getPage() {
        return page;
    }

    public DeveloperNotifyQuery setPage(String page) {
        this.page = page;
        return this;
    }

    public List<Integer> getNotify_type() {
        return notify_type;
    }

    public DeveloperNotifyQuery setNotify_type(List<Integer> notify_type) {
        this.notify_type = notify_type;
        return this;
    }

    public static DeveloperNotifyQueryBuilder builder() {
        return new DeveloperNotifyQueryBuilder();
    }

    public static final class DeveloperNotifyQueryBuilder {
        private Map<String, String> data;
        private String msg_id;
        private String open_id;
        private String page;
        private List<Integer> notify_type;
        private SubscriptionAccessTokenTypeEnum accessTokenTypeEnum;
        private Integer tenantId;
        private String clientKey;

        private DeveloperNotifyQueryBuilder() {
        }


        public DeveloperNotifyQueryBuilder data(Map<String, String> data) {
            this.data = data;
            return this;
        }

        public DeveloperNotifyQueryBuilder msgId(String msgId) {
            this.msg_id = msgId;
            return this;
        }

        public DeveloperNotifyQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public DeveloperNotifyQueryBuilder page(String page) {
            this.page = page;
            return this;
        }

        public DeveloperNotifyQueryBuilder notifyType(List<Integer> notifyType) {
            this.notify_type = notifyType;
            return this;
        }

        public DeveloperNotifyQueryBuilder accessTokenTypeEnum(SubscriptionAccessTokenTypeEnum accessTokenTypeEnum) {
            this.accessTokenTypeEnum = accessTokenTypeEnum;
            return this;
        }

        public DeveloperNotifyQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public DeveloperNotifyQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public DeveloperNotifyQuery build() {
            DeveloperNotifyQuery developerNotifyQuery = new DeveloperNotifyQuery();
            developerNotifyQuery.setData(data);
            developerNotifyQuery.setMsg_id(msg_id);
            developerNotifyQuery.setOpen_id(open_id);
            developerNotifyQuery.setPage(page);
            developerNotifyQuery.setNotify_type(notify_type);
            developerNotifyQuery.setAccessTokenTypeEnum(accessTokenTypeEnum);
            developerNotifyQuery.setTenantId(tenantId);
            developerNotifyQuery.setClientKey(clientKey);
            return developerNotifyQuery;
        }
    }
}
