package com.dyj.common.domain;

public class TextAntiDirtPredicts {

    /**
     * 检测结果-置信度-服务/目标
     */
    private String target;

    /**
     * 检测结果-置信度-模型/标签
     */
    private String model_name;

    /**
     * 检测结果-置信度-概率，仅供参考，可以忽略
     */
    private Integer prob;

    /**
     * 检测结果-置信度-结果，当值为 true 时表示检测的文本包含违法违规内容
     */
    private Boolean hit;


    public String getTarget() {
        return target;
    }

    public TextAntiDirtPredicts setTarget(String target) {
        this.target = target;
        return this;
    }

    public String getModel_name() {
        return model_name;
    }

    public TextAntiDirtPredicts setModel_name(String model_name) {
        this.model_name = model_name;
        return this;
    }

    public Integer getProb() {
        return prob;
    }

    public TextAntiDirtPredicts setProb(Integer prob) {
        this.prob = prob;
        return this;
    }

    public Boolean getHit() {
        return hit;
    }

    public TextAntiDirtPredicts setHit(Boolean hit) {
        this.hit = hit;
        return this;
    }
}
