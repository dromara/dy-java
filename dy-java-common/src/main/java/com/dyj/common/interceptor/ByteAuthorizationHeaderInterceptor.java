package com.dyj.common.interceptor;

import com.dtflys.forest.http.ForestRequest;
import com.dtflys.forest.interceptor.Interceptor;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.config.DyConfiguration;
import com.dyj.common.domain.DyAppletKey;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.domain.query.UserInfoQuery;
import com.dyj.common.service.IAgentTokenService;
import com.dyj.common.utils.DecryptUtils;
import com.dyj.common.utils.DyConfigUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * 小程序 Byte-Authorization header参数
 */
@Component
public class ByteAuthorizationHeaderInterceptor implements Interceptor<Object> {

    private final Log log = LogFactory.getLog(ByteAuthorizationHeaderInterceptor.class);

    @Override
    public boolean beforeExecute(ForestRequest request) {
        Integer tenantId = null;
        String clientKey = "";
        Object[] arguments = request.getArguments();
        for (Object argument : arguments) {
            if(argument instanceof BaseQuery){
                BaseQuery query = (BaseQuery) argument;
                tenantId = query.getTenantId();
                clientKey = query.getClientKey();
            }
        }
        AgentConfiguration agentConfiguration = DyConfigUtils.getAgent(tenantId,clientKey);
        DyAppletKey dyAppletKey = DyConfigUtils.getDyAppletKey(tenantId,clientKey);
        if (dyAppletKey == null || dyAppletKey.getApplicationPrivateKey() == null) {
            return false;
        }

        String uri = request.getURI().getRawPath();
        String method = request.getType().getName();
        String nonceStr = UUID.randomUUID().toString().replaceAll("-","");
        String timestamp = System.currentTimeMillis()/1000+"";
        String body = request.getBody().encodeToString();
        if (body == null) {
            body = "";
        }

        String waitSign = method +
                "\n" +
                uri +
                "\n" +
                timestamp +
                "\n" +
                nonceStr +
                "\n" +
                body +
                "\n";
        String signature = DecryptUtils.sha256withRSASignature(waitSign,dyAppletKey.getApplicationPrivateKey());
        String authorization = agentConfiguration.getAuthorizationType() +
                " " +
                "appid=\"" +
                agentConfiguration.getClientKey() +
                "\"," +
                "nonce_str=\"" +
                nonceStr +
                "\"," +
                "timestamp=\"" +
                timestamp +
                "\"," +
                "key_version=\"" +
                agentConfiguration.getApplicationPublicKeyVersion() +
                "\"," +
                "signature=\"" +
                signature +
                "\"";
        request.addHeader("Byte-Authorization", authorization);
        return Interceptor.super.beforeExecute(request);
    }


}
