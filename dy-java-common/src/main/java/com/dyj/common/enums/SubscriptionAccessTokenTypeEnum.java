package com.dyj.common.enums;

/**
 * 订阅消息accessToken类型
 */
public enum SubscriptionAccessTokenTypeEnum {
    /**
     * 当服务商时，调用https://open.douyin.com/api/tpapp/v2/auth/get_auth_token/生成的token
     */
    TP_V2_AUTH_TOKEN,

    /**
     * 当非服务商时，调用https://open.douyin.com/oauth/client_token/生成的token
     */
    CLIENT_TOKEN;


    SubscriptionAccessTokenTypeEnum() {
    }



}
