package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.vo.AwemeVideoKeywordListVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.interceptor.ClientTokenInterceptor;

/**
 * 抖音开放能力
 * @author danmo
 * @date 2024-07-17 10:15
 **/
@BaseRequest(baseURL = "${domain}")
public interface AptOpenCapacityClient {

    /**
     * 查询视频关键词列表
     * 只有开通了 permission_key 为 open.video.search 权限的小程序才可以调用
     * @param query 应用信息
     * @param pageNum 分页编号，从 1 开始
     * @param pageSize 分页大小，小于等于 20
     * @return  DySimpleResult<AwemeVideoKeywordListVo>
     */
    @Get(value = "/api/apps/v1/capacity/query_aweme_video_keyword_list/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<AwemeVideoKeywordListVo> queryAwemeVideoKeywordList(@Var("query") BaseQuery query, @Query("page_num") Long pageNum, @Query("page_size") Long pageSize);

    /**
     * 添加视频关键词
     * @param query 应用信息
     * @param keyword 关键词
     * @param reason 添加原因
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v1/capacity/add_aweme_video_keyword/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> addAwemeVideoKeyword(@Var("query") BaseQuery query, @JSONBody("keyword") String keyword, @JSONBody("reason") String reason);

    /**
     * 删除视频关键词
     * @param query 应用信息
     * @param keywordId 需要删除的关键词id，必须是审核通过/拒绝的关键词
     * @return DySimpleResult<String>
     */
    @Post(value = "/api/apps/v1/capacity/delete_aweme_video_keyword/", interceptor = ClientTokenInterceptor.class)
    DySimpleResult<String> deleteAwemeVideoKeyword(@Var("query") BaseQuery query, @JSONBody("keyword_id") String keywordId);

}
