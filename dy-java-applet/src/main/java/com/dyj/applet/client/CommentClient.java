package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;

/**
 * 支付->评价
 */
@BaseRequest(baseURL = "${domain}")
public interface CommentClient {
}
