package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dyj.applet.domain.QueryIndustryItemOrderInfo;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.interceptor.ByteAuthorizationHeaderInterceptor;
import com.dyj.common.interceptor.ClientTokenInterceptor;

import java.util.List;

/**
 * 行业交易系统
 */
@BaseRequest(baseURL = "${ttDomain}")
public interface IndustryTransactionClient {

    /**
     * 行业交易系统->预下单->查询 CPS 信息
     * @param body 查询 CPS 信息请求值
     * @return
     */
    @Post(value = "${queryTradeCps}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<QueryIndustryCpsVo> queryTradeCps(@JSONBody QueryIndustryOrderCpsQuery body);

    /**
     * 行业交易系统->预下单->查询订单信息
     * @param body 查询订单信息请求值
     * @return
     */
    @Post(value = "${queryTradeOrder}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<QueryTradeOrderVo> queryTradeOrder(@JSONBody QueryTradeOrderQuery body);

    /**
     * 行业交易系统->预下单->开发者发起下单
     * @param body 开发者发起下单
     * @return
     */
    @Post(value = "${createTradeOrder}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<PreCreateIndustryOrderVo> createTradeOrder(@JSONBody CreateTradeOrderQuery body);

    /**
     * 行业交易系统->核销->抖音码->验券准备
     * @param body 验券准备请求值
     * @return
     */
    @Post(value = "${tradeDeliveryPrepare}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<TradeDeliveryPrepareVo> tradeDeliveryPrepare(@JSONBody DeliveryPrepareQuery body);

    /**
     * 行业交易系统->核销->抖音码->验券
     * @param body 验券准备请求值
     * @return
     */
    @Post(value = "${tradeDeliveryVerify}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<TradeDeliveryVerifyVo> tradeDeliveryVerify(@JSONBody DeliveryVerifyQuery body);

    /**
     * 行业交易系统->核销->抖音码->查询劵状态信息
     * @param body 查询劵状态信息请求值
     * @return
     */
    @Post(value = "${queryItemOrderInfo}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<List<QueryIndustryItemOrderInfo>> queryItemOrderInfo(@JSONBody QueryIndustryItemOrderInfoQuery body);

    /**
     * 行业交易系统->核销->三方码->推送核销状态
     * @param body 推送核销状态
     * @return
     */
    @Post(value = "${tradePushDelivery}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<TradePushDeliveryVo> tradePushDelivery(@JSONBody TradePushDeliveryQuery body);

    /**
     * 行业交易系统->分账->发起分账
     * @param body 发起分账
     * @return
     */
    @Post(value = "${tradeCreateSettle}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<CreateSettleV2Vo> tradeCreateSettle(@JSONBody CreateSettleV2Query body);

    /**
     * 行业交易系统->分账->查询分账
     * @param body 查询分账
     * @return
     */
    @Post(value = "${tradeQuerySettle}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<TradeQuerySettleVo> tradeQuerySettle(@JSONBody TradeQuerySettleQuery body);

    /**
     * 行业交易系统->退货退款->开发者发起退款
     * @param body 开发者发起退款
     * @return
     */
    @Post(value = "${tradeCreateRefund}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<CreateRefundVo> tradeCreateRefund(@JSONBody TradeCreateRefundQuery body);

    /**
     * 行业交易系统->退货退款->同步退款审核结果
     * @param body 同步退款审核结果请求值
     * @return
     */
    @Post(value = "${merchantAuditCallbackV2}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<Void> merchantAuditCallbackV2(@JSONBody MerchantAuditCallbackQuery body);

    /**
     * 行业交易系统->退货退款->查询退款
     * @param body 查询退款
     * @return
     */
    @Post(value = "${tradeQueryRefund}", interceptor = ByteAuthorizationHeaderInterceptor.class)
    DySimpleResult<TradeQueryRefundVo> tradeQueryRefund(@JSONBody QueryRefundQuery body);


}
