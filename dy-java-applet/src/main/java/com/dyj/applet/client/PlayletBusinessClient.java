package com.dyj.applet.client;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dyj.applet.domain.query.PlayletBusinessUploadQuery;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.interceptor.ClientTokenInterceptor;

/**
 * 短剧行业
 *
 * @author danmo
 * @date 2024-05-21 10:17
 **/
@BaseRequest(baseURL = "${domain}")
public interface PlayletBusinessClient {

    /**
     * 上传短剧权益事件
     *
     * @param query 上传短剧权益事件入参
     * @return DySimpleResult
     */
    @Post(url = "/api/apps/v1/playlet_business/upload/", interceptor = ClientTokenInterceptor.class)
    public DySimpleResult<String> upload(@JSONBody PlayletBusinessUploadQuery query);
}
