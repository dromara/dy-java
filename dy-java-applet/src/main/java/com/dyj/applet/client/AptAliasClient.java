package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.query.ApplyAliasAddQuery;
import com.dyj.applet.domain.query.ApplyAliasSetSearchTagQuery;
import com.dyj.applet.domain.query.ApplyAliasUpdateQuery;
import com.dyj.applet.domain.vo.ApplyAliasListVo;
import com.dyj.applet.domain.vo.ApplyAliasSetSearchTagVo;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.interceptor.AppV2TokenHeaderInterceptor;

/**
 * 小程序别名接口
 *
 * @author danmo
 * @date 2025/03/04
 */
@BaseRequest(baseURL = "${domain}")
public interface AptAliasClient {

    /**
     * 添加小程序别名
     * @param query 入参
     */
    @Post(value = "${createAlias}", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<String> createAlias(@JSONBody ApplyAliasAddQuery query);

    /**
     * 查询小程序别名
     * @param query 入参
     */
    @Get(value = "${listAlias}", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<ApplyAliasListVo> queryAlias(@Var("query") BaseQuery query);

    /**
     * 修改小程序别名
     * @param query 入参
     */
    @Post(value = "${updateAlias}", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<String> updateAlias(@JSONBody ApplyAliasUpdateQuery query);

    /**
     * 删除小程序别名
     * @param query 入参
     * @param alias:别名
     */
    @Post(value = "${deleteAlias}", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<String> deleteAlias(@Var("query") BaseQuery query, @JSONBody("alias") String alias);

    /**
     * 设置小程序搜索标签
     */
    @Post(value = "${setSearchTag}", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<String> setSearchTag(@JSONBody ApplyAliasSetSearchTagQuery query);

    /**
     * 查询小程序搜索标签列表
     */
    @Get(value = "${getSearchTagList}", interceptor = AppV2TokenHeaderInterceptor.class)
    DySimpleResult<ApplyAliasSetSearchTagVo> getSearchTagList(@Var("query") BaseQuery query);
}
