package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.query.CreateSubscriptionTplQuery;
import com.dyj.applet.domain.query.ImageCensorQuery;
import com.dyj.applet.domain.query.TextAntiDirtQuery;
import com.dyj.applet.domain.vo.ImageCensorVo;
import com.dyj.applet.domain.vo.QueryCreatedSubscriptionTplListVo;
import com.dyj.applet.domain.vo.QuerySubscriptionTplListVo;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.domain.query.DeveloperNotifyQuery;
import com.dyj.common.domain.vo.TextAntiDirtVo;
import com.dyj.common.interceptor.ClientTokenInterceptor;
import com.dyj.common.interceptor.SubscriptionTokenInterceptor;
import com.dyj.common.interceptor.XTokenHeaderInterceptor;

import java.util.List;

/**
 * 触达与营销->订阅消息
 */
@BaseRequest(baseURL = "${domain}")
public interface NotificationSubscriptionClient {

    /**
     * 触达与营销->订阅消息->给用户发送订阅消息
     * @param body 给用户发送订阅消息请求值
     * @return
     */
    @Post(value = "${developerNotify}", interceptor = SubscriptionTokenInterceptor.class)
    DySimpleResult<Void> developerNotify(@JSONBody DeveloperNotifyQuery body);

    /**
     * 触达与营销->订阅消息->查询订阅消息模版库
     * @param categoryIds <p><span style="font-size: 14px;" elementtiming="element-timing">服务类目id，多个id用英文逗号隔开，</span><span style="color: #171A1C;"><span style="font-size: 14px;">可以通过</span></span><span style="color: #3C89FF;"><span style="font-size: 14px;"><a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/server/category/query-app-categories" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">获取已设置的服务类目接口</a></span></span><span style="color: #171A1C;"><span style="font-size: 14px;">获取小程序的服务类目，只能是已经审核通过的服务类目</span></span></p> 选填
     * @param classification 订阅消息类型
     * @param keyword <p>根据关键词搜索</p> 选填
     * @param pageNum 分页编号，从1开始
     * @param pageSize 分页大小，小于等于50
     * @param templateType <p>模版类型</p><p>1: 公共模版</p>
     * @return
     */
    @Get(value = "${querySubscriptionTplList}", interceptor = SubscriptionTokenInterceptor.class)
    DySimpleResult<QuerySubscriptionTplListVo> querySubscriptionTplList(@Var("query") BaseQuery query,
                                                                        @Query("category_ids") String categoryIds,
                                                                        @Query("classification") Integer classification,
                                                                        @Query("keyword") String keyword,
                                                                        @Query("page_num") Integer pageNum,
                                                                        @Query("page_size") Integer pageSize,
                                                                        @Query("template_type") Integer templateType);

    /**
     * 触达与营销->订阅消息->新建订阅消息模板
     * @param body 新建订阅消息模板请求值
     * @return
     */
    @Post(value = "${createSubscriptionTpl}", interceptor = SubscriptionTokenInterceptor.class)
    DySimpleResult<Void> createSubscriptionTpl(@JSONBody CreateSubscriptionTplQuery body);

    /**
     * 触达与营销->订阅消息->查询小程序新建的订阅消息模板列表
     * @param pageNum 分页编号，从1开始
     * @param pageSize 分页大小，小于等于50
     * @param status 审核状态 选填
     * @return
     */
    @Get(value = "${queryCreatedSubscriptionTplList}", interceptor = SubscriptionTokenInterceptor.class)
    DySimpleResult<QueryCreatedSubscriptionTplListVo> queryCreatedSubscriptionTplList(@Var("query") BaseQuery query,@Query("page_num") Integer pageNum, @Query("page_size") Integer pageSize, @Query("status") Integer status);
}
