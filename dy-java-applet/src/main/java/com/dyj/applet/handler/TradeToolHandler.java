package com.dyj.applet.handler;

import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DyResult;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.interceptor.ClientTokenInterceptor;

/**
 * 交易工具
 */
public class TradeToolHandler extends AbstractAppletHandler {
    public TradeToolHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 交易工具->信用免押->信用授权->信用准入查询
     * @param body 信用准入查询请求值
     * @return
     */
    public DySimpleResult<QueryAdmissibleAuthVo> queryAdmissibleAuth(QueryAdmissibleAuthQuery body) {
        userInfoQuery(body);
        return getTradeToolClient().queryAdmissibleAuth(body);
    }

    /**
     * 交易工具->信用免押->信用授权->信用下单
     * @param body 信用下单请求值
     * @return
     */
    public DySimpleResult<CreateAuthOrderDataVo> createAuthOrder(CreateAuthOrderQuery body) {
        baseQuery(body);
        return getTradeToolClient().createAuthOrder(body);
    }

    /**
     * 交易工具->信用免押->信用授权->信用订单撤销
     * @param body 信用订单撤销请求值
     * @return
     */
    public DySimpleResult<Void> cancelAuthOrder(CancelAuthOrderQuery body) {
        baseQuery(body);
        return getTradeToolClient().cancelAuthOrder(body);
    }

    /**
     * 信用订单完结
     * @param body 信用订单完结请求值
     * @return
     */
    public DySimpleResult<Void> finishAuthOrder(FinishAuthOrderQuery body) {
        baseQuery(body);
        return getTradeToolClient().finishAuthOrder(body);
    }

    /**
     * 交易工具->信用免押->信用授权->信用订单查询
     * @param body 信用订单查询请求值
     * @return
     */
    public DySimpleResult<QueryAuthOrderVo> queryAuthOrder(QueryAuthOrderQuery body) {
        baseQuery(body);
        return getTradeToolClient().queryAuthOrder(body);
    }

    /**
     * 交易工具->信用免押->扣款->发起扣款
     * @param body 发起扣款请求值
     * @return
     */
    public DySimpleResult<CreatePayOrderVo> createPayOrder(CreatePayOrderQuery body) {
        baseQuery(body);
        return getTradeToolClient().createPayOrder(body);
    }

    /**
     * 交易工具->信用免押->扣款->关闭扣款
     * @param body 关闭扣款请求值
     * @return
     */
    public DySimpleResult<Void> closePayOrder(@JSONBody ClosePayOrderQuery body) {
        baseQuery(body);
        return getTradeToolClient().closePayOrder(body);
    }

    /**
     * 交易工具->信用免押->扣款->扣款单查询
     * @param body 扣款单查询请求值
     * @return
     */
    public DySimpleResult<QueryPayOrderVo> queryPayOrder(QueryPayOrderQuery body) {
        baseQuery(body);
        return getTradeToolClient().queryPayOrder(body);
    }


    /**
     * 交易工具->信用免押->退款->发起退款
     * @param body 发起退款请求值
     * @return
     */
    public DySimpleResult<CreatePayOrderVo> createPayOrderRefund(CreatePayOrderRefundQuery body) {
        baseQuery(body);
        return getTradeToolClient().createPayOrderRefund(body);
    }


    /**
     * 交易工具->信用免押->退款->退款查询
     * @param body 退款查询请求值
     * @return
     */
    public DySimpleResult<QueryPayRefundVo> queryPayRefund(QueryPayRefundQuery body) {
        baseQuery(body);
        return getTradeToolClient().queryPayRefund(body);
    }

    /**
     * 交易工具->周期代扣->签约授权->发起解约
     * @param body 发起解约请求值
     * @return
     */
    public DySimpleResult<Void> terminateSign(TerminateSignQuery body) {
        baseQuery(body);
        return getTradeToolClient().terminateSign(body);
    }

    /**
     * 交易工具->周期代扣->签约授权->签约订单查询
     * @param body 签约订单查询请求值
     * @return
     */
    public DySimpleResult<QuerySignOrderVo> querySignOrder(QuerySignOrderQuery body) {
        baseQuery(body);
        return getTradeToolClient().querySignOrder(body);
    }

    /**
     * 交易工具->周期代扣->代扣->发起代扣
     * @param body 发起代扣请求值
     * @return
     */
    public DySimpleResult<CreateSignPayOrderVo> createSignPayOrder(CreateSignPayOrderQuery body) {
        baseQuery(body);
        return getTradeToolClient().createSignPayOrder(body);
    }

    /**
     * 交易工具->周期代扣->代扣->代扣结果查询
     * @param body 代扣结果查询请求值
     * @return
     */
    public DySimpleResult<QuerySignPayOrderVo> querySignPayOrder(QuerySignPayOrderQuery body) {
        baseQuery(body);
        return getTradeToolClient().querySignPayOrder(body);
    }


    /**
     * 交易工具->周期代扣->退款->发起退款
     * @param body 发起退款请求值
     * @return
     */
    public DySimpleResult<CreateSignRefundVo> createSignRefund(CreateSignRefundQuery body) {
        baseQuery(body);
        return getTradeToolClient().createSignRefund(body);
    }


    /**
     * 交易工具->周期代扣->退款->退款查询
     * @param body 退款查询请求值
     * @return
     */
    public DySimpleResult<QuerySignRefundVo> querySignRefund(QuerySignRefundQuery body) {
        baseQuery(body);
        return getTradeToolClient().querySignRefund(body);
    }
}
