package com.dyj.applet.handler;

import com.dyj.applet.domain.vo.BinDoudianAccountVo;
import com.dyj.applet.domain.vo.CreateDoudianAppVo;
import com.dyj.applet.domain.vo.DoudianAppVo;
import com.dyj.applet.domain.vo.DoudianShopVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-12 14:37
 **/
public class AptDouDianHandler extends AbstractAppletHandler {
    public AptDouDianHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 绑定抖店开放平台账号
     *
     * @return DySimpleResult<BinDoudianAccountVo>
     */
    public DySimpleResult<BinDoudianAccountVo> bindDoudianAccount() {
        return getDouDianClient().bindDoudianAccount(userInfoQuery());
    }

    /**
     * 查询绑定的抖店开放平台账号信息
     *
     * @return DySimpleResult<BinDoudianAccountVo>
     */
    public DySimpleResult<BinDoudianAccountVo> queryBindDoudianAccount() {
        return getDouDianClient().queryBindDoudianAccount(userInfoQuery());
    }

    /**
     * 配置抖店开放平台应用
     *
     * @param shopName       店铺名称
     * @param note           备注说明
     * @param screenShotList 系统主要功能截图，使用上传素材接口获取到的路径
     * @return DySimpleResult<CreateDoudianAccountVo>
     */
    public DySimpleResult<CreateDoudianAppVo> createDoudianApp(String shopName, String note, List<String> screenShotList) {
        return getDouDianClient().createDoudianApp(userInfoQuery(), shopName, note, screenShotList);
    }

    /**
     * 查询配置的抖店开放平台应用信息
     *
     * @return DySimpleResult<DoudianAppVo>
     */
    public DySimpleResult<DoudianAppVo> queryDoudianApp() {
        return getDouDianClient().queryDoudianApp(userInfoQuery());
    }

    /**
     * 获取绑定抖店账号信息
     *
     * @return DySimpleResult<DoudianShopVo>
     */
    public DySimpleResult<DoudianShopVo> queryDoudianShopInfo() {
        return getDouDianClient().queryDoudianShopInfo(userInfoQuery());
    }
}
