package com.dyj.applet.handler;

import com.dtflys.forest.annotation.*;
import com.dyj.applet.domain.query.CreateSubscriptionTplQuery;
import com.dyj.applet.domain.query.ImageCensorQuery;
import com.dyj.applet.domain.query.TextAntiDirtQuery;
import com.dyj.applet.domain.vo.ImageCensorVo;
import com.dyj.applet.domain.vo.QueryCreatedSubscriptionTplListVo;
import com.dyj.applet.domain.vo.QuerySubscriptionTplListVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.domain.query.DeveloperNotifyQuery;
import com.dyj.common.domain.vo.TextAntiDirtVo;
import com.dyj.common.interceptor.SubscriptionTokenInterceptor;

import java.util.List;

/**
 * 触达与营销->订阅消息
 */
public class NotificationSubscriptionHandler extends AbstractAppletHandler{

    public NotificationSubscriptionHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 触达与营销->订阅消息->给用户发送订阅消息
     * @param body 给用户发送订阅消息请求值
     * @return
     */
    public DySimpleResult<Void> developerNotify(DeveloperNotifyQuery body){
        baseQuery(body);
        return getNotificationSubscriptionClient().developerNotify(body);
    }

    /**
     * 触达与营销->订阅消息->查询订阅消息模版库
     * @param categoryIds <p><span style="font-size: 14px;" elementtiming="element-timing">服务类目id，多个id用英文逗号隔开，</span><span style="color: #171A1C;"><span style="font-size: 14px;">可以通过</span></span><span style="color: #3C89FF;"><span style="font-size: 14px;"><a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/server/category/query-app-categories" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">获取已设置的服务类目接口</a></span></span><span style="color: #171A1C;"><span style="font-size: 14px;">获取小程序的服务类目，只能是已经审核通过的服务类目</span></span></p> 选填
     * @param classification 订阅消息类型
     * @param keyword <p>根据关键词搜索</p> 选填
     * @param pageNum 分页编号，从1开始
     * @param pageSize 分页大小，小于等于50
     * @param templateType <p>模版类型</p><p>1: 公共模版</p>
     * @return
     */
    public DySimpleResult<QuerySubscriptionTplListVo> querySubscriptionTplList(String categoryIds,
                                                                               Integer classification,
                                                                               String keyword,
                                                                               Integer pageNum,
                                                                               Integer pageSize,
                                                                               Integer templateType){
        return getNotificationSubscriptionClient().querySubscriptionTplList(baseQuery(), categoryIds, classification, keyword, pageNum, pageSize, templateType);

    }

    /**
     * 触达与营销->订阅消息->新建订阅消息模板
     * @param body 新建订阅消息模板请求值
     * @return
     */
    public DySimpleResult<Void> createSubscriptionTpl(CreateSubscriptionTplQuery body) {
        baseQuery(body);
        return getNotificationSubscriptionClient().createSubscriptionTpl(body);
    }

    /**
     * 触达与营销->订阅消息->查询小程序新建的订阅消息模板列表
     * @param pageNum 分页编号，从1开始
     * @param pageSize 分页大小，小于等于50
     * @param status 审核状态 选填
     * @return
     */
    public DySimpleResult<QueryCreatedSubscriptionTplListVo> queryCreatedSubscriptionTplList(Integer pageNum, Integer pageSize, Integer status) {
        return getNotificationSubscriptionClient().queryCreatedSubscriptionTplList(baseQuery(), pageNum, pageSize, status);
    }
}
