package com.dyj.applet.handler;

import com.dyj.applet.domain.vo.AwemeVideoKeywordListVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

/**
 * @author danmo
 * @date 2024-07-17 10:23
 **/
public class AptOpenCapacityHandler extends AbstractAppletHandler {

    public AptOpenCapacityHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }


    /**
     * 查询视频关键词列表
     * 只有开通了 permission_key 为 open.video.search 权限的小程序才可以调用
     *
     * @param pageNum  分页编号，从 1 开始
     * @param pageSize 分页大小，小于等于 20
     * @return DySimpleResult<AwemeVideoKeywordListVo>
     */
    public DySimpleResult<AwemeVideoKeywordListVo> queryAwemeVideoKeywordList(Long pageNum, Long pageSize) {
        return getOpenCapacityClient().queryAwemeVideoKeywordList(baseQuery(), pageNum, pageSize);
    }

    /**
     * 添加视频关键词
     *
     * @param keyword 关键词
     * @param reason  添加原因
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> addAwemeVideoKeyword(String keyword, String reason) {
        return getOpenCapacityClient().addAwemeVideoKeyword(baseQuery(), keyword, reason);
    }

    /**
     * 删除视频关键词
     *
     * @param keywordId 需要删除的关键词id，必须是审核通过/拒绝的关键词
     * @return DySimpleResult<String>
     */
    public DySimpleResult<String> deleteAwemeVideoKeyword(String keywordId) {
        return getOpenCapacityClient().deleteAwemeVideoKeyword(baseQuery(), keywordId);
    }
}
