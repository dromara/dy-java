package com.dyj.applet.handler;

import com.dtflys.forest.annotation.JSONBody;
import com.dtflys.forest.annotation.Post;
import com.dyj.applet.domain.query.ImageCensorQuery;
import com.dyj.applet.domain.query.TextAntiDirtQuery;
import com.dyj.applet.domain.vo.ImageCensorVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.vo.TextAntiDirtVo;
import com.dyj.common.interceptor.ClientTokenInterceptor;
import com.dyj.common.interceptor.XTokenHeaderInterceptor;

import java.util.List;

/**
 * 基础能力->内容安全
 */
public class CensorHandler extends AbstractAppletHandler{

    public CensorHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }


    /**
     * 基础能力->内容安全->图片检测V3
     * @param body 图片检测V3请求值
     * @return
     */
    public ImageCensorVo imageCensor(ImageCensorQuery body){
        baseQuery(body);
        return getCensorClient().imageCensor(body);
    }

    /**
     * 基础能力->内容安全->内容安全检测
     * @param body
     * @return
     */
    public DySimpleResult<List<TextAntiDirtVo>> textAntiDirt(TextAntiDirtQuery body) {
        baseQuery(body);
        return getCensorClient().textAntiDirt(body);
    }
}
