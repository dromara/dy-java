package com.dyj.applet.domain.query;

import com.dyj.applet.domain.*;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

import java.util.List;

/**
 * 发起进件请求值
 */
public class CreateSubMerchantMerchantQuery extends BaseTransactionMerchantQuery {

    /**
     * <p>小程序的 app_id</p><p>在给小程序或小程序合作方进件必填</p> 选填
     */
    private String app_id;
    /**
     * <p>受益人类型枚举:</p><p>LEGAL: 法人</p><p>BENEFICIARY: 其他受益人</p><p>当所选渠道包含微信、商户类型merchant_type=2为企业时，必填</p> 选填
     */
    private String beneficiary_type;
    /**
     * <p>进件结果通知接口（开发者自己的https服务）；如果不传默认用支付设置中的回调地址（路径：小程序开发者平台-功能管理-支付-支付产品-支付设置- URL）</p> 选填
     */
    private String callback_url;
    /**
     * <p>注册的市编码，<a href="https://www.b910.cn/tool/1.htm" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">获取链接</a></p>
     */
    private String city_code;
    /**
     * <p>张三</p>
     */
    private String create_name;
    /**
     * <p>注册的区编码，<a href="https://www.b910.cn/tool/1.htm" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">获取链接</a></p>
     */
    private String district_code;
    /**
     * <p>商户名称 和营业执照/身份证上的一致</p>
     */
    private String merchant_name;
    /**
     * 商户简称
     */
    private String merchant_short_name;
    /**
     * <p>商户类型枚举值：</p><p>1: 个人</p><p>2: 企业</p><p>3: 个体工商户</p><p>4: 小微商户</p><p>5: 事业单位</p><p>6: 民办非企业组织</p><p>7: 社会团体</p><p>8: 党政及国家机关</p>
     */
    private Long merchant_type;
    /**
     * <p>外部单号，必须保证唯一，用来标识本次进件请求</p>
     */
    private String out_order_id;
    /**
     * <p>注册的省编码 ，<a href="https://www.b910.cn/tool/1.htm" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">获取链接</a></p>
     */
    private String province_code;
    /**
     * 注册详细地址
     */
    private String registered_addr;
    /**
     * <p>商户id，用于接入方自行标识并管理进件方，需要保证在app_id下唯一</p><p>type=3时不需传，其他情况必填</p> 选填
     */
    private String sub_merchant_id;
    /**
     * <p>小程序第三方平台应用 id</p><p>在服务商身份下必填</p> 选填
     */
    private String thirdparty_id;
    /**
     * <p>进件类型枚举值：</p><p>1: 开发者给小程序收款商户进件</p><p>2: 开发者给小程序合作方进件</p><p>3: 服务商为自己进件</p><p>4: 服务商给小程序收款商户进件</p><p>5: 服务商给小程序的合作方进件</p><p>注意：2、3、5类型的商户号只能接收分账，没有收款权限</p>
     */
    private Long type;
    /**
     * <p>受益人信息</p><p>当所选渠道包含微信，商户类型merchant_type=2为企业，且受益人类型beneficiary_type不是法人时必填，参照下面beneficiary参数描述</p> 选填
     */
    private CreateSubMerchantBeneficiary beneficiary;
    /**
     * <p>经营地址</p>
     */
    private CreateSubMerchantBusinessLicense business_license;
    /**
     * <p>进件渠道</p><p>枚举值：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">抖音支付：hz</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">支付宝：alipay</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">微信支付：wx</li></ul>
     */
    private List<String> channels;
    /**
     * 补充材料图片 选填
     */
    private List<ChannelAndUrl> ext_evidences;
    /**
     * <p>行业类型code，<a href="https://sf1-cdn-tos.douyinstatic.com/obj/microapp/frontend/docs/%E5%95%86%E6%88%B7%E5%B9%B3%E5%8F%B0%E8%A1%8C%E4%B8%9A%E7%B1%BB%E5%9E%8B%E5%8F%8A%E7%89%B9%E6%AE%8A%E8%A1%8C%E4%B8%9A%E8%B5%84%E8%B4%A8%E6%95%B4%E7%90%86.xlsx" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">获取链接</a></p>
     */
    private List<String> industry_code;
    /**
     * <p>1.特殊行业需要额外补充对应材料，具体如下：<a href="https://lf3-static.bytednsdoc.com/obj/eden-cn/fhw_tyvmahsw_ulwv/ljhwZthlaukjlkulzlp/%E7%89%B9%E6%AE%8A%E8%A1%8C%E4%B8%9A%E8%A1%A5%E5%85%85%E6%9D%90%E6%96%99%E8%AF%B4%E6%98%8E.xlsx" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">特殊行业补充材料说明</a></p><p>2.最多可上传 5 张图片，图片仅支持 BMP、JPG、JPEG、PNG 格式，大小不超过 2MB</p><p>3.图片信息列表，详见下面 图片信息参数</p> 选填
     */
    private List<ChannelAndUrl> industry_info_pic_urls;
    /**
     * 法人信息
     */
    private CreateSubMerchantLegalPerson legal_person;
    /**
     * <p>结算账户信息信息</p>
     */
    private CreateSubMerchantMerchantCardInfo merchant_card_info;
    /**
     * 商户管理员信息
     */
    private CreateSubMerchantMerchantOperationInfo merchant_operation_info;

    public String getApp_id() {
        return app_id;
    }

    public CreateSubMerchantMerchantQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getBeneficiary_type() {
        return beneficiary_type;
    }

    public CreateSubMerchantMerchantQuery setBeneficiary_type(String beneficiary_type) {
        this.beneficiary_type = beneficiary_type;
        return this;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public CreateSubMerchantMerchantQuery setCallback_url(String callback_url) {
        this.callback_url = callback_url;
        return this;
    }

    public String getCity_code() {
        return city_code;
    }

    public CreateSubMerchantMerchantQuery setCity_code(String city_code) {
        this.city_code = city_code;
        return this;
    }

    public String getCreate_name() {
        return create_name;
    }

    public CreateSubMerchantMerchantQuery setCreate_name(String create_name) {
        this.create_name = create_name;
        return this;
    }

    public String getDistrict_code() {
        return district_code;
    }

    public CreateSubMerchantMerchantQuery setDistrict_code(String district_code) {
        this.district_code = district_code;
        return this;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public CreateSubMerchantMerchantQuery setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
        return this;
    }

    public String getMerchant_short_name() {
        return merchant_short_name;
    }

    public CreateSubMerchantMerchantQuery setMerchant_short_name(String merchant_short_name) {
        this.merchant_short_name = merchant_short_name;
        return this;
    }

    public Long getMerchant_type() {
        return merchant_type;
    }

    public CreateSubMerchantMerchantQuery setMerchant_type(Long merchant_type) {
        this.merchant_type = merchant_type;
        return this;
    }

    public String getOut_order_id() {
        return out_order_id;
    }

    public CreateSubMerchantMerchantQuery setOut_order_id(String out_order_id) {
        this.out_order_id = out_order_id;
        return this;
    }

    public String getProvince_code() {
        return province_code;
    }

    public CreateSubMerchantMerchantQuery setProvince_code(String province_code) {
        this.province_code = province_code;
        return this;
    }

    public String getRegistered_addr() {
        return registered_addr;
    }

    public CreateSubMerchantMerchantQuery setRegistered_addr(String registered_addr) {
        this.registered_addr = registered_addr;
        return this;
    }

    public String getSub_merchant_id() {
        return sub_merchant_id;
    }

    public CreateSubMerchantMerchantQuery setSub_merchant_id(String sub_merchant_id) {
        this.sub_merchant_id = sub_merchant_id;
        return this;
    }

    public String getThirdparty_id() {
        return thirdparty_id;
    }

    public CreateSubMerchantMerchantQuery setThirdparty_id(String thirdparty_id) {
        this.thirdparty_id = thirdparty_id;
        return this;
    }

    public Long getType() {
        return type;
    }

    public CreateSubMerchantMerchantQuery setType(Long type) {
        this.type = type;
        return this;
    }

    public CreateSubMerchantBeneficiary getBeneficiary() {
        return beneficiary;
    }

    public CreateSubMerchantMerchantQuery setBeneficiary(CreateSubMerchantBeneficiary beneficiary) {
        this.beneficiary = beneficiary;
        return this;
    }

    public CreateSubMerchantBusinessLicense getBusiness_license() {
        return business_license;
    }

    public CreateSubMerchantMerchantQuery setBusiness_license(CreateSubMerchantBusinessLicense business_license) {
        this.business_license = business_license;
        return this;
    }

    public List<String> getChannels() {
        return channels;
    }

    public CreateSubMerchantMerchantQuery setChannels(List<String> channels) {
        this.channels = channels;
        return this;
    }

    public List<ChannelAndUrl> getExt_evidences() {
        return ext_evidences;
    }

    public CreateSubMerchantMerchantQuery setExt_evidences(List<ChannelAndUrl> ext_evidences) {
        this.ext_evidences = ext_evidences;
        return this;
    }

    public List<String> getIndustry_code() {
        return industry_code;
    }

    public CreateSubMerchantMerchantQuery setIndustry_code(List<String> industry_code) {
        this.industry_code = industry_code;
        return this;
    }

    public List<ChannelAndUrl> getIndustry_info_pic_urls() {
        return industry_info_pic_urls;
    }

    public CreateSubMerchantMerchantQuery setIndustry_info_pic_urls(List<ChannelAndUrl> industry_info_pic_urls) {
        this.industry_info_pic_urls = industry_info_pic_urls;
        return this;
    }

    public CreateSubMerchantLegalPerson getLegal_person() {
        return legal_person;
    }

    public CreateSubMerchantMerchantQuery setLegal_person(CreateSubMerchantLegalPerson legal_person) {
        this.legal_person = legal_person;
        return this;
    }

    public CreateSubMerchantMerchantCardInfo getMerchant_card_info() {
        return merchant_card_info;
    }

    public CreateSubMerchantMerchantQuery setMerchant_card_info(CreateSubMerchantMerchantCardInfo merchant_card_info) {
        this.merchant_card_info = merchant_card_info;
        return this;
    }

    public CreateSubMerchantMerchantOperationInfo getMerchant_operation_info() {
        return merchant_operation_info;
    }

    public CreateSubMerchantMerchantQuery setMerchant_operation_info(CreateSubMerchantMerchantOperationInfo merchant_operation_info) {
        this.merchant_operation_info = merchant_operation_info;
        return this;
    }

    public static CreateSubMerchantQueryBuilder builder() {
        return new CreateSubMerchantQueryBuilder();
    }

    public static final class CreateSubMerchantQueryBuilder {
        private String app_id;
        private String beneficiary_type;
        private String callback_url;
        private String city_code;
        private String create_name;
        private String district_code;
        private String merchant_name;
        private String merchant_short_name;
        private Long merchant_type;
        private String out_order_id;
        private String province_code;
        private String registered_addr;
        private String sub_merchant_id;
        private String thirdparty_id;
        private Long type;
        private CreateSubMerchantBeneficiary beneficiary;
        private CreateSubMerchantBusinessLicense business_license;
        private List<String> channels;
        private List<ChannelAndUrl> ext_evidences;
        private List<String> industry_code;
        private List<ChannelAndUrl> industry_info_pic_urls;
        private CreateSubMerchantLegalPerson legal_person;
        private CreateSubMerchantMerchantCardInfo merchant_card_info;
        private CreateSubMerchantMerchantOperationInfo merchant_operation_info;
        private Integer tenantId;
        private String clientKey;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType = TransactionMerchantTokenTypeEnum.CLIENT_TOKEN;

        private CreateSubMerchantQueryBuilder() {
        }

        public CreateSubMerchantQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public CreateSubMerchantQueryBuilder beneficiaryType(String beneficiaryType) {
            this.beneficiary_type = beneficiaryType;
            return this;
        }

        public CreateSubMerchantQueryBuilder callbackUrl(String callbackUrl) {
            this.callback_url = callbackUrl;
            return this;
        }

        public CreateSubMerchantQueryBuilder cityCode(String cityCode) {
            this.city_code = cityCode;
            return this;
        }

        public CreateSubMerchantQueryBuilder createName(String createName) {
            this.create_name = createName;
            return this;
        }

        public CreateSubMerchantQueryBuilder districtCode(String districtCode) {
            this.district_code = districtCode;
            return this;
        }

        public CreateSubMerchantQueryBuilder merchantName(String merchantName) {
            this.merchant_name = merchantName;
            return this;
        }

        public CreateSubMerchantQueryBuilder merchantShortName(String merchantShortName) {
            this.merchant_short_name = merchantShortName;
            return this;
        }

        public CreateSubMerchantQueryBuilder merchantType(Long merchantType) {
            this.merchant_type = merchantType;
            return this;
        }

        public CreateSubMerchantQueryBuilder outOrderId(String outOrderId) {
            this.out_order_id = outOrderId;
            return this;
        }

        public CreateSubMerchantQueryBuilder provinceCode(String provinceCode) {
            this.province_code = provinceCode;
            return this;
        }

        public CreateSubMerchantQueryBuilder registeredAddr(String registeredAddr) {
            this.registered_addr = registeredAddr;
            return this;
        }

        public CreateSubMerchantQueryBuilder subMerchantId(String subMerchantId) {
            this.sub_merchant_id = subMerchantId;
            return this;
        }

        public CreateSubMerchantQueryBuilder thirdpartyId(String thirdpartyId) {
            this.thirdparty_id = thirdpartyId;
            return this;
        }

        public CreateSubMerchantQueryBuilder type(Long type) {
            this.type = type;
            return this;
        }

        public CreateSubMerchantQueryBuilder beneficiary(CreateSubMerchantBeneficiary beneficiary) {
            this.beneficiary = beneficiary;
            return this;
        }

        public CreateSubMerchantQueryBuilder businessLicense(CreateSubMerchantBusinessLicense businessLicense) {
            this.business_license = businessLicense;
            return this;
        }

        public CreateSubMerchantQueryBuilder channels(List<String> channels) {
            this.channels = channels;
            return this;
        }

        public CreateSubMerchantQueryBuilder extEvidences(List<ChannelAndUrl> extEvidences) {
            this.ext_evidences = extEvidences;
            return this;
        }

        public CreateSubMerchantQueryBuilder industryCode(List<String> industryCode) {
            this.industry_code = industryCode;
            return this;
        }

        public CreateSubMerchantQueryBuilder industryInfoPicUrls(List<ChannelAndUrl> industryInfoPicUrls) {
            this.industry_info_pic_urls = industryInfoPicUrls;
            return this;
        }

        public CreateSubMerchantQueryBuilder legalPerson(CreateSubMerchantLegalPerson legalPerson) {
            this.legal_person = legalPerson;
            return this;
        }

        public CreateSubMerchantQueryBuilder merchantCardInfo(CreateSubMerchantMerchantCardInfo merchantCardInfo) {
            this.merchant_card_info = merchantCardInfo;
            return this;
        }

        public CreateSubMerchantQueryBuilder merchantOperationInfo(CreateSubMerchantMerchantOperationInfo merchantOperationInfo) {
            this.merchant_operation_info = merchantOperationInfo;
            return this;
        }

        public CreateSubMerchantQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateSubMerchantQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateSubMerchantQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public CreateSubMerchantMerchantQuery build() {
            CreateSubMerchantMerchantQuery createSubMerchantQuery = new CreateSubMerchantMerchantQuery();
            createSubMerchantQuery.setApp_id(app_id);
            createSubMerchantQuery.setBeneficiary_type(beneficiary_type);
            createSubMerchantQuery.setCallback_url(callback_url);
            createSubMerchantQuery.setCity_code(city_code);
            createSubMerchantQuery.setCreate_name(create_name);
            createSubMerchantQuery.setDistrict_code(district_code);
            createSubMerchantQuery.setMerchant_name(merchant_name);
            createSubMerchantQuery.setMerchant_short_name(merchant_short_name);
            createSubMerchantQuery.setMerchant_type(merchant_type);
            createSubMerchantQuery.setOut_order_id(out_order_id);
            createSubMerchantQuery.setProvince_code(province_code);
            createSubMerchantQuery.setRegistered_addr(registered_addr);
            createSubMerchantQuery.setSub_merchant_id(sub_merchant_id);
            createSubMerchantQuery.setThirdparty_id(thirdparty_id);
            createSubMerchantQuery.setType(type);
            createSubMerchantQuery.setBeneficiary(beneficiary);
            createSubMerchantQuery.setBusiness_license(business_license);
            createSubMerchantQuery.setChannels(channels);
            createSubMerchantQuery.setExt_evidences(ext_evidences);
            createSubMerchantQuery.setIndustry_code(industry_code);
            createSubMerchantQuery.setIndustry_info_pic_urls(industry_info_pic_urls);
            createSubMerchantQuery.setLegal_person(legal_person);
            createSubMerchantQuery.setMerchant_card_info(merchant_card_info);
            createSubMerchantQuery.setMerchant_operation_info(merchant_operation_info);
            createSubMerchantQuery.setTenantId(tenantId);
            createSubMerchantQuery.setClientKey(clientKey);
            createSubMerchantQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            return createSubMerchantQuery;
        }
    }
}
