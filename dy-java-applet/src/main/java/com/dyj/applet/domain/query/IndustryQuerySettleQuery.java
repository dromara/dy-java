package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 查询分账请求值
 */
public class IndustryQuerySettleQuery extends BaseQuery {

    /**
     * 抖音开平侧订单 id，长度 <= 64字节 选填
     */
    private String order_id;
    /**
     * 开发者侧订单 id，长度 <= 64 字节 选填
     */
    private String out_order_no;
    /**
     * 开发者侧分账单 id，长度 <= 64字节 选填
     */
    private String out_settle_no;
    /**
     * 抖音开平侧分账单 id，长度 <= 64字节 选填
     */
    private String settle_id;

    public String getOrder_id() {
        return order_id;
    }

    public IndustryQuerySettleQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public IndustryQuerySettleQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getOut_settle_no() {
        return out_settle_no;
    }

    public IndustryQuerySettleQuery setOut_settle_no(String out_settle_no) {
        this.out_settle_no = out_settle_no;
        return this;
    }

    public String getSettle_id() {
        return settle_id;
    }

    public IndustryQuerySettleQuery setSettle_id(String settle_id) {
        this.settle_id = settle_id;
        return this;
    }

    public static IndustryQuerySettleQueryBuilder builder() {
        return new IndustryQuerySettleQueryBuilder();
    }

    public static final class IndustryQuerySettleQueryBuilder {
        private String order_id;
        private String out_order_no;
        private String out_settle_no;
        private String settle_id;
        private Integer tenantId;
        private String clientKey;

        private IndustryQuerySettleQueryBuilder() {
        }

        public IndustryQuerySettleQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public IndustryQuerySettleQueryBuilder outOrderNo(String outOrderNo) {
            this.out_order_no = outOrderNo;
            return this;
        }

        public IndustryQuerySettleQueryBuilder outSettleNo(String outSettleNo) {
            this.out_settle_no = outSettleNo;
            return this;
        }

        public IndustryQuerySettleQueryBuilder settleId(String settleId) {
            this.settle_id = settleId;
            return this;
        }

        public IndustryQuerySettleQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public IndustryQuerySettleQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public IndustryQuerySettleQuery build() {
            IndustryQuerySettleQuery industryQuerySettleQuery = new IndustryQuerySettleQuery();
            industryQuerySettleQuery.setOrder_id(order_id);
            industryQuerySettleQuery.setOut_order_no(out_order_no);
            industryQuerySettleQuery.setOut_settle_no(out_settle_no);
            industryQuerySettleQuery.setSettle_id(settle_id);
            industryQuerySettleQuery.setTenantId(tenantId);
            industryQuerySettleQuery.setClientKey(clientKey);
            return industryQuerySettleQuery;
        }
    }
}
