package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class QueryTradeOrderQuery extends BaseQuery {

    /**
     * 抖音开平内部交易订单号，通过预下单回调传给开发者服务，长度 < 64byte 选填
     */
    private String order_id;
    /**
     * 开发者系统生成的订单号，与唯一order_id关联，长度 < 64byte 选填
     */
    private String out_order_no;

    public String getOrder_id() {
        return order_id;
    }

    public QueryTradeOrderQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public QueryTradeOrderQuery setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public static QueryTradeOrderQueryBuilder builder() {
        return new QueryTradeOrderQueryBuilder();
    }

    public static final class QueryTradeOrderQueryBuilder {
        private String order_id;
        private String out_order_no;
        private Integer tenantId;
        private String clientKey;

        private QueryTradeOrderQueryBuilder() {
        }

        public QueryTradeOrderQueryBuilder order_id(String order_id) {
            this.order_id = order_id;
            return this;
        }

        public QueryTradeOrderQueryBuilder out_order_no(String out_order_no) {
            this.out_order_no = out_order_no;
            return this;
        }

        public QueryTradeOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryTradeOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryTradeOrderQuery build() {
            QueryTradeOrderQuery queryTradeOrderQuery = new QueryTradeOrderQuery();
            queryTradeOrderQuery.setOrder_id(order_id);
            queryTradeOrderQuery.setOut_order_no(out_order_no);
            queryTradeOrderQuery.setTenantId(tenantId);
            queryTradeOrderQuery.setClientKey(clientKey);
            return queryTradeOrderQuery;
        }
    }
}
