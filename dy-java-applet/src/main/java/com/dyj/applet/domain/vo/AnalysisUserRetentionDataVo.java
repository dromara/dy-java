package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisUserRetentionData;

import java.util.List;

public class AnalysisUserRetentionDataVo {

    private List<AnalysisUserRetentionData> retention_data_list;

    public List<AnalysisUserRetentionData> getRetention_data_list() {
        return retention_data_list;
    }

    public void setRetention_data_list(List<AnalysisUserRetentionData> retention_data_list) {
        this.retention_data_list = retention_data_list;
    }
}
