package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 信用下单请求值
 */
public class CreateAuthOrderQuery extends BaseQuery {

    /**
     * <p>准入token，调用<a href="/docs/resource/zh-CN/mini-app/develop/server/management-capacity/auth-deposit/auth/query-admissible-auth" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">信用准入查询</a>获取准入token</p>
     */
    private String admissible_token;
    /**
     * <p>下单回调地址，https开头，长度<=512byte</p> 选填
     */
    private String notify_url;
    /**
     * <p>信用单开发者的单号，长度<=64byte</p>
     */
    private String out_auth_order_no;

    public String getAdmissible_token() {
        return admissible_token;
    }

    public CreateAuthOrderQuery setAdmissible_token(String admissible_token) {
        this.admissible_token = admissible_token;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public CreateAuthOrderQuery setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOut_auth_order_no() {
        return out_auth_order_no;
    }

    public CreateAuthOrderQuery setOut_auth_order_no(String out_auth_order_no) {
        this.out_auth_order_no = out_auth_order_no;
        return this;
    }

    public static CreateAuthOrderQueryBuilder builder() {
        return new CreateAuthOrderQueryBuilder();
    }

    public static final class CreateAuthOrderQueryBuilder {
        private String admissible_token;
        private String notify_url;
        private String out_auth_order_no;
        private Integer tenantId;
        private String clientKey;

        private CreateAuthOrderQueryBuilder() {
        }

        public CreateAuthOrderQueryBuilder admissibleToken(String admissibleToken) {
            this.admissible_token = admissibleToken;
            return this;
        }

        public CreateAuthOrderQueryBuilder notifyUrl(String notifyUrl) {
            this.notify_url = notifyUrl;
            return this;
        }

        public CreateAuthOrderQueryBuilder outAuthOrderNo(String outAuthOrderNo) {
            this.out_auth_order_no = outAuthOrderNo;
            return this;
        }

        public CreateAuthOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateAuthOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateAuthOrderQuery build() {
            CreateAuthOrderQuery createAuthOrderQuery = new CreateAuthOrderQuery();
            createAuthOrderQuery.setAdmissible_token(admissible_token);
            createAuthOrderQuery.setNotify_url(notify_url);
            createAuthOrderQuery.setOut_auth_order_no(out_auth_order_no);
            createAuthOrderQuery.setTenantId(tenantId);
            createAuthOrderQuery.setClientKey(clientKey);
            return createAuthOrderQuery;
        }
    }
}
