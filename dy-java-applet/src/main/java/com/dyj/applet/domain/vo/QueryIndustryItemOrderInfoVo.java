package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QueryIndustryItemOrderInfo;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * 查询券状态信息返回值
 */
public class QueryIndustryItemOrderInfoVo extends BaseVo {

    /**
     * 列表，返回的item单数据
     */
    private List<QueryIndustryItemOrderInfo> item_list;

    public List<QueryIndustryItemOrderInfo> getItem_list() {
        return item_list;
    }

    public QueryIndustryItemOrderInfoVo setItem_list(List<QueryIndustryItemOrderInfo> item_list) {
        this.item_list = item_list;
        return this;
    }
}
