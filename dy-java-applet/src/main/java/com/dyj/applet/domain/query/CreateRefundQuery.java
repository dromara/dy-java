package com.dyj.applet.domain.query;

import com.dyj.applet.domain.RefundItemOrderDetail;
import com.dyj.applet.domain.RefundOrderEntrySchema;
import com.dyj.applet.domain.RefundReason;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

/**
 * 发起退款请求值
 */
public class CreateRefundQuery extends BaseQuery {

    /**
     * 开发者自定义透传字段，不支持二进制，长度 <= 2048 byte
     */
    private String cp_extra;
    /**
     * <p>退款结果通知地址，必须是 HTTPS 类型， 长度 &lt;= 512 byte 。若不填，则默认使用在解决方案配置-消息通知中指定的回调地址，配置方式参考<a href="/docs/resource/zh-CN/mini-app/open-capacity/Industry/industry_mode/guide/" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">解决方案配置文档</a></p> 选填
     */
    private String notify_url;
    /**
     * 抖音开平侧订单号，长度 <= 64 byte
     */
    private String order_id;
    /**
     * 开发者侧退款单号，长度 <= 64 byte
     */
    private String out_refund_no;
    /**
     * <p>当订单未发生任何退款时，可设置refund_all=true，refund_total_amount=订单实付金额，发起整单退款。refund_all=true时不能设置item_order_detail</p> 选填
     */
    private Boolean refund_all;
    /**
     * 退款总金额，单位分
     */
    private Long refund_total_amount;
    /**
     * 需要发起退款的商品单信息 选填
     */
    private List<RefundItemOrderDetail> item_order_detail;
    /**
     * <p>退款单的跳转的schema，参考<a href="/docs/resource/zh-CN/mini-app/develop/server/trade-system/general/common-param" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">通用参数-关于 xxx_entry_schema 的前置说明</a></p>
     */
    private RefundOrderEntrySchema order_entry_schema;
    /**
     * <p>退款原因，可填多个，不超过10个</p>
     */
    private List<RefundReason> refund_reason;

    public String getCp_extra() {
        return cp_extra;
    }

    public CreateRefundQuery setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public CreateRefundQuery setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public CreateRefundQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_refund_no() {
        return out_refund_no;
    }

    public CreateRefundQuery setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
        return this;
    }

    public Boolean getRefund_all() {
        return refund_all;
    }

    public CreateRefundQuery setRefund_all(Boolean refund_all) {
        this.refund_all = refund_all;
        return this;
    }

    public Long getRefund_total_amount() {
        return refund_total_amount;
    }

    public CreateRefundQuery setRefund_total_amount(Long refund_total_amount) {
        this.refund_total_amount = refund_total_amount;
        return this;
    }

    public List<RefundItemOrderDetail> getItem_order_detail() {
        return item_order_detail;
    }

    public CreateRefundQuery setItem_order_detail(List<RefundItemOrderDetail> item_order_detail) {
        this.item_order_detail = item_order_detail;
        return this;
    }

    public RefundOrderEntrySchema getOrder_entry_schema() {
        return order_entry_schema;
    }

    public CreateRefundQuery setOrder_entry_schema(RefundOrderEntrySchema order_entry_schema) {
        this.order_entry_schema = order_entry_schema;
        return this;
    }

    public List<RefundReason> getRefund_reason() {
        return refund_reason;
    }

    public CreateRefundQuery setRefund_reason(List<RefundReason> refund_reason) {
        this.refund_reason = refund_reason;
        return this;
    }

    public static CreateRefundQueryBuilder builder() {
        return new CreateRefundQueryBuilder();
    }

    public static final class CreateRefundQueryBuilder {
        private String cp_extra;
        private String notify_url;
        private String order_id;
        private String out_refund_no;
        private Boolean refund_all;
        private Long refund_total_amount;
        private List<RefundItemOrderDetail> item_order_detail;
        private RefundOrderEntrySchema order_entry_schema;
        private List<RefundReason> refund_reason;
        private Integer tenantId;
        private String clientKey;

        private CreateRefundQueryBuilder() {
        }

        public CreateRefundQueryBuilder cpExtra(String cpExtra) {
            this.cp_extra = cpExtra;
            return this;
        }

        public CreateRefundQueryBuilder notifyUrl(String notifyUrl) {
            this.notify_url = notifyUrl;
            return this;
        }

        public CreateRefundQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public CreateRefundQueryBuilder outRefundNo(String outRefundNo) {
            this.out_refund_no = outRefundNo;
            return this;
        }

        public CreateRefundQueryBuilder refundAll(Boolean refundAll) {
            this.refund_all = refundAll;
            return this;
        }

        public CreateRefundQueryBuilder refundTotalAmount(Long refundTotalAmount) {
            this.refund_total_amount = refundTotalAmount;
            return this;
        }

        public CreateRefundQueryBuilder itemOrderDetail(List<RefundItemOrderDetail> itemOrderDetail) {
            this.item_order_detail = itemOrderDetail;
            return this;
        }

        public CreateRefundQueryBuilder orderEntrySchema(RefundOrderEntrySchema orderEntrySchema) {
            this.order_entry_schema = orderEntrySchema;
            return this;
        }

        public CreateRefundQueryBuilder refundReason(List<RefundReason> refundReason) {
            this.refund_reason = refundReason;
            return this;
        }

        public CreateRefundQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateRefundQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateRefundQuery build() {
            CreateRefundQuery createRefundQuery = new CreateRefundQuery();
            createRefundQuery.setCp_extra(cp_extra);
            createRefundQuery.setNotify_url(notify_url);
            createRefundQuery.setOrder_id(order_id);
            createRefundQuery.setOut_refund_no(out_refund_no);
            createRefundQuery.setRefund_all(refund_all);
            createRefundQuery.setRefund_total_amount(refund_total_amount);
            createRefundQuery.setItem_order_detail(item_order_detail);
            createRefundQuery.setOrder_entry_schema(order_entry_schema);
            createRefundQuery.setRefund_reason(refund_reason);
            createRefundQuery.setTenantId(tenantId);
            createRefundQuery.setClientKey(clientKey);
            return createRefundQuery;
        }
    }
}
