package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class QueryPayOrderQuery extends BaseQuery {

    /**
     * <p>扣款单开发者的单号，长度<=64byte，pay_order_id 与 out_pay_order_no 二选一</p> 选填
     */
    private String out_pay_order_no;
    /**
     * <p>调用<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/server/management-capacity/auth-deposit/pay/create-pay-order" target="_blank" rel="nofollow" class="syl-link">发起扣款</a>接口后，会得到扣款单平台订单号（pay_order_id），此处查询扣款的接口需要传递该订单号，用来查询对应的订单。（pay_order_id 与 out_pay_order_no （开发者侧生产的订单号）二选一）</p> 选填
     */
    private String pay_order_id;

    public String getOut_pay_order_no() {
        return out_pay_order_no;
    }

    public QueryPayOrderQuery setOut_pay_order_no(String out_pay_order_no) {
        this.out_pay_order_no = out_pay_order_no;
        return this;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public QueryPayOrderQuery setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public static QueryPayOrderQueryBuilder builder() {
        return new QueryPayOrderQueryBuilder();
    }

    public static final class QueryPayOrderQueryBuilder {
        private String out_pay_order_no;
        private String pay_order_id;
        private Integer tenantId;
        private String clientKey;

        private QueryPayOrderQueryBuilder() {
        }

        public QueryPayOrderQueryBuilder outPayOrderNo(String outPayOrderNo) {
            this.out_pay_order_no = outPayOrderNo;
            return this;
        }

        public QueryPayOrderQueryBuilder payOrderId(String payOrderId) {
            this.pay_order_id = payOrderId;
            return this;
        }

        public QueryPayOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryPayOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryPayOrderQuery build() {
            QueryPayOrderQuery queryPayOrderQuery = new QueryPayOrderQuery();
            queryPayOrderQuery.setOut_pay_order_no(out_pay_order_no);
            queryPayOrderQuery.setPay_order_id(pay_order_id);
            queryPayOrderQuery.setTenantId(tenantId);
            queryPayOrderQuery.setClientKey(clientKey);
            return queryPayOrderQuery;
        }
    }
}
