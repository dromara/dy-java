package com.dyj.applet.domain;

import java.util.List;

public class BookInfo {

    /**
     * 预约单号，len <= 64 byte
     */
    private String book_id;

    /**
     * 商户单号，len <= 64 byte
     */
    private String order_id;

    /**
     * 预约状态
     * BOOKING 预约中
     * SUCCESS 预约成功
     * FINISH 预约完成(已核销）
     * CANCEL 预约取消
     * FAIL 预约失败
     */
    private String status;

    /**
     * 预约子单信息，详见BookChildInfo
     */
    private List<BookChildInfo> book_child_info_list;

    /**
     * 该预约单关联的加价单id，len <= 64 byte
     * 选填
     */
    private String markup_order_id;

    /**
     * 外部预约单号，len <= 64 byte
     */
    private String out_book_no;

    public String getBook_id() {
        return book_id;
    }

    public BookInfo setBook_id(String book_id) {
        this.book_id = book_id;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public BookInfo setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public BookInfo setStatus(String status) {
        this.status = status;
        return this;
    }

    public List<BookChildInfo> getBook_child_info_list() {
        return book_child_info_list;
    }

    public BookInfo setBook_child_info_list(List<BookChildInfo> book_child_info_list) {
        this.book_child_info_list = book_child_info_list;
        return this;
    }

    public String getMarkup_order_id() {
        return markup_order_id;
    }

    public BookInfo setMarkup_order_id(String markup_order_id) {
        this.markup_order_id = markup_order_id;
        return this;
    }

    public String getOut_book_no() {
        return out_book_no;
    }

    public BookInfo setOut_book_no(String out_book_no) {
        this.out_book_no = out_book_no;
        return this;
    }
}
