package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class ShortLiveDataWithIdQuery extends BaseQuery {

    /**
     * 开始时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
     */
    private Long start_time;

    /**
     * 结束时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据
     * query_bind_type=0，end_time就传2023-03-20 00:00:00对应的时间戳
     * query_bind_type=1，end_time就传2023-03-20 23:59:59对应的时间戳
     */
    private Long end_time;

    /**
     * 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；不传表示查全部端数据
     */
    private String host_name;

    /**
     * 查询数据的挂载类型，1-自挂载数据，0-自挂载+达人挂载数据，目前调用此open_api必须传0或1。传其他字段会产生非预期的结果
     */
    private Long query_bind_type;

    /**
     * 查询的抖音号列表，限制长度在200以内，即一次性最多查询 200个抖音号 所对应的自挂载,达人挂载视频/自挂载，达人挂载直播间所产生的数据
     */
    private List<String> aweme_short_id_list;

    /**
     * 查询的列表类型，传4表示查询短视频数据，传5表示查询直播间数据
     */
    private Integer query_data_type;
    /**
     * 查询的短视频列表，该参数和open_item_id_list参数长度合限制在1000以内，即一次性最多查询 1000个 自挂载，达人挂载短视频的详细数据
     */
    private List<String> item_id_list;
    /**
     * 查询的加密短视频列表，该参数和item_id_list参数长度合限制在1000以内，即一次性最多查询 1000个 自挂载，达人挂载该小程序的短视频所产生的数据
     */
    private List<String> open_item_id_list;
    /**
     * 查询的直播间列表，限制长度在1000以内，即一次性最多查询 1000个 自挂载，达人挂载直播间的详细数据
     */
    private List<String> room_id_list;

    /**
     * 查询页号，从1开始
     */
    private Integer page_no;

    /**
     * 每页的大小（20以内），分页结果按短视频发布时间or直播间开播时间倒序排列（如果出现系统错误，建议调小分页大小获取）
     */
    private Integer page_size;

    public ShortLiveDataWithIdQueryBuilder builder() {
        return new ShortLiveDataWithIdQueryBuilder();
    }

    public static class ShortLiveDataWithIdQueryBuilder {
        /**
         * 开始时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询2023年3月20号以来的数据，start_time就传2023-03-20 00:00:00对应的时间戳
         */
        private Long startTime;

        /**
         * 结束时间，必须是昨天以前的某个时间戳，传入当天的整点时间戳，例如要查询截止到2023年3月20号的数据
         * query_bind_type=0，end_time就传2023-03-20 00:00:00对应的时间戳
         * query_bind_type=1，end_time就传2023-03-20 23:59:59对应的时间戳
         */
        private Long endTime;

        /**
         * 宿主APP，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条；tt_lite-今日头条lite；huoshan-抖音火山版；不传表示查全部端数据
         */
        private String hostName;

        /**
         * 查询数据的挂载类型，1-自挂载数据，0-自挂载+达人挂载数据，目前调用此open_api必须传0或1。传其他字段会产生非预期的结果
         */
        private Long queryBindType;

        /**
         * 查询的抖音号列表，限制长度在200以内，即一次性最多查询 200个抖音号 所对应的自挂载,达人挂载视频/自挂载，达人挂载直播间所产生的数据
         */
        private List<String> awemeShortIdList;

        /**
         * 查询的列表类型，传4表示查询短视频数据，传5表示查询直播间数据
         */
        private Integer queryDataType;
        /**
         * 查询的短视频列表，该参数和open_item_id_list参数长度合限制在1000以内，即一次性最多查询 1000个 自挂载，达人挂载短视频的详细数据
         */
        private List<String> itemIdList;
        /**
         * 查询的加密短视频列表，该参数和item_id_list参数长度合限制在1000以内，即一次性最多查询 1000个 自挂载，达人挂载该小程序的短视频所产生的数据
         */
        private List<String> openItemIdList;
        /**
         * 查询的直播间列表，限制长度在1000以内，即一次性最多查询 1000个 自挂载，达人挂载直播间的详细数据
         */
        private List<String> roomIdList;

        /**
         * 查询页号，从1开始
         */
        private Integer pageNo;

        /**
         * 每页的大小（20以内），分页结果按短视频发布时间or直播间开播时间倒序排列（如果出现系统错误，建议调小分页大小获取）
         */
        private Integer pageSize;

        /**
         * 租户ID
         */
        private Integer tenantId;
        /**
         * 应用Key
         */
        private String clientKey;

        public ShortLiveDataWithIdQuery build() {
            ShortLiveDataWithIdQuery shortLiveDataWithIdQuery = new ShortLiveDataWithIdQuery();
            shortLiveDataWithIdQuery.setStart_time(startTime);
            shortLiveDataWithIdQuery.setEnd_time(endTime);
            shortLiveDataWithIdQuery.setHost_name(hostName);
            shortLiveDataWithIdQuery.setQuery_bind_type(queryBindType);
            shortLiveDataWithIdQuery.setAweme_short_id_list(awemeShortIdList);
            shortLiveDataWithIdQuery.setQuery_data_type(queryDataType);
            shortLiveDataWithIdQuery.setItem_id_list(itemIdList);
            shortLiveDataWithIdQuery.setOpen_item_id_list(openItemIdList);
            shortLiveDataWithIdQuery.setRoom_id_list(roomIdList);
            shortLiveDataWithIdQuery.setPage_no(pageNo);
            shortLiveDataWithIdQuery.setPage_size(pageSize);
            shortLiveDataWithIdQuery.setTenantId(tenantId);
            shortLiveDataWithIdQuery.setClientKey(clientKey);
            return shortLiveDataWithIdQuery;
        }
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public String getHost_name() {
        return host_name;
    }

    public void setHost_name(String host_name) {
        this.host_name = host_name;
    }

    public Long getQuery_bind_type() {
        return query_bind_type;
    }

    public void setQuery_bind_type(Long query_bind_type) {
        this.query_bind_type = query_bind_type;
    }

    public List<String> getAweme_short_id_list() {
        return aweme_short_id_list;
    }

    public void setAweme_short_id_list(List<String> aweme_short_id_list) {
        this.aweme_short_id_list = aweme_short_id_list;
    }

    public Integer getQuery_data_type() {
        return query_data_type;
    }

    public void setQuery_data_type(Integer query_data_type) {
        this.query_data_type = query_data_type;
    }

    public List<String> getItem_id_list() {
        return item_id_list;
    }

    public void setItem_id_list(List<String> item_id_list) {
        this.item_id_list = item_id_list;
    }

    public List<String> getOpen_item_id_list() {
        return open_item_id_list;
    }

    public void setOpen_item_id_list(List<String> open_item_id_list) {
        this.open_item_id_list = open_item_id_list;
    }

    public List<String> getRoom_id_list() {
        return room_id_list;
    }

    public void setRoom_id_list(List<String> room_id_list) {
        this.room_id_list = room_id_list;
    }

    public Integer getPage_no() {
        return page_no;
    }

    public void setPage_no(Integer page_no) {
        this.page_no = page_no;
    }

    public Integer getPage_size() {
        return page_size;
    }

    public void setPage_size(Integer page_size) {
        this.page_size = page_size;
    }
}
