package com.dyj.applet.domain;

/**
 * 商品信息
 */
public class TradeOrderGoodsInfo {

    /**
     * 商品图片链接，长度 <= 512 byte
     * 注意：非 POI 商品必传
     */
    private String goods_image;

    /**
     * 商品标题/商品名称，长度 <= 256 byte
     * 注意：非 POI 商品必传
     */
    private String goods_title;

    /**
     * 商品标签，最多设置三个标签，例如：随时退｜免预约｜提前3日预约（“｜”是中文类型），详见 pay-button 支付 的 type 的合法值 部分，注意不是 good-type 的合法值。
     * 注意：非 POI 商品必传
     */
    private String labels;

    /**
     * 使用规则，如 “周一至周日可用”、“周一至周五可用”、“非节假日可用”，默认“周一至周日可用”
     */
    private String date_rule;

    /**
     * 商品价格，单位（分）
     * 注意：非 POI 商品必传
     */
    private Long price;

    /**
     * 商品数量
     */
    private Integer quantity;

    /**
     * 商品 id
     */
    private String goods_id;

    /**
     * 商品 id 类别，
     * POI 商品传 1
     * 非 POI 商品传 2
     */
    private Integer goods_id_type;

    /**
     * 商品详情页
     */
    private IndustryOrderGoodsPage goods_page;

    /**
     * 券的有效期
     */
    private IndustryOrderValidTime order_valid_time;

    /**
     * 折扣金额，单位分
     */
    private Long discount_amount;

    /**
     * 预约信息
     * 选填
     */
    private TradeOrderGoodsInfo goods_book_info;

    /**
     * 开发者自定义收款商户号，须申请白名单
     * 选填
     */
    private String merchant_uid;

    public String getGoods_image() {
        return goods_image;
    }

    public TradeOrderGoodsInfo setGoods_image(String goods_image) {
        this.goods_image = goods_image;
        return this;
    }

    public String getGoods_title() {
        return goods_title;
    }

    public TradeOrderGoodsInfo setGoods_title(String goods_title) {
        this.goods_title = goods_title;
        return this;
    }

    public String getLabels() {
        return labels;
    }

    public TradeOrderGoodsInfo setLabels(String labels) {
        this.labels = labels;
        return this;
    }

    public String getDate_rule() {
        return date_rule;
    }

    public TradeOrderGoodsInfo setDate_rule(String date_rule) {
        this.date_rule = date_rule;
        return this;
    }

    public Long getPrice() {
        return price;
    }

    public TradeOrderGoodsInfo setPrice(Long price) {
        this.price = price;
        return this;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public TradeOrderGoodsInfo setQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public TradeOrderGoodsInfo setGoods_id(String goods_id) {
        this.goods_id = goods_id;
        return this;
    }

    public Integer getGoods_id_type() {
        return goods_id_type;
    }

    public TradeOrderGoodsInfo setGoods_id_type(Integer goods_id_type) {
        this.goods_id_type = goods_id_type;
        return this;
    }

    public IndustryOrderGoodsPage getGoods_page() {
        return goods_page;
    }

    public TradeOrderGoodsInfo setGoods_page(IndustryOrderGoodsPage goods_page) {
        this.goods_page = goods_page;
        return this;
    }

    public IndustryOrderValidTime getOrder_valid_time() {
        return order_valid_time;
    }

    public TradeOrderGoodsInfo setOrder_valid_time(IndustryOrderValidTime order_valid_time) {
        this.order_valid_time = order_valid_time;
        return this;
    }

    public Long getDiscount_amount() {
        return discount_amount;
    }

    public TradeOrderGoodsInfo setDiscount_amount(Long discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public TradeOrderGoodsInfo getGoods_book_info() {
        return goods_book_info;
    }

    public TradeOrderGoodsInfo setGoods_book_info(TradeOrderGoodsInfo goods_book_info) {
        this.goods_book_info = goods_book_info;
        return this;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public TradeOrderGoodsInfo setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
        return this;
    }
}
