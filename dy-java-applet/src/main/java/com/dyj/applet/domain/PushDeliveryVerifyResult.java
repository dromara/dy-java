package com.dyj.applet.domain;

/**
 * 每个券的验券结果
 * @author ws
 **/
public class PushDeliveryVerifyResult {

    /**
     * 代表一张券码的标识
     */
    private String certificate_id;
    /**
     * 交易系统里对应的商品单 id
     */
    private String item_order_id;
    /**
     * 代表券码一次核销的唯一标识。开发者可用于撤销核销
     */
    private String verify_id;
    /**
     * 核销时间，13 位毫秒级时间戳
     */
    private Long verify_time;

    public String getCertificate_id() {
        return certificate_id;
    }

    public PushDeliveryVerifyResult setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public PushDeliveryVerifyResult setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public String getVerify_id() {
        return verify_id;
    }

    public PushDeliveryVerifyResult setVerify_id(String verify_id) {
        this.verify_id = verify_id;
        return this;
    }

    public Long getVerify_time() {
        return verify_time;
    }

    public PushDeliveryVerifyResult setVerify_time(Long verify_time) {
        this.verify_time = verify_time;
        return this;
    }
}
