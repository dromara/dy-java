package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-24 16:24
 **/
public class AuditTemplateInfo {
    /**
     * 模版id
     */
    private Long id;
    /**
     * 模版名称
     */
    private String title;
    /**
     * 模版描述
     */
    private String desc;

    private List<AuditTemplateContent> template_content;

    public static class AuditTemplateInfoBuilder {
        private Long id;
        private String title;
        private String desc;
        private List<AuditTemplateContent> templateContent;

        public AuditTemplateInfoBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public AuditTemplateInfoBuilder title(String title) {
            this.title = title;
            return this;
        }

        public AuditTemplateInfoBuilder desc(String desc) {
            this.desc = desc;
            return this;
        }

        public AuditTemplateInfoBuilder templateContent(List<AuditTemplateContent> templateContent) {
            this.templateContent = templateContent;
            return this;
        }


        public AuditTemplateInfo build() {
            AuditTemplateInfo auditTemplateInfo = new AuditTemplateInfo();
            auditTemplateInfo.setId(id);
            auditTemplateInfo.setTitle(title);
            auditTemplateInfo.setDesc(desc);
            auditTemplateInfo.setTemplate_content(templateContent);
            return auditTemplateInfo;
        }
    }

    public static AuditTemplateInfoBuilder builder() {
        return new AuditTemplateInfoBuilder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<AuditTemplateContent> getTemplate_content() {
        return template_content;
    }

    public void setTemplate_content(List<AuditTemplateContent> template_content) {
        this.template_content = template_content;
    }
}
