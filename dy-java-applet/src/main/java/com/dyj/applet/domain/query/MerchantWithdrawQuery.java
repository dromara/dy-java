package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 商户提现请求值
 */
public class MerchantWithdrawQuery extends BaseTransactionMerchantQuery {


    /**
     * <p>小程序的 app_id；除服务商为自己提现外，其他情况必填</p> 选填
     */
    private String app_id;
    /**
     * <p>提现结果通知接口（开发者自己的https服务）；如果不传默认用支付设置中的回调地址</p> 选填
     */
    private String callback;
    /**
     * <p>提现渠道枚举值:</p><ul><li>alipay: 支付宝</li><li>wx: 微信</li><li>hz: 抖音支付</li><li>yeepay: 易宝</li><li>yzt: 担保支付企业版聚合账户</li></ul>
     */
    private String channel_type;
    /**
     * <p>向开发者回调时，会原样存储在回调参数的extra字段中返回。</p> 选填
     */
    private String cp_extra;
    /**
     * <p>抖音信息和光合信号标识:</p><ul><li>不传或传0或1 按抖音信息提现；</li><li>传2按光合信号提现。</li></ul><p>注意：channel_type为yzt时需要传2</p> 选填
     */
    private Integer merchant_entity;
    /**
     * <p>进件完成返回的商户号</p>
     */
    private String merchant_uid;
    /**
     * <p>外部单号（开发者侧）；唯一标识一笔提现请求</p>
     */
    private String out_order_id;
    /**
     * <p>小程序第三方平台应用 id。服务商发起提现请求的必填</p> 选填
     */
    private String thirdparty_id;
    /**
     * <p>提现金额；单位分</p>
     */
    private Long withdraw_amount;

    public String getApp_id() {
        return app_id;
    }

    public MerchantWithdrawQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getCallback() {
        return callback;
    }

    public MerchantWithdrawQuery setCallback(String callback) {
        this.callback = callback;
        return this;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public MerchantWithdrawQuery setChannel_type(String channel_type) {
        this.channel_type = channel_type;
        return this;
    }

    public String getCp_extra() {
        return cp_extra;
    }

    public MerchantWithdrawQuery setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public Integer getMerchant_entity() {
        return merchant_entity;
    }

    public MerchantWithdrawQuery setMerchant_entity(Integer merchant_entity) {
        this.merchant_entity = merchant_entity;
        return this;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public MerchantWithdrawQuery setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
        return this;
    }

    public String getOut_order_id() {
        return out_order_id;
    }

    public MerchantWithdrawQuery setOut_order_id(String out_order_id) {
        this.out_order_id = out_order_id;
        return this;
    }

    public String getThirdparty_id() {
        return thirdparty_id;
    }

    public MerchantWithdrawQuery setThirdparty_id(String thirdparty_id) {
        this.thirdparty_id = thirdparty_id;
        return this;
    }

    public Long getWithdraw_amount() {
        return withdraw_amount;
    }

    public MerchantWithdrawQuery setWithdraw_amount(Long withdraw_amount) {
        this.withdraw_amount = withdraw_amount;
        return this;
    }

    public static MerchantWithdrawQueryBuilder builder() {
        return new MerchantWithdrawQueryBuilder();
    }

    public static final class MerchantWithdrawQueryBuilder {
        private String app_id;
        private String callback;
        private String channel_type;
        private String cp_extra;
        private Integer merchant_entity;
        private String merchant_uid;
        private String out_order_id;
        private String thirdparty_id;
        private Long withdraw_amount;
        private Integer tenantId;
        private String clientKey;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType;

        private MerchantWithdrawQueryBuilder() {
        }

        public MerchantWithdrawQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public MerchantWithdrawQueryBuilder callback(String callback) {
            this.callback = callback;
            return this;
        }

        public MerchantWithdrawQueryBuilder channelType(String channelType) {
            this.channel_type = channelType;
            return this;
        }

        public MerchantWithdrawQueryBuilder cpExtra(String cpExtra) {
            this.cp_extra = cpExtra;
            return this;
        }

        public MerchantWithdrawQueryBuilder merchantEntity(Integer merchantEntity) {
            this.merchant_entity = merchantEntity;
            return this;
        }

        public MerchantWithdrawQueryBuilder merchantUid(String merchantUid) {
            this.merchant_uid = merchantUid;
            return this;
        }

        public MerchantWithdrawQueryBuilder outOrderId(String outOrderId) {
            this.out_order_id = outOrderId;
            return this;
        }

        public MerchantWithdrawQueryBuilder thirdpartyId(String thirdpartyId) {
            this.thirdparty_id = thirdpartyId;
            return this;
        }

        public MerchantWithdrawQueryBuilder withdrawAmount(Long withdrawAmount) {
            this.withdraw_amount = withdrawAmount;
            return this;
        }

        public MerchantWithdrawQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public MerchantWithdrawQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public MerchantWithdrawQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public MerchantWithdrawQuery build() {
            MerchantWithdrawQuery merchantWithdrawQuery = new MerchantWithdrawQuery();
            merchantWithdrawQuery.setApp_id(app_id);
            merchantWithdrawQuery.setCallback(callback);
            merchantWithdrawQuery.setChannel_type(channel_type);
            merchantWithdrawQuery.setCp_extra(cp_extra);
            merchantWithdrawQuery.setMerchant_entity(merchant_entity);
            merchantWithdrawQuery.setMerchant_uid(merchant_uid);
            merchantWithdrawQuery.setOut_order_id(out_order_id);
            merchantWithdrawQuery.setThirdparty_id(thirdparty_id);
            merchantWithdrawQuery.setWithdraw_amount(withdraw_amount);
            merchantWithdrawQuery.setTenantId(tenantId);
            merchantWithdrawQuery.setClientKey(clientKey);
            merchantWithdrawQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            return merchantWithdrawQuery;
        }
    }
}
