package com.dyj.applet.domain;

import java.util.List;

public class QueryCreatedSubscriptionTemplate {

    /**
     * 订阅消息类型
     */
    private Integer classification;
    /**
     * 审核拒绝原因 选填
     */
    private String fail_reason;
    /**
     * 操作时间，时间戳
     */
    private Long operating_time;
    /**
     * 审核状态
     */
    private Integer status;
    /**
     * 模板标题
     */
    private String title;
    /**
     * 模板适用的宿主APP列表，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条
     */
    private List<String> host_list;
    /**
     * 模板关键词列表
     */
    private List<String> keyword_list;

    public Integer getClassification() {
        return classification;
    }

    public QueryCreatedSubscriptionTemplate setClassification(Integer classification) {
        this.classification = classification;
        return this;
    }

    public String getFail_reason() {
        return fail_reason;
    }

    public QueryCreatedSubscriptionTemplate setFail_reason(String fail_reason) {
        this.fail_reason = fail_reason;
        return this;
    }

    public Long getOperating_time() {
        return operating_time;
    }

    public QueryCreatedSubscriptionTemplate setOperating_time(Long operating_time) {
        this.operating_time = operating_time;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public QueryCreatedSubscriptionTemplate setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public QueryCreatedSubscriptionTemplate setTitle(String title) {
        this.title = title;
        return this;
    }

    public List<String> getHost_list() {
        return host_list;
    }

    public QueryCreatedSubscriptionTemplate setHost_list(List<String> host_list) {
        this.host_list = host_list;
        return this;
    }

    public List<String> getKeyword_list() {
        return keyword_list;
    }

    public QueryCreatedSubscriptionTemplate setKeyword_list(List<String> keyword_list) {
        this.keyword_list = keyword_list;
        return this;
    }
}
