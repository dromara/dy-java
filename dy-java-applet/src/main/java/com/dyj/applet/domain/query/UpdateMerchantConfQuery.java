package com.dyj.applet.domain.query;

import com.dyj.applet.domain.DeliveryAppInfo;
import com.dyj.applet.domain.ProductDoubleOpenInfo;
import com.dyj.common.domain.query.BaseQuery;

public class UpdateMerchantConfQuery extends BaseQuery {

    /**
     * 商家绑定的小程序类型,0-核销工具，1-混合双开
     */
    private String bind_biz_type;

    /**
     * 商家id
     */
    private String account_id;

    /**
     * bind_biz_type=0时需填写，核销工具相关信息
     */
    private DeliveryAppInfo delivery_app_info;

    /**
     *
     * bind_biz_type=1时需填写，混合双开相关信息
     */
    private ProductDoubleOpenInfo product_double_open_info;

    public String getBind_biz_type() {
        return bind_biz_type;
    }

    public UpdateMerchantConfQuery setBind_biz_type(String bind_biz_type) {
        this.bind_biz_type = bind_biz_type;
        return this;
    }

    public String getAccount_id() {
        return account_id;
    }

    public UpdateMerchantConfQuery setAccount_id(String account_id) {
        this.account_id = account_id;
        return this;
    }

    public DeliveryAppInfo getDelivery_app_info() {
        return delivery_app_info;
    }

    public UpdateMerchantConfQuery setDelivery_app_info(DeliveryAppInfo delivery_app_info) {
        this.delivery_app_info = delivery_app_info;
        return this;
    }

    public ProductDoubleOpenInfo getProduct_double_open_info() {
        return product_double_open_info;
    }

    public UpdateMerchantConfQuery setProduct_double_open_info(ProductDoubleOpenInfo product_double_open_info) {
        this.product_double_open_info = product_double_open_info;
        return this;
    }

    public static UpdateMerchantConfQueryBuilder builder() {
        return new UpdateMerchantConfQueryBuilder();
    }

    public static final class UpdateMerchantConfQueryBuilder {
        private String bind_biz_type;
        private String account_id;
        private DeliveryAppInfo delivery_app_info;
        private ProductDoubleOpenInfo product_double_open_info;
        private Integer tenantId;
        private String clientKey;

        private UpdateMerchantConfQueryBuilder() {
        }

        public UpdateMerchantConfQueryBuilder bindBizType(String bindBizType) {
            this.bind_biz_type = bindBizType;
            return this;
        }

        public UpdateMerchantConfQueryBuilder accountId(String accountId) {
            this.account_id = accountId;
            return this;
        }

        public UpdateMerchantConfQueryBuilder deliveryAppInfo(DeliveryAppInfo deliveryAppInfo) {
            this.delivery_app_info = deliveryAppInfo;
            return this;
        }

        public UpdateMerchantConfQueryBuilder productDoubleOpenInfo(ProductDoubleOpenInfo productDoubleOpenInfo) {
            this.product_double_open_info = productDoubleOpenInfo;
            return this;
        }

        public UpdateMerchantConfQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public UpdateMerchantConfQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public UpdateMerchantConfQuery build() {
            UpdateMerchantConfQuery updateMerchantConfQuery = new UpdateMerchantConfQuery();
            updateMerchantConfQuery.setBind_biz_type(bind_biz_type);
            updateMerchantConfQuery.setAccount_id(account_id);
            updateMerchantConfQuery.setDelivery_app_info(delivery_app_info);
            updateMerchantConfQuery.setProduct_double_open_info(product_double_open_info);
            updateMerchantConfQuery.setTenantId(tenantId);
            updateMerchantConfQuery.setClientKey(clientKey);
            return updateMerchantConfQuery;
        }
    }
}
