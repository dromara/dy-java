package com.dyj.applet.domain.vo;

import java.util.List;

public class GoodsDiscountDetail {

    /**
     * 商品 id，此处为课程 id，注意这里是 string 类型
     */
    private String goods_id;

    /**
     * 购买数量
     */
    private Integer quantity;

    /**
     * 商品总价，单位分
     */
    private Integer total_amount;

    /**
     * 该商品总优惠金额，该商品的实付金额 = total_amount - total_discount_amount
     */
    private Integer total_discount_amount;

    /**
     * 营销明细
     * 选填
     */
    private List<MarketingDetailInfo> marketing_detail_info;

    public String getGoods_id() {
        return goods_id;
    }

    public GoodsDiscountDetail setGoods_id(String goods_id) {
        this.goods_id = goods_id;
        return this;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public GoodsDiscountDetail setQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public Integer getTotal_amount() {
        return total_amount;
    }

    public GoodsDiscountDetail setTotal_amount(Integer total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public Integer getTotal_discount_amount() {
        return total_discount_amount;
    }

    public GoodsDiscountDetail setTotal_discount_amount(Integer total_discount_amount) {
        this.total_discount_amount = total_discount_amount;
        return this;
    }

    public List<MarketingDetailInfo> getMarketing_detail_info() {
        return marketing_detail_info;
    }

    public GoodsDiscountDetail setMarketing_detail_info(List<MarketingDetailInfo> marketing_detail_info) {
        this.marketing_detail_info = marketing_detail_info;
        return this;
    }
}
