package com.dyj.applet.domain;

/**
 * 商品维度营销查询结果，包含商品信息以及该商品下可用/不可用营销信息
 */
public class MarketingGoodsMarketingResult {

    /**
     * 商品id
     */
    private String goods_id;

    /**
     * 购买数量
     */
    private Long quantity;

    /**
     * 商品总价，单位分
     */
    private Long total_amount;

    /**
     * 该商品下可用的优惠信息（商品维度）选填
     */
    private MarketingBundle available_marketing;

    /**
     * 该商品下不可用的优惠信息（商品维度）选填
     */
    private MarketingBundle unavailable_marketing;

    public String getGoods_id() {
        return goods_id;
    }

    public MarketingGoodsMarketingResult setGoods_id(String goods_id) {
        this.goods_id = goods_id;
        return this;
    }

    public Long getQuantity() {
        return quantity;
    }

    public MarketingGoodsMarketingResult setQuantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public MarketingGoodsMarketingResult setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public MarketingBundle getAvailable_marketing() {
        return available_marketing;
    }

    public MarketingGoodsMarketingResult setAvailable_marketing(MarketingBundle available_marketing) {
        this.available_marketing = available_marketing;
        return this;
    }

    public MarketingBundle getUnavailable_marketing() {
        return unavailable_marketing;
    }

    public MarketingGoodsMarketingResult setUnavailable_marketing(MarketingBundle unavailable_marketing) {
        this.unavailable_marketing = unavailable_marketing;
        return this;
    }
}
