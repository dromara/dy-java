package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 11:29
 **/
public class ProductAttr {

    /**
     * 属性描述
     */
    private String desc;
    /**
     *
     * 是否列表
     */
    private Boolean is_multi;
    /**
     * 是否必填
     */
    private Boolean is_required;
    /**
     * 属性key
     */
    private String key;
    /**
     * 属性名称
     */
    private String name;
    /**
     * 属性类型
     */
    private String value_type;
    /**
     * 属性样例
     */
    private String value_demo;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Boolean getIs_multi() {
        return is_multi;
    }

    public void setIs_multi(Boolean is_multi) {
        this.is_multi = is_multi;
    }

    public Boolean getIs_required() {
        return is_required;
    }

    public void setIs_required(Boolean is_required) {
        this.is_required = is_required;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue_type() {
        return value_type;
    }

    public void setValue_type(String value_type) {
        this.value_type = value_type;
    }

    public String getValue_demo() {
        return value_demo;
    }

    public void setValue_demo(String value_demo) {
        this.value_demo = value_demo;
    }
}
