package com.dyj.applet.domain.vo;

/**
 * 商户提现返回值
 */
public class MerchantWithdrawVo {

    /**
     * <p>抖音信息和光合信号标识</p><ul><li>1: 抖音信息提现</li><li>2: 光合信号提现</li></ul>
     */
    private Integer merchant_entity;
    /**
     * <p>平台侧的受理单号</p>
     */
    private String order_id;

    public Integer getMerchant_entity() {
        return merchant_entity;
    }

    public MerchantWithdrawVo setMerchant_entity(Integer merchant_entity) {
        this.merchant_entity = merchant_entity;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public MerchantWithdrawVo setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }
}
