package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.CommonPlanTalentMedial;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-13 15:57
 **/
public class CommonPlanTalentMedialVo {

    /**
     * 总页数
     */
    private Integer page_count;
    /**
     * 短视频/直播间总数
     */
    private Integer total_count;

    /**
     * 数据产出日期
     */
    private String date;

    private List<CommonPlanTalentMedial> data;


    public Integer getPage_count() {
        return page_count;
    }

    public void setPage_count(Integer page_count) {
        this.page_count = page_count;
    }

    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<CommonPlanTalentMedial> getData() {
        return data;
    }

    public void setData(List<CommonPlanTalentMedial> data) {
        this.data = data;
    }
}
