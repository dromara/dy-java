package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.ImageCensorPredicts;

import java.util.List;

public class ImageCensorVo {

    /**
     *  选填
     */
    private String err_msg;
    /**
     *  选填
     */
    private Integer err_no;
    /**
     *  选填
     */
    private String log_id;
    /**
     *  选填
     */
    private List<ImageCensorPredicts> predicts;

    public String getErr_msg() {
        return err_msg;
    }

    public ImageCensorVo setErr_msg(String err_msg) {
        this.err_msg = err_msg;
        return this;
    }

    public Integer getErr_no() {
        return err_no;
    }

    public ImageCensorVo setErr_no(Integer err_no) {
        this.err_no = err_no;
        return this;
    }

    public String getLog_id() {
        return log_id;
    }

    public ImageCensorVo setLog_id(String log_id) {
        this.log_id = log_id;
        return this;
    }

    public List<ImageCensorPredicts> getPredicts() {
        return predicts;
    }

    public ImageCensorVo setPredicts(List<ImageCensorPredicts> predicts) {
        this.predicts = predicts;
        return this;
    }
}
