package com.dyj.applet.domain;

public class LiveRoomData {
    /**
     * 主播名称
     */
    private String anchor_name;
    /**
     * 直播间开播时间
     */
    private Long create_time;
    /**
     * 直播间ID
     */
    private Long live_room_id;
    /**
     * 直播间标题
     */
    private String live_room_name;

    public String getAnchor_name() {
        return anchor_name;
    }

    public void setAnchor_name(String anchor_name) {
        this.anchor_name = anchor_name;
    }

    public Long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Long create_time) {
        this.create_time = create_time;
    }

    public Long getLive_room_id() {
        return live_room_id;
    }

    public void setLive_room_id(Long live_room_id) {
        this.live_room_id = live_room_id;
    }

    public String getLive_room_name() {
        return live_room_name;
    }

    public void setLive_room_name(String live_room_name) {
        this.live_room_name = live_room_name;
    }
}
