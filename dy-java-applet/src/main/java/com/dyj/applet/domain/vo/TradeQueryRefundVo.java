package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.TradeQueryRefundResult;

import java.util.List;

public class TradeQueryRefundVo {

    private List<TradeQueryRefundResult> refund_list;

    public List<TradeQueryRefundResult> getRefund_list() {
        return refund_list;
    }

    public TradeQueryRefundVo setRefund_list(List<TradeQueryRefundResult> refund_list) {
        this.refund_list = refund_list;
        return this;
    }
}
