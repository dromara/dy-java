package com.dyj.applet.domain;

/**
 * 创建开发者接口发券活动 活动id
 */
public class CreateDeveloperActivityActivityId {

    /**
     * 活动id
     */
    private String activity_id;

    public String getActivity_id() {
        return activity_id;
    }

    public CreateDeveloperActivityActivityId setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }
}
