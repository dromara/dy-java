package com.dyj.applet.domain;

import java.util.List;

/**
 * 用户所选营销策略信息，仅包含订单维度优惠信息
 */
public class MarketingBundle {

    /**
     * 用户身份信息(如会员) 选填
     */
    private List<MarketingMembershipInfo> membership_info;

    /**
     * 优惠券信息 选填
     */
    private List<MarketingCouponInfo> coupon_info;

    /**
     * 活动信息 选填
     */
    private List<MarketingActivityInfo> activity_info;

    /**
     * 积分信息
     */
    private List<MarketingScoreInfo> score_info;

    public List<MarketingMembershipInfo> getMembership_info() {
        return membership_info;
    }

    public MarketingBundle setMembership_info(List<MarketingMembershipInfo> membership_info) {
        this.membership_info = membership_info;
        return this;
    }

    public List<MarketingCouponInfo> getCoupon_info() {
        return coupon_info;
    }

    public MarketingBundle setCoupon_info(List<MarketingCouponInfo> coupon_info) {
        this.coupon_info = coupon_info;
        return this;
    }

    public List<MarketingActivityInfo> getActivity_info() {
        return activity_info;
    }

    public MarketingBundle setActivity_info(List<MarketingActivityInfo> activity_info) {
        this.activity_info = activity_info;
        return this;
    }

    public List<MarketingScoreInfo> getScore_info() {
        return score_info;
    }

    public MarketingBundle setScore_info(List<MarketingScoreInfo> score_info) {
        this.score_info = score_info;
        return this;
    }
}
