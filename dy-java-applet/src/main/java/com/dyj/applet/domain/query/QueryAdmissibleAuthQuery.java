package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

/**
 * 信用准入查询请求值
 */
public class QueryAdmissibleAuthQuery extends UserInfoQuery {

    /**
     * <p>该笔交易卖家商户号</p> 选填
     */
    private String merchant_uid;

    /**
     * <p>免押场景</p><p>1-租车</p><p>2-租物</p>
     */
    private Long scene;
    /**
     * <p>信用模板ID，请参考接入指南，完成进件和签约后获取信用模板ID</p>
     */
    private String service_id;
    /**
     * <p>需要交付的押金金额，单位分</p>
     */
    private Long total_amount;

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public QueryAdmissibleAuthQuery setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
        return this;
    }

    public Long getScene() {
        return scene;
    }

    public QueryAdmissibleAuthQuery setScene(Long scene) {
        this.scene = scene;
        return this;
    }

    public String getService_id() {
        return service_id;
    }

    public QueryAdmissibleAuthQuery setService_id(String service_id) {
        this.service_id = service_id;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public QueryAdmissibleAuthQuery setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public static QueryAdmissibleAuthQueryBuilder builder() {
        return new QueryAdmissibleAuthQueryBuilder();
    }

    public static final class QueryAdmissibleAuthQueryBuilder {
        private String merchant_uid;
        private Long scene;
        private String service_id;
        private Long total_amount;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private QueryAdmissibleAuthQueryBuilder() {
        }

        public QueryAdmissibleAuthQueryBuilder merchantUid(String merchantUid) {
            this.merchant_uid = merchantUid;
            return this;
        }

        public QueryAdmissibleAuthQueryBuilder scene(Long scene) {
            this.scene = scene;
            return this;
        }

        public QueryAdmissibleAuthQueryBuilder serviceId(String serviceId) {
            this.service_id = serviceId;
            return this;
        }

        public QueryAdmissibleAuthQueryBuilder totalAmount(Long totalAmount) {
            this.total_amount = totalAmount;
            return this;
        }

        public QueryAdmissibleAuthQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public QueryAdmissibleAuthQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryAdmissibleAuthQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryAdmissibleAuthQuery build() {
            QueryAdmissibleAuthQuery queryAdmissibleAuthQuery = new QueryAdmissibleAuthQuery();
            queryAdmissibleAuthQuery.setMerchant_uid(merchant_uid);
            queryAdmissibleAuthQuery.setScene(scene);
            queryAdmissibleAuthQuery.setService_id(service_id);
            queryAdmissibleAuthQuery.setTotal_amount(total_amount);
            queryAdmissibleAuthQuery.setOpen_id(open_id);
            queryAdmissibleAuthQuery.setTenantId(tenantId);
            queryAdmissibleAuthQuery.setClientKey(clientKey);
            return queryAdmissibleAuthQuery;
        }
    }
}
