package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

import java.util.List;
import java.util.Map;

public class AddFunctionConfigQuery extends BaseTransactionMerchantQuery {

    /**
     * 功能配置列表
     */
    private Map<String, List<String>> scene_function_config;

    public Map<String, List<String>> getScene_function_config() {
        return scene_function_config;
    }

    public AddFunctionConfigQuery setScene_function_config(Map<String, List<String>> scene_function_config) {
        this.scene_function_config = scene_function_config;
        return this;
    }

    public static AddFunctionConfigQueryBuilder builder() {
        return new AddFunctionConfigQueryBuilder();
    }

    public static final class AddFunctionConfigQueryBuilder {
        private Map<String, List<String>> scene_function_config;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType;
        private Integer tenantId;
        private String clientKey;

        private AddFunctionConfigQueryBuilder() {
        }

        public AddFunctionConfigQueryBuilder sceneFunctionConfig(Map<String, List<String>> sceneFunctionConfig) {
            this.scene_function_config = sceneFunctionConfig;
            return this;
        }

        public AddFunctionConfigQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public AddFunctionConfigQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public AddFunctionConfigQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public AddFunctionConfigQuery build() {
            AddFunctionConfigQuery addFunctionConfigQuery = new AddFunctionConfigQuery();
            addFunctionConfigQuery.setScene_function_config(scene_function_config);
            addFunctionConfigQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            addFunctionConfigQuery.setTenantId(tenantId);
            addFunctionConfigQuery.setClientKey(clientKey);
            return addFunctionConfigQuery;
        }
    }
}
