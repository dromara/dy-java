package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 撤销核销请求值
 */
public class VerifyCancelQuery extends BaseQuery {

    /**
     * 代表一张券码的标识
     */
    private String certificate_id;
    /**
     * 需要撤销的certificate_id所属的订单id
     */
    private String order_id;
    /**
     * 代表券码一次核销的唯一标识（验券时返回）
     */
    private String verify_id;

    public String getCertificate_id() {
        return certificate_id;
    }

    public VerifyCancelQuery setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public VerifyCancelQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getVerify_id() {
        return verify_id;
    }

    public VerifyCancelQuery setVerify_id(String verify_id) {
        this.verify_id = verify_id;
        return this;
    }

    public static VerifyCancelQueryBuilder builder() {
        return new VerifyCancelQueryBuilder();
    }

    public static final class VerifyCancelQueryBuilder {
        private String certificate_id;
        private String order_id;
        private String verify_id;
        private Integer tenantId;
        private String clientKey;

        private VerifyCancelQueryBuilder() {
        }

        public VerifyCancelQueryBuilder certificateId(String certificateId) {
            this.certificate_id = certificateId;
            return this;
        }

        public VerifyCancelQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public VerifyCancelQueryBuilder verifyId(String verifyId) {
            this.verify_id = verifyId;
            return this;
        }

        public VerifyCancelQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public VerifyCancelQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public VerifyCancelQuery build() {
            VerifyCancelQuery verifyCancelQuery = new VerifyCancelQuery();
            verifyCancelQuery.setCertificate_id(certificate_id);
            verifyCancelQuery.setOrder_id(order_id);
            verifyCancelQuery.setVerify_id(verify_id);
            verifyCancelQuery.setTenantId(tenantId);
            verifyCancelQuery.setClientKey(clientKey);
            return verifyCancelQuery;
        }
    }
}
