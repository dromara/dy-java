package com.dyj.applet.domain;

import java.util.List;

public class ShortLiveDataWithIdData {

    /**
     * /短视频ID
     */
    private String item_id;
    /**
     * // 加密视频ID
     */
    private List<String> open_item_id_list;
    /**
     * 短视频标题
     */
    private String item_title;
    /**
     * 短视频封面（目前只有抖音端发布的短视频才有封面地址）
     */
    private String item_cover;
    /**
     * 短视频播放地址（目前只有抖音端发布的短视频才有播放地址）
     */
    private String item_addr;
    /**
     * 短视频作者对应的抖音号
     */
    private String item_aweme_shortid;
    /**
     * 短视频作者对应的类型，1:企业号 0:个人号
     */
    private Integer item_aweme_type;
    /**
     * 短视频作者的昵称
     */
    private String item_aweme_name;
    /**
     * 短视频作者头像地址
     */
    private String item_aweme_avatar;
    /**
     * 短视频时长，单位秒
     */
    private Long item_duration;
    /**
     * 短视频创建时间，时间戳形式
     */
    private Long item_create_time;
    /**
     * 短视频最后一帧观看完成次数
     */
    private Integer item_last_frame_watch_cnt;
    /**
     * 短视频完播率(短视频最后一帧观看完成次数/短视频播放次数)
     */
    private Double item_completion_rate;
    /**
     * 短视频评论次数
     */
    private Integer item_comment_count;
    /**
     * 短视频点赞次数
     */
    private Integer item_like_count;
    /**
     * 短视频分享次数
     */
    private Integer item_share_count;
    /**
     * 短视频播放次数
     */
    private Integer item_vv;
    /**
     * 小程序曝光次数
     */
    private Integer mp_show_pv;
    /**
     * 小程序点击次数（该指标后续不再提供，可以参考 mp_drainage_pv 指标）
     */
    private Integer mp_click_pv;
    /**
     * 进入小程序次数
     */
    private Integer mp_drainage_pv;
    /**
     * 短视频播放人数
     */
    private Integer item_uv;
    /**
     * 小程序曝光人数
     */
    private Integer mp_show_uv;
    /**
     * 小程序点击人数（该指标后续不再提供，可以参考 mp_drainage_uv 指标）
     */
    private Integer mp_click_uv;
    /**
     * 进入小程序人数
     */
    private Integer mp_drainage_uv;
    /**
     * 支付订单数
     */
    private Integer pay_order_cnt;
    /**
     * 支付订单人数
     */
    private Integer pay_customer_cnt;
    /**
     * 支付订单金额(单位：分)
     */
    private Long pay_order_amount;
    /**
     * 客单价(支付订单金额/支付订单人数 单位：分)
     */
    private Long customer_once_price;
    /**
     * 单均价(支付订单金额/支付订单数 单位：分)
     */
    private Long order_once_price;
    /**
     * 发起退款用户数
     */
    private Integer refund_customer_cnt;
    /**
     * 发起退款订单数
     */
    private Integer refund_order_cnt;
    /**
     * 发起退款金额(单位：分)
     */
    private Long refund_amount;
    /**
     * BC账号绑定关系，0-普通号，100-品牌号，200-员工号，300-合作号
     */
    private Integer bc_relation_bind_type;

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public List<String> getOpen_item_id_list() {
        return open_item_id_list;
    }

    public void setOpen_item_id_list(List<String> open_item_id_list) {
        this.open_item_id_list = open_item_id_list;
    }

    public String getItem_title() {
        return item_title;
    }

    public void setItem_title(String item_title) {
        this.item_title = item_title;
    }

    public String getItem_cover() {
        return item_cover;
    }

    public void setItem_cover(String item_cover) {
        this.item_cover = item_cover;
    }

    public String getItem_addr() {
        return item_addr;
    }

    public void setItem_addr(String item_addr) {
        this.item_addr = item_addr;
    }

    public String getItem_aweme_shortid() {
        return item_aweme_shortid;
    }

    public void setItem_aweme_shortid(String item_aweme_shortid) {
        this.item_aweme_shortid = item_aweme_shortid;
    }

    public Integer getItem_aweme_type() {
        return item_aweme_type;
    }

    public void setItem_aweme_type(Integer item_aweme_type) {
        this.item_aweme_type = item_aweme_type;
    }

    public String getItem_aweme_name() {
        return item_aweme_name;
    }

    public void setItem_aweme_name(String item_aweme_name) {
        this.item_aweme_name = item_aweme_name;
    }

    public String getItem_aweme_avatar() {
        return item_aweme_avatar;
    }

    public void setItem_aweme_avatar(String item_aweme_avatar) {
        this.item_aweme_avatar = item_aweme_avatar;
    }

    public Long getItem_duration() {
        return item_duration;
    }

    public void setItem_duration(Long item_duration) {
        this.item_duration = item_duration;
    }

    public Long getItem_create_time() {
        return item_create_time;
    }

    public void setItem_create_time(Long item_create_time) {
        this.item_create_time = item_create_time;
    }

    public Integer getItem_last_frame_watch_cnt() {
        return item_last_frame_watch_cnt;
    }

    public void setItem_last_frame_watch_cnt(Integer item_last_frame_watch_cnt) {
        this.item_last_frame_watch_cnt = item_last_frame_watch_cnt;
    }

    public Double getItem_completion_rate() {
        return item_completion_rate;
    }

    public void setItem_completion_rate(Double item_completion_rate) {
        this.item_completion_rate = item_completion_rate;
    }

    public Integer getItem_comment_count() {
        return item_comment_count;
    }

    public void setItem_comment_count(Integer item_comment_count) {
        this.item_comment_count = item_comment_count;
    }

    public Integer getItem_like_count() {
        return item_like_count;
    }

    public void setItem_like_count(Integer item_like_count) {
        this.item_like_count = item_like_count;
    }

    public Integer getItem_share_count() {
        return item_share_count;
    }

    public void setItem_share_count(Integer item_share_count) {
        this.item_share_count = item_share_count;
    }

    public Integer getItem_vv() {
        return item_vv;
    }

    public void setItem_vv(Integer item_vv) {
        this.item_vv = item_vv;
    }

    public Integer getMp_show_pv() {
        return mp_show_pv;
    }

    public void setMp_show_pv(Integer mp_show_pv) {
        this.mp_show_pv = mp_show_pv;
    }

    public Integer getMp_click_pv() {
        return mp_click_pv;
    }

    public void setMp_click_pv(Integer mp_click_pv) {
        this.mp_click_pv = mp_click_pv;
    }

    public Integer getMp_drainage_pv() {
        return mp_drainage_pv;
    }

    public void setMp_drainage_pv(Integer mp_drainage_pv) {
        this.mp_drainage_pv = mp_drainage_pv;
    }

    public Integer getItem_uv() {
        return item_uv;
    }

    public void setItem_uv(Integer item_uv) {
        this.item_uv = item_uv;
    }

    public Integer getMp_show_uv() {
        return mp_show_uv;
    }

    public void setMp_show_uv(Integer mp_show_uv) {
        this.mp_show_uv = mp_show_uv;
    }

    public Integer getMp_click_uv() {
        return mp_click_uv;
    }

    public void setMp_click_uv(Integer mp_click_uv) {
        this.mp_click_uv = mp_click_uv;
    }

    public Integer getMp_drainage_uv() {
        return mp_drainage_uv;
    }

    public void setMp_drainage_uv(Integer mp_drainage_uv) {
        this.mp_drainage_uv = mp_drainage_uv;
    }

    public Integer getPay_order_cnt() {
        return pay_order_cnt;
    }

    public void setPay_order_cnt(Integer pay_order_cnt) {
        this.pay_order_cnt = pay_order_cnt;
    }

    public Integer getPay_customer_cnt() {
        return pay_customer_cnt;
    }

    public void setPay_customer_cnt(Integer pay_customer_cnt) {
        this.pay_customer_cnt = pay_customer_cnt;
    }

    public Long getPay_order_amount() {
        return pay_order_amount;
    }

    public void setPay_order_amount(Long pay_order_amount) {
        this.pay_order_amount = pay_order_amount;
    }

    public Long getCustomer_once_price() {
        return customer_once_price;
    }

    public void setCustomer_once_price(Long customer_once_price) {
        this.customer_once_price = customer_once_price;
    }

    public Long getOrder_once_price() {
        return order_once_price;
    }

    public void setOrder_once_price(Long order_once_price) {
        this.order_once_price = order_once_price;
    }

    public Integer getRefund_customer_cnt() {
        return refund_customer_cnt;
    }

    public void setRefund_customer_cnt(Integer refund_customer_cnt) {
        this.refund_customer_cnt = refund_customer_cnt;
    }

    public Integer getRefund_order_cnt() {
        return refund_order_cnt;
    }

    public void setRefund_order_cnt(Integer refund_order_cnt) {
        this.refund_order_cnt = refund_order_cnt;
    }

    public Long getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(Long refund_amount) {
        this.refund_amount = refund_amount;
    }

    public Integer getBc_relation_bind_type() {
        return bc_relation_bind_type;
    }

    public void setBc_relation_bind_type(Integer bc_relation_bind_type) {
        this.bc_relation_bind_type = bc_relation_bind_type;
    }
}
