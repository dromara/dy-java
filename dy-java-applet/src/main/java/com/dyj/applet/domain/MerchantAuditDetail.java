package com.dyj.applet.domain;

public class MerchantAuditDetail {

    /**
     * 不同意退款信息(不同意退款时必填)，长度 <= 512 byte 选填
     */
    private String deny_message;
    /**
     * <p>审核状态，</p><ul><li>1-同意退款 </li><li>2-不同意退款</li></ul>
     */
    private Integer refund_audit_status;
    /**
     * <p>交易系统侧退款单号，长度 &lt;= 64 byte</p>
     */
    private String refund_id;

    public String getDeny_message() {
        return deny_message;
    }

    public MerchantAuditDetail setDeny_message(String deny_message) {
        this.deny_message = deny_message;
        return this;
    }

    public Integer getRefund_audit_status() {
        return refund_audit_status;
    }

    public MerchantAuditDetail setRefund_audit_status(Integer refund_audit_status) {
        this.refund_audit_status = refund_audit_status;
        return this;
    }

    public String getRefund_id() {
        return refund_id;
    }

    public MerchantAuditDetail setRefund_id(String refund_id) {
        this.refund_id = refund_id;
        return this;
    }
}
