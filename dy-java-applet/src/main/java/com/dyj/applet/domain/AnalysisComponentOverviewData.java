package com.dyj.applet.domain;

public class AnalysisComponentOverviewData {
    /**
     * 组件点击次数
     */
    private Long ComponentClickPv;
    /**
     * 组件点击用户数
     */
    private Long ComponentClickUv;
    /**
     * 组件留资次数
     */
    private Long ComponentReportPv;
    /**
     * 组件留资用户数
     */
    private Long ComponentReportUv;
    /**
     * 组件曝光次数
     */
    private Long ComponentShowPv;
    /**
     * 组件访问用户数
     */
    private Long ComponentShowUv;
    /**
     * 当日线索用户数量
     */
    private Long ClueCnt;
    /**
     * 当日线索用户数量
     */
    private Long ClueUcnt;
    /**
     * 当日有效线索数量
     */
    private Long ValidClueCnt;
    /**
     * 当日有效线索用户数量
     */
    private Long ValidClueUcnt;
    /**
     * 日期，跨天的时间筛选返回
     */
    private String Date;
    /**
     * 小时，同一天的时间筛选返回
     */
    private String Hour;

    public Long getComponentClickPv() {
        return ComponentClickPv;
    }

    public void setComponentClickPv(Long componentClickPv) {
        ComponentClickPv = componentClickPv;
    }

    public Long getComponentClickUv() {
        return ComponentClickUv;
    }

    public void setComponentClickUv(Long componentClickUv) {
        ComponentClickUv = componentClickUv;
    }

    public Long getComponentReportPv() {
        return ComponentReportPv;
    }

    public void setComponentReportPv(Long componentReportPv) {
        ComponentReportPv = componentReportPv;
    }

    public Long getComponentReportUv() {
        return ComponentReportUv;
    }

    public void setComponentReportUv(Long componentReportUv) {
        ComponentReportUv = componentReportUv;
    }

    public Long getComponentShowPv() {
        return ComponentShowPv;
    }

    public void setComponentShowPv(Long componentShowPv) {
        ComponentShowPv = componentShowPv;
    }

    public Long getComponentShowUv() {
        return ComponentShowUv;
    }

    public void setComponentShowUv(Long componentShowUv) {
        ComponentShowUv = componentShowUv;
    }

    public Long getClueCnt() {
        return ClueCnt;
    }

    public void setClueCnt(Long clueCnt) {
        ClueCnt = clueCnt;
    }

    public Long getClueUcnt() {
        return ClueUcnt;
    }

    public void setClueUcnt(Long clueUcnt) {
        ClueUcnt = clueUcnt;
    }

    public Long getValidClueCnt() {
        return ValidClueCnt;
    }

    public void setValidClueCnt(Long validClueCnt) {
        ValidClueCnt = validClueCnt;
    }

    public Long getValidClueUcnt() {
        return ValidClueUcnt;
    }

    public void setValidClueUcnt(Long validClueUcnt) {
        ValidClueUcnt = validClueUcnt;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getHour() {
        return Hour;
    }

    public void setHour(String hour) {
        Hour = hour;
    }
}
