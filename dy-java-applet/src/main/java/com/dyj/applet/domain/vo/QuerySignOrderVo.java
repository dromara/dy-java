package com.dyj.applet.domain.vo;

public class QuerySignOrderVo {

    /**
     * <p>小程序 app_id</p>
     */
    private String app_id;
    /**
     * <p>平台侧签约单的单号，长度<=64byte</p>
     */
    private String auth_order_id;
    /**
     * <p>解约来源</p><p>1-用户解约</p><p>2-商户解约</p> 选填
     */
    private Long cancel_source;
    /**
     * <p>下单回调地址，https开头，长度<=512byte</p> 选填
     */
    private String notify_url;
    /**
     * <p>用户open id</p>
     */
    private String open_id;
    /**
     * <p>开发者侧签约单的单号，长度<=64byte</p>
     */
    private String out_auth_order_id;
    /**
     * <p>签约模板ID</p>
     */
    private String service_id;
    /**
     * <p>用户签约完成时间，时间毫秒</p> 选填
     */
    private Long sign_time;
    /**
     * <p>签约单状态</p><p>TOBESERVED: 待服务</p><p>SERVING：服务中</p><p>CANCEL: 已解约</p><p>TIMEOUT: 用户未签约</p><p>DONE: 服务完成，签约已到期</p>
     */
    private String status;

    public String getApp_id() {
        return app_id;
    }

    public QuerySignOrderVo setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public QuerySignOrderVo setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public Long getCancel_source() {
        return cancel_source;
    }

    public QuerySignOrderVo setCancel_source(Long cancel_source) {
        this.cancel_source = cancel_source;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public QuerySignOrderVo setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOpen_id() {
        return open_id;
    }

    public QuerySignOrderVo setOpen_id(String open_id) {
        this.open_id = open_id;
        return this;
    }

    public String getOut_auth_order_id() {
        return out_auth_order_id;
    }

    public QuerySignOrderVo setOut_auth_order_id(String out_auth_order_id) {
        this.out_auth_order_id = out_auth_order_id;
        return this;
    }

    public String getService_id() {
        return service_id;
    }

    public QuerySignOrderVo setService_id(String service_id) {
        this.service_id = service_id;
        return this;
    }

    public Long getSign_time() {
        return sign_time;
    }

    public QuerySignOrderVo setSign_time(Long sign_time) {
        this.sign_time = sign_time;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public QuerySignOrderVo setStatus(String status) {
        this.status = status;
        return this;
    }
}
