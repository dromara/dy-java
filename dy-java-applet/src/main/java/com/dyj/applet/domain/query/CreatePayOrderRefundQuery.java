package com.dyj.applet.domain.query;

import com.dyj.applet.domain.PayOrderFeeDetail;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class CreatePayOrderRefundQuery extends BaseQuery {

    /**
     * <p>退款结果回调地址，https开头</p> 选填
     */
    private String notify_url;
    /**
     * <p>扣款退款单开发者侧退款单号，长度<=64byte</p>
     */
    private String out_pay_refund_no;
    /**
     * <p>扣款单平台订单号，长度<=64byte</p>
     */
    private String pay_order_id;
    /**
     * <p>退款原因，长度<=256byte</p> 选填
     */
    private String refund_reason;
    /**
     * <p>退款总金额</p>
     */
    private Long refund_total_amount;
    /**
     * <p>退款付费项目详情</p>
     */
    private List<PayOrderFeeDetail> fee_detail_list;


    public String getNotify_url() {
        return notify_url;
    }

    public CreatePayOrderRefundQuery setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOut_pay_refund_no() {
        return out_pay_refund_no;
    }

    public CreatePayOrderRefundQuery setOut_pay_refund_no(String out_pay_refund_no) {
        this.out_pay_refund_no = out_pay_refund_no;
        return this;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public CreatePayOrderRefundQuery setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public String getRefund_reason() {
        return refund_reason;
    }

    public CreatePayOrderRefundQuery setRefund_reason(String refund_reason) {
        this.refund_reason = refund_reason;
        return this;
    }

    public Long getRefund_total_amount() {
        return refund_total_amount;
    }

    public CreatePayOrderRefundQuery setRefund_total_amount(Long refund_total_amount) {
        this.refund_total_amount = refund_total_amount;
        return this;
    }

    public List<PayOrderFeeDetail> getFee_detail_list() {
        return fee_detail_list;
    }

    public CreatePayOrderRefundQuery setFee_detail_list(List<PayOrderFeeDetail> fee_detail_list) {
        this.fee_detail_list = fee_detail_list;
        return this;
    }

    public static CreatePayOrderRefundQueryBuilder builder() {
        return new CreatePayOrderRefundQueryBuilder();
    }

    public static final class CreatePayOrderRefundQueryBuilder {
        private String notify_url;
        private String out_pay_refund_no;
        private String pay_order_id;
        private String refund_reason;
        private Long refund_total_amount;
        private List<PayOrderFeeDetail> fee_detail_list;
        private Integer tenantId;
        private String clientKey;

        private CreatePayOrderRefundQueryBuilder() {
        }

        public CreatePayOrderRefundQueryBuilder notifyUrl(String notifyUrl) {
            this.notify_url = notifyUrl;
            return this;
        }

        public CreatePayOrderRefundQueryBuilder outPayRefundNo(String outPayRefundNo) {
            this.out_pay_refund_no = outPayRefundNo;
            return this;
        }

        public CreatePayOrderRefundQueryBuilder payOrderId(String payOrderId) {
            this.pay_order_id = payOrderId;
            return this;
        }

        public CreatePayOrderRefundQueryBuilder refundReason(String refundReason) {
            this.refund_reason = refundReason;
            return this;
        }

        public CreatePayOrderRefundQueryBuilder refundTotalAmount(Long refundTotalAmount) {
            this.refund_total_amount = refundTotalAmount;
            return this;
        }

        public CreatePayOrderRefundQueryBuilder feeDetailList(List<PayOrderFeeDetail> feeDetailList) {
            this.fee_detail_list = feeDetailList;
            return this;
        }

        public CreatePayOrderRefundQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreatePayOrderRefundQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreatePayOrderRefundQuery build() {
            CreatePayOrderRefundQuery createPayOrderRefundQuery = new CreatePayOrderRefundQuery();
            createPayOrderRefundQuery.setNotify_url(notify_url);
            createPayOrderRefundQuery.setOut_pay_refund_no(out_pay_refund_no);
            createPayOrderRefundQuery.setPay_order_id(pay_order_id);
            createPayOrderRefundQuery.setRefund_reason(refund_reason);
            createPayOrderRefundQuery.setRefund_total_amount(refund_total_amount);
            createPayOrderRefundQuery.setFee_detail_list(fee_detail_list);
            createPayOrderRefundQuery.setTenantId(tenantId);
            createPayOrderRefundQuery.setClientKey(clientKey);
            return createPayOrderRefundQuery;
        }
    }
}
