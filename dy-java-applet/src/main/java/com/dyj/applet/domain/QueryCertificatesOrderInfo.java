package com.dyj.applet.domain;

import java.util.List;

public class QueryCertificatesOrderInfo {

    /**
     * 抖音内部交易订单号，长度<64byte。
     */
    private String order_id;

    /**
     * 此订单的券信息
     */
    private List<CertificatesInfo> certificates;

    /**
     * 是否可用
     */
    private Boolean can_use;

    public String getOrder_id() {
        return order_id;
    }

    public QueryCertificatesOrderInfo setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public List<CertificatesInfo> getCertificates() {
        return certificates;
    }

    public QueryCertificatesOrderInfo setCertificates(List<CertificatesInfo> certificates) {
        this.certificates = certificates;
        return this;
    }

    public Boolean getCan_use() {
        return can_use;
    }

    public QueryCertificatesOrderInfo setCan_use(Boolean can_use) {
        this.can_use = can_use;
        return this;
    }
}
