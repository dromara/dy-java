package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AwemeVideoKeyword;

import java.util.List;

/**
 * @author danmo
 * @date 2024-07-17 10:17
 **/
public class AwemeVideoKeywordListVo {

    /**
     * 记录总数
     */
    private Long total_count;
    /**
     * 关键词列表
     */
    private List<AwemeVideoKeyword> keyword_list;

    public Long getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Long total_count) {
        this.total_count = total_count;
    }

    public List<AwemeVideoKeyword> getKeyword_list() {
        return keyword_list;
    }

    public void setKeyword_list(List<AwemeVideoKeyword> keyword_list) {
        this.keyword_list = keyword_list;
    }
}
