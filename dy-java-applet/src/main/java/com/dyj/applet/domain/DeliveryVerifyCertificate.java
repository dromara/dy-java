package com.dyj.applet.domain;

/**
 * 验券准备接口返回的加密券码，传入几份表示核销几份。
 */
public class DeliveryVerifyCertificate {

    /**
     * 券 id
     */
    private String certificate_id;
    /**
     * 加密券码
     */
    private String encrypted_code;

    public String getCertificate_id() {
        return certificate_id;
    }

    public DeliveryVerifyCertificate setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
        return this;
    }

    public String getEncrypted_code() {
        return encrypted_code;
    }

    public DeliveryVerifyCertificate setEncrypted_code(String encrypted_code) {
        this.encrypted_code = encrypted_code;
        return this;
    }
}
