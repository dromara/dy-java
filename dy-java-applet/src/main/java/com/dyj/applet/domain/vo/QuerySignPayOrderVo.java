package com.dyj.applet.domain.vo;

public class QuerySignPayOrderVo {

    /**
     * <p>小程序 app_id</p>
     */
    private String app_id;
    /**
     * <p>平台侧签约单的单号，长度<=64byte</p>
     */
    private String auth_order_id;
    /**
     * <p>渠道支付单</p> 选填
     */
    private String channel_pay_id;
    /**
     * <p>开发者自定义收款商户号</p> 选填
     */
    private String merchant_uid;
    /**
     * <p>下单结果回调地址，https开头</p> 选填
     */
    private String notify_url;
    /**
     * <p>开发者侧代扣单的单号，长度<=64byte</p>
     */
    private String out_pay_order_no;
    /**
     * <p>支付渠道枚举（<strong>扣款成功时才有</strong>）：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">10：抖音支付</li></ul> 选填
     */
    private Integer pay_channel;
    /**
     * <p>平台侧代扣单的单号，长度<=64byte</p>
     */
    private String pay_order_id;
    /**
     * <p>支付成功时间，时间单位为毫秒</p> 选填
     */
    private Long pay_time;
    /**
     * <p>代扣单状态</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">"SUCCESS" （扣款成功）</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">"PROCESSING" (订单处理中)</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">"TIME_OUT" （超时未支付 ｜超时未扣款成功）</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">"FAIL" （扣款失败，特殊情况下发起代扣直接失败才会有该状态，正常情况下用户账户余额不足，状态都是流转到TIME_OUT）</li></ul>
     */
    private String status;
    /**
     * <p>代扣金额，单位[分]，代扣金额是签约模板固定，无法更改</p>
     */
    private Long total_amount;
    /**
     * <p>用户抖音交易单号（账单号），和用户抖音钱包-账单中所展示的交易单号相同</p> 选填
     */
    private String user_bill_pay_id;

    public String getApp_id() {
        return app_id;
    }

    public QuerySignPayOrderVo setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public QuerySignPayOrderVo setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public String getChannel_pay_id() {
        return channel_pay_id;
    }

    public QuerySignPayOrderVo setChannel_pay_id(String channel_pay_id) {
        this.channel_pay_id = channel_pay_id;
        return this;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public QuerySignPayOrderVo setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public QuerySignPayOrderVo setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOut_pay_order_no() {
        return out_pay_order_no;
    }

    public QuerySignPayOrderVo setOut_pay_order_no(String out_pay_order_no) {
        this.out_pay_order_no = out_pay_order_no;
        return this;
    }

    public Integer getPay_channel() {
        return pay_channel;
    }

    public QuerySignPayOrderVo setPay_channel(Integer pay_channel) {
        this.pay_channel = pay_channel;
        return this;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public QuerySignPayOrderVo setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public Long getPay_time() {
        return pay_time;
    }

    public QuerySignPayOrderVo setPay_time(Long pay_time) {
        this.pay_time = pay_time;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public QuerySignPayOrderVo setStatus(String status) {
        this.status = status;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public QuerySignPayOrderVo setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public String getUser_bill_pay_id() {
        return user_bill_pay_id;
    }

    public QuerySignPayOrderVo setUser_bill_pay_id(String user_bill_pay_id) {
        this.user_bill_pay_id = user_bill_pay_id;
        return this;
    }
}
