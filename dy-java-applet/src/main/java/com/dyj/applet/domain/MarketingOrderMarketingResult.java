package com.dyj.applet.domain;

import java.util.List;

/**
 * 订单维度营销查询结果，包含订单总价以及订单维度可用/不可用营销
 */
public class MarketingOrderMarketingResult {

    /**
     * 商品总价，单位分
     */
    private Long total_amount;

    /**
     * 订单维度可用的优惠信息
     */
    private MarketingBundle available_marketing;

    /**
     * 订单维度不可用的优惠信息
     */
    private MarketingBundle unavailable_marketing;

    public Long getTotal_amount() {
        return total_amount;
    }

    public MarketingOrderMarketingResult setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public MarketingBundle getAvailable_marketing() {
        return available_marketing;
    }

    public MarketingOrderMarketingResult setAvailable_marketing(MarketingBundle available_marketing) {
        this.available_marketing = available_marketing;
        return this;
    }

    public MarketingBundle getUnavailable_marketing() {
        return unavailable_marketing;
    }

    public MarketingOrderMarketingResult setUnavailable_marketing(MarketingBundle unavailable_marketing) {
        this.unavailable_marketing = unavailable_marketing;
        return this;
    }
}
