package com.dyj.applet.domain;

public class ApplyAliasModifyTag {

    /**
     * 修改前的搜索标签
     */
    private String before_tag;

    /**
     * 删除的搜索标签
     */
    private String search_tag;

    public static ApplyAliasModifyTagBuilder builder() {
        return new ApplyAliasModifyTagBuilder();
    }

    public static class ApplyAliasModifyTagBuilder {
        /**
         * 修改前的搜索标签
         */
        private String beforeTag;

        /**
         * 删除的搜索标签
         */
        private String searchTag;

        public ApplyAliasModifyTagBuilder beforeTag(String beforeTag) {
            this.beforeTag = beforeTag;
            return this;
        }

        public ApplyAliasModifyTagBuilder searchTag(String searchTag) {
            this.searchTag = searchTag;
            return this;
        }

        public ApplyAliasModifyTag build() {
            ApplyAliasModifyTag applyAliasModifyTag = new ApplyAliasModifyTag();
            applyAliasModifyTag.setBefore_tag(beforeTag);
            applyAliasModifyTag.setSearch_tag(searchTag);
            return applyAliasModifyTag;
        }
    }

    public String getBefore_tag() {
        return before_tag;
    }

    public void setBefore_tag(String before_tag) {
        this.before_tag = before_tag;
    }

    public String getSearch_tag() {
        return search_tag;
    }

    public void setSearch_tag(String search_tag) {
        this.search_tag = search_tag;
    }
}
