package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.ApplyAliasInfo;

import java.util.List;

public class ApplyAliasListVo {

    /**
     * 小程序别名列表
     */
    private List<ApplyAliasInfo> alias_list;

    /**
     * 本周可操作次数
     */
    private Integer week_available_times;

    /**
     * 本周总次数
     */
    private Integer week_total_times;

    public List<ApplyAliasInfo> getAlias_list() {
        return alias_list;
    }

    public void setAlias_list(List<ApplyAliasInfo> alias_list) {
        this.alias_list = alias_list;
    }

    public Integer getWeek_available_times() {
        return week_available_times;
    }

    public void setWeek_available_times(Integer week_available_times) {
        this.week_available_times = week_available_times;
    }

    public Integer getWeek_total_times() {
        return week_total_times;
    }

    public void setWeek_total_times(Integer week_total_times) {
        this.week_total_times = week_total_times;
    }
}
