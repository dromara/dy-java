package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisDealDataWithConversionData;

public class AnalysisDealDataWithConversionVo {

    private AnalysisDealDataWithConversionData Data;

    public AnalysisDealDataWithConversionData getData() {
        return Data;
    }

    public void setData(AnalysisDealDataWithConversionData data) {
        Data = data;
    }
}
