package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QueryCreatedSubscriptionTemplate;

import java.util.List;

public class QueryCreatedSubscriptionTplListVo {

    /**
     * 记录总数
     */
    private Long total_count;
    /**
     * 订阅消息模板列表
     */
    private List<QueryCreatedSubscriptionTemplate> template_list;

    public Long getTotal_count() {
        return total_count;
    }

    public QueryCreatedSubscriptionTplListVo setTotal_count(Long total_count) {
        this.total_count = total_count;
        return this;
    }

    public List<QueryCreatedSubscriptionTemplate> getTemplate_list() {
        return template_list;
    }

    public QueryCreatedSubscriptionTplListVo setTemplate_list(List<QueryCreatedSubscriptionTemplate> template_list) {
        this.template_list = template_list;
        return this;
    }
}
