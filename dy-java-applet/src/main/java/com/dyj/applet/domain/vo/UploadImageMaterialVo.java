package com.dyj.applet.domain.vo;

public class UploadImageMaterialVo {

    /**
     * <p>图片素材id</p> 选填
     */
    private String image_material_id;

    public String getImage_material_id() {
        return image_material_id;
    }

    public UploadImageMaterialVo setImage_material_id(String image_material_id) {
        this.image_material_id = image_material_id;
        return this;
    }
}
