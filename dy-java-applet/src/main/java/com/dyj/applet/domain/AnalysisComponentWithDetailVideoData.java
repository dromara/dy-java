package com.dyj.applet.domain;

import java.util.List;

public class AnalysisComponentWithDetailVideoData {

    private Long Total;

    private List<ComponentWithDetailVideoData> DataList;

    public Long getTotal() {
        return Total;
    }

    public void setTotal(Long total) {
        Total = total;
    }

    public List<ComponentWithDetailVideoData> getDataList() {
        return DataList;
    }

    public void setDataList(List<ComponentWithDetailVideoData> dataList) {
        DataList = dataList;
    }
}
