package com.dyj.applet.domain;

import java.util.Map;

/**
 * 营销算价->积分信息
 */
public class MarketingScoreInfo {

    /**
     * 可用积分值
     */
    private Integer value;

    /**
     * 积分 id，如“JingDong-vip”表示京东 VIP 会员积分，再比如可能会有具体某个店铺的积分，很多大店铺是有自己的积分系统的
     */
    private String id;

    /**
     * 积分名字：如：京东 VIP 会员积分
     */
    private String name;

    /**
     * 营销类别
     * 1：商家
     * 2：平台
     */
    private Integer kind;

    /**
     * 创建人类型
     * 1：抖音平台
     * 2：抖音来客—商家
     * 3：小程序商家
     */
    private Integer creator_type;

    /**
     * 扩展字段
     */
    private Map<Object,Object> marketing_extend;

    public Integer getValue() {
        return value;
    }

    public MarketingScoreInfo setValue(Integer value) {
        this.value = value;
        return this;
    }

    public String getId() {
        return id;
    }

    public MarketingScoreInfo setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public MarketingScoreInfo setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getKind() {
        return kind;
    }

    public MarketingScoreInfo setKind(Integer kind) {
        this.kind = kind;
        return this;
    }

    public Integer getCreator_type() {
        return creator_type;
    }

    public MarketingScoreInfo setCreator_type(Integer creator_type) {
        this.creator_type = creator_type;
        return this;
    }

    public Map<Object, Object> getMarketing_extend() {
        return marketing_extend;
    }

    public MarketingScoreInfo setMarketing_extend(Map<Object, Object> marketing_extend) {
        this.marketing_extend = marketing_extend;
        return this;
    }
}
