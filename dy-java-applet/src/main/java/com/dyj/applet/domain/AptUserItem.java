package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-24 14:00
 **/
public class AptUserItem {

    /**
     * 每日发布内容数
     */
    private Long new_issue;
    /**
     * 每天新增视频播放
     */
    private Long new_play;
    /**
     * 每天新增视频播放
     */
    private Long total_issue;
    /**
     * 日期
     */
    private String date;

    public Long getNew_issue() {
        return new_issue;
    }

    public void setNew_issue(Long new_issue) {
        this.new_issue = new_issue;
    }

    public Long getNew_play() {
        return new_play;
    }

    public void setNew_play(Long new_play) {
        this.new_play = new_play;
    }

    public Long getTotal_issue() {
        return total_issue;
    }

    public void setTotal_issue(Long total_issue) {
        this.total_issue = total_issue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
