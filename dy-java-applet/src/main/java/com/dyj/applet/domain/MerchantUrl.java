package com.dyj.applet.domain;

/**
 * 进件页面链接
 */
public class MerchantUrl {
    /**
     * <p>小程序平台分配商户号，用于支付、分账等环节标识商户</p>
     */
    private String merchant_id;
    /**
     * <p>请求页面链接，24小时过期，注意定时更新，请勿长期保存。</p>
     */
    private String url;

    public String getMerchant_id() {
        return merchant_id;
    }

    public MerchantUrl setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public MerchantUrl setUrl(String url) {
        this.url = url;
        return this;
    }
}
