package com.dyj.applet.domain;

public class SmallHomeRoomData {
    /**
     * 小程序app_id
     */
    private String app_id;
    /**
     * 小程序名称
     */
    private String app_name;
    /**
     * 核销订单金额(单位：分)
     */
    private Long delivery_success_amount;
    /**
     * 核销订单数
     */
    private Long delivery_success_order_cnt;
    /**
     * 直播开播时间，秒级时间戳
     */
    private Long start_time;
    /**
     * 直播结束时间，秒级时间戳
     */
    private Long end_time;
    /**
     * 累计直播时长(单位：秒)
     */
    private Long live_duration;
    /**
     * 最高在线人数
     */
    private Long max_watch_user_cnt;
    /**
     * 主播昵称
     */
    private String nick_name;
    /**
     * 支付订单金额(单位：分)
     */
    private Long pay_amount;
    /**
     * 支付订单数
     */
    private Integer pay_order_cnt;
    /**
     * 人均观看时长(累计观看时长/累计看播人数 (单位：秒))
     */
    private Long per_watch_duration;
    /**
     * 主播uid
     */
    private String anchor_id;
    /**
     * 已退款订单金额(单位：分)
     */
    private Long refund_success_amount;
    /**
     *已退款订单数
     */
    private Long refund_success_order_cnt;
    /**
     * 累计看播人数
     */
    private Integer watch_user_cnt;


    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public Long getDelivery_success_amount() {
        return delivery_success_amount;
    }

    public void setDelivery_success_amount(Long delivery_success_amount) {
        this.delivery_success_amount = delivery_success_amount;
    }

    public Long getDelivery_success_order_cnt() {
        return delivery_success_order_cnt;
    }

    public void setDelivery_success_order_cnt(Long delivery_success_order_cnt) {
        this.delivery_success_order_cnt = delivery_success_order_cnt;
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public Long getLive_duration() {
        return live_duration;
    }

    public void setLive_duration(Long live_duration) {
        this.live_duration = live_duration;
    }

    public Long getMax_watch_user_cnt() {
        return max_watch_user_cnt;
    }

    public void setMax_watch_user_cnt(Long max_watch_user_cnt) {
        this.max_watch_user_cnt = max_watch_user_cnt;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public Long getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(Long pay_amount) {
        this.pay_amount = pay_amount;
    }

    public Integer getPay_order_cnt() {
        return pay_order_cnt;
    }

    public void setPay_order_cnt(Integer pay_order_cnt) {
        this.pay_order_cnt = pay_order_cnt;
    }

    public Long getPer_watch_duration() {
        return per_watch_duration;
    }

    public void setPer_watch_duration(Long per_watch_duration) {
        this.per_watch_duration = per_watch_duration;
    }

    public String getAnchor_id() {
        return anchor_id;
    }

    public void setAnchor_id(String anchor_id) {
        this.anchor_id = anchor_id;
    }

    public Long getRefund_success_amount() {
        return refund_success_amount;
    }

    public void setRefund_success_amount(Long refund_success_amount) {
        this.refund_success_amount = refund_success_amount;
    }

    public Long getRefund_success_order_cnt() {
        return refund_success_order_cnt;
    }

    public void setRefund_success_order_cnt(Long refund_success_order_cnt) {
        this.refund_success_order_cnt = refund_success_order_cnt;
    }

    public Integer getWatch_user_cnt() {
        return watch_user_cnt;
    }

    public void setWatch_user_cnt(Integer watch_user_cnt) {
        this.watch_user_cnt = watch_user_cnt;
    }
}
