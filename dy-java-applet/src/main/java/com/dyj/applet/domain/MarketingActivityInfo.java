package com.dyj.applet.domain;

import java.util.Map;

/**
 * 营销算价->活动信息
 */
public class MarketingActivityInfo {

    /**
     * 活动 id
     */
    private String id;

    /**
     * 活动名称
     */
    private String name;

    /**
     * 有效起始时间戳 选填
     */
    private Long start_time;

    /**
     * 有效结束时间戳 选填
     */
    private Long end_time;

    /**
     * 活动规则
     */
    private String rule;

    /**
     * 营销类别
     * 1：商家
     * 2：平台
     */
    private Integer kind;

    /**
     * 创建人类型
     * 1：抖音平台
     * 2：抖音来客—商家
     * 3：小程序商家
     */
    private Integer creator_type;

    /**
     * 扩展字段
     */
    private Map<Object,Object> marketing_extend;

    /**
     * 1：秒杀
     * 2：运费立减
     * 3：立减
     * 4：会员专享价
     * 5：渠道专享价
     */
    private Integer type;

    public String getId() {
        return id;
    }

    public MarketingActivityInfo setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public MarketingActivityInfo setName(String name) {
        this.name = name;
        return this;
    }

    public Long getStart_time() {
        return start_time;
    }

    public MarketingActivityInfo setStart_time(Long start_time) {
        this.start_time = start_time;
        return this;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public MarketingActivityInfo setEnd_time(Long end_time) {
        this.end_time = end_time;
        return this;
    }

    public String getRule() {
        return rule;
    }

    public MarketingActivityInfo setRule(String rule) {
        this.rule = rule;
        return this;
    }

    public Integer getKind() {
        return kind;
    }

    public MarketingActivityInfo setKind(Integer kind) {
        this.kind = kind;
        return this;
    }

    public Integer getCreator_type() {
        return creator_type;
    }

    public MarketingActivityInfo setCreator_type(Integer creator_type) {
        this.creator_type = creator_type;
        return this;
    }

    public Map<Object, Object> getMarketing_extend() {
        return marketing_extend;
    }

    public MarketingActivityInfo setMarketing_extend(Map<Object, Object> marketing_extend) {
        this.marketing_extend = marketing_extend;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public MarketingActivityInfo setType(Integer type) {
        this.type = type;
        return this;
    }
}
