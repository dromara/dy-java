package com.dyj.applet.domain;

public class ProductDoubleOpenInfo {

    /**
     * 引导文案: 通过query_text查询接口设置
     * 选填
     */
    private String guidance_text_id;

    /**
     * 到店按钮文案: 通过query_text查询接口设置
     * 选填
     */
    private String arrival_store_button_text_id;

    /**
     *
     * 到家按钮文案: 通过query_text查询接口设置
     * 选填
     */
    private String arrival_home_button_text_id;

    /**
     * 二维码展示方式，枚举值（1，2，3）
     */
    private Integer display_mode;

    public String getGuidance_text_id() {
        return guidance_text_id;
    }

    public ProductDoubleOpenInfo setGuidance_text_id(String guidance_text_id) {
        this.guidance_text_id = guidance_text_id;
        return this;
    }

    public String getArrival_store_button_text_id() {
        return arrival_store_button_text_id;
    }

    public ProductDoubleOpenInfo setArrival_store_button_text_id(String arrival_store_button_text_id) {
        this.arrival_store_button_text_id = arrival_store_button_text_id;
        return this;
    }

    public String getArrival_home_button_text_id() {
        return arrival_home_button_text_id;
    }

    public ProductDoubleOpenInfo setArrival_home_button_text_id(String arrival_home_button_text_id) {
        this.arrival_home_button_text_id = arrival_home_button_text_id;
        return this;
    }

    public Integer getDisplay_mode() {
        return display_mode;
    }

    public ProductDoubleOpenInfo setDisplay_mode(Integer display_mode) {
        this.display_mode = display_mode;
        return this;
    }
}
