package com.dyj.applet.domain;

public class ChannelAndUrl {

    /**
     * <p>图片对应的进件渠道枚举，详见请求参数channels</p> 选填
     */
    private String channel;
    /**
     * <p>图片id信息，通过图片上传接口获取</p> 选填
     */
    private String url;

    public String getChannel() {
        return channel;
    }

    public ChannelAndUrl setChannel(String channel) {
        this.channel = channel;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public ChannelAndUrl setUrl(String url) {
        this.url = url;
        return this;
    }

    public static ChannelAndUrlBuilder builder(){
        return new ChannelAndUrlBuilder();
    }

    public static final class ChannelAndUrlBuilder {
        private String channel;
        private String url;

        private ChannelAndUrlBuilder() {
        }


        public ChannelAndUrlBuilder channel(String channel) {
            this.channel = channel;
            return this;
        }

        public ChannelAndUrlBuilder url(String url) {
            this.url = url;
            return this;
        }

        public ChannelAndUrl build() {
            ChannelAndUrl channelAndUrl = new ChannelAndUrl();
            channelAndUrl.setChannel(channel);
            channelAndUrl.setUrl(url);
            return channelAndUrl;
        }
    }
}
