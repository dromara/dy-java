package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AppCategoryAuditInfo;

import java.util.List;

public class AuditCategoryListVo {

    /**
     * 本月总共可以修改次数
     */
    private Integer category_change_limit;

    /**
     * 最多可以设置的服务类目数量
     */
    private Integer category_limit;
    /**
     *本月剩余修改次数
     */
    private Integer remaining_times;

    private List<AppCategoryAuditInfo> app_category_audit_info;

    public Integer getCategory_change_limit() {
        return category_change_limit;
    }

    public void setCategory_change_limit(Integer category_change_limit) {
        this.category_change_limit = category_change_limit;
    }

    public Integer getCategory_limit() {
        return category_limit;
    }

    public void setCategory_limit(Integer category_limit) {
        this.category_limit = category_limit;
    }

    public Integer getRemaining_times() {
        return remaining_times;
    }

    public void setRemaining_times(Integer remaining_times) {
        this.remaining_times = remaining_times;
    }

    public List<AppCategoryAuditInfo> getApp_category_audit_info() {
        return app_category_audit_info;
    }

    public void setApp_category_audit_info(List<AppCategoryAuditInfo> app_category_audit_info) {
        this.app_category_audit_info = app_category_audit_info;
    }
}
