package com.dyj.applet.domain;


public class LiveWithShortData {

    private ShortData Data;

    private ShortInfo ShortInfo;

    public static class ShortData{
        /**
         * 客单价
         */
        private Long CustomerOncePrice;
        /**
         * 支付人数
         */
        private Long PayCustomerCnt;
        /**
         * 支付订单金额
         */
        private Long PayOrderAmount;
        /**
         * 直播总时长，单位：秒
         */
        private Long RoomDuration;
        /**
         * 最高在线人数
         */
        private Long RoomHighestOnlineCount;
        /**
         * 新增粉丝数
         */
        private Long RoomNewFans;
        /**
         * 直播场次
         */
        private Long RoomSessionCnt;
        /**
         *
         * 曝光-观看率
         */
        private Long RoomWatchRate;

        public Long getCustomerOncePrice() {
            return CustomerOncePrice;
        }

        public void setCustomerOncePrice(Long customerOncePrice) {
            CustomerOncePrice = customerOncePrice;
        }

        public Long getPayCustomerCnt() {
            return PayCustomerCnt;
        }

        public void setPayCustomerCnt(Long payCustomerCnt) {
            PayCustomerCnt = payCustomerCnt;
        }

        public Long getPayOrderAmount() {
            return PayOrderAmount;
        }

        public void setPayOrderAmount(Long payOrderAmount) {
            PayOrderAmount = payOrderAmount;
        }

        public Long getRoomDuration() {
            return RoomDuration;
        }

        public void setRoomDuration(Long roomDuration) {
            RoomDuration = roomDuration;
        }

        public Long getRoomHighestOnlineCount() {
            return RoomHighestOnlineCount;
        }

        public void setRoomHighestOnlineCount(Long roomHighestOnlineCount) {
            RoomHighestOnlineCount = roomHighestOnlineCount;
        }

        public Long getRoomNewFans() {
            return RoomNewFans;
        }

        public void setRoomNewFans(Long roomNewFans) {
            RoomNewFans = roomNewFans;
        }

        public Long getRoomSessionCnt() {
            return RoomSessionCnt;
        }

        public void setRoomSessionCnt(Long roomSessionCnt) {
            RoomSessionCnt = roomSessionCnt;
        }

        public Long getRoomWatchRate() {
            return RoomWatchRate;
        }

        public void setRoomWatchRate(Long roomWatchRate) {
            RoomWatchRate = roomWatchRate;
        }
    }

    public static class ShortInfo{
        /**
         * 账号类型，0-非企业号，1-企业号
         */
        private Integer AccountType;
        /**
         * 抖音头像
         */
        private String Avat;
        /**
         * 抖音号
         */
        private String AwemeShortID;
        /**
         * 抖音昵称
         */
        private String Nickname;

        public Integer getAccountType() {
            return AccountType;
        }

        public void setAccountType(Integer accountType) {
            AccountType = accountType;
        }

        public String getAvat() {
            return Avat;
        }

        public void setAvat(String avat) {
            Avat = avat;
        }

        public String getAwemeShortID() {
            return AwemeShortID;
        }

        public void setAwemeShortID(String awemeShortID) {
            AwemeShortID = awemeShortID;
        }

        public String getNickname() {
            return Nickname;
        }

        public void setNickname(String nickname) {
            Nickname = nickname;
        }
    }
}
