package com.dyj.applet.domain;

import java.util.List;

/**
 * 订单算价结果
 */
public class MarketingOrderCalculationResultInfo {

    /**
     * 订单维度总优惠金额，单位分，如订单满减
     */
    private Long order_total_discount_amount;

    /**
     * 商品维度总优惠金额，单位分，如咖啡第二杯半价
     */
    private Long goods_total_discount_amount;

    /**
     * 营销明细，包含该订单的订单维度和商品维度所有优惠信息 选填
     */
    private List<MarketingDetail> marketing_detail_info;

    public Long getOrder_total_discount_amount() {
        return order_total_discount_amount;
    }

    public MarketingOrderCalculationResultInfo setOrder_total_discount_amount(Long order_total_discount_amount) {
        this.order_total_discount_amount = order_total_discount_amount;
        return this;
    }

    public Long getGoods_total_discount_amount() {
        return goods_total_discount_amount;
    }

    public MarketingOrderCalculationResultInfo setGoods_total_discount_amount(Long goods_total_discount_amount) {
        this.goods_total_discount_amount = goods_total_discount_amount;
        return this;
    }

    public List<MarketingDetail> getMarketing_detail_info() {
        return marketing_detail_info;
    }

    public MarketingOrderCalculationResultInfo setMarketing_detail_info(List<MarketingDetail> marketing_detail_info) {
        this.marketing_detail_info = marketing_detail_info;
        return this;
    }
}
