package com.dyj.applet.domain.vo;

public class CreatePayOrderRefundVo {
    /**
     * <p>退款单平台订单号，长度<=64byte</p>
     */
    private String pay_refund_id;

    public String getPay_refund_id() {
        return pay_refund_id;
    }

    public CreatePayOrderRefundVo setPay_refund_id(String pay_refund_id) {
        this.pay_refund_id = pay_refund_id;
        return this;
    }
}
