package com.dyj.applet.domain;

public class AnalysisLiveDealData {
    /**
     * 小程序点击数，挂载该小程序的直播间中，该小程序被打开的次数
     */
    private Long click_count;
    /**
     * 创建订单金额（单位：分）
     */
    private Long create_order_amount;
    /**
     * 创建订单数
     */
    private Long create_order_count;
    /**
     * 创建订单人数
     */
    private Long create_user_count;
    /**
     * 客单价，单位：分
     */
    private Long customer_once_price;
    /**
     * 单均价，单位：分
     */
    private Long order_once_price;
    /**
     * 支付订单金额（单位：分）
     */
    private Long pay_order_amount;
    /**
     * 支付订单数
     */
    private Long pay_order_count;
    /**
     * 退款订单金额，单位：分
     */
    private Long refund_order_amount;
    /**
     * 退款订单数，该直播内发起退款的订单数量 (含部分退款)
     */
    private Long refund_order_cnt;
    /**
     * 退款订单金额
     */
    private Long refund_success_order_amount;
    /**
     * 退款订单数
     */
    private Long refund_success_order_cnt;
    /**
     * 退款订单人数
     */
    private Long refund_success_user_cnt;
    /**
     * 动销商品数，该直播内创建的订单，且销量>=1的商品sku数量
     */
    private Long sale_product_cnt;
    /**
     * 小程序曝光次数，挂载该小程序的直播间中，该小程序被曝光的次数
     */
    private Long show_count;
    /**
     * 支付人数，该直播内创建的订单，且订单支付成功的去重客户数
     */
    private Long success_user_cnt;
    /**
     * 时间，格式为“2022-12-27 15:04:05”
     */
    private String time;

    public Long getClick_count() {
        return click_count;
    }

    public void setClick_count(Long click_count) {
        this.click_count = click_count;
    }

    public Long getCreate_order_amount() {
        return create_order_amount;
    }

    public void setCreate_order_amount(Long create_order_amount) {
        this.create_order_amount = create_order_amount;
    }

    public Long getCreate_order_count() {
        return create_order_count;
    }

    public void setCreate_order_count(Long create_order_count) {
        this.create_order_count = create_order_count;
    }

    public Long getCreate_user_count() {
        return create_user_count;
    }

    public void setCreate_user_count(Long create_user_count) {
        this.create_user_count = create_user_count;
    }

    public Long getCustomer_once_price() {
        return customer_once_price;
    }

    public void setCustomer_once_price(Long customer_once_price) {
        this.customer_once_price = customer_once_price;
    }

    public Long getOrder_once_price() {
        return order_once_price;
    }

    public void setOrder_once_price(Long order_once_price) {
        this.order_once_price = order_once_price;
    }

    public Long getPay_order_amount() {
        return pay_order_amount;
    }

    public void setPay_order_amount(Long pay_order_amount) {
        this.pay_order_amount = pay_order_amount;
    }

    public Long getPay_order_count() {
        return pay_order_count;
    }

    public void setPay_order_count(Long pay_order_count) {
        this.pay_order_count = pay_order_count;
    }

    public Long getRefund_order_amount() {
        return refund_order_amount;
    }

    public void setRefund_order_amount(Long refund_order_amount) {
        this.refund_order_amount = refund_order_amount;
    }

    public Long getRefund_order_cnt() {
        return refund_order_cnt;
    }

    public void setRefund_order_cnt(Long refund_order_cnt) {
        this.refund_order_cnt = refund_order_cnt;
    }

    public Long getRefund_success_order_amount() {
        return refund_success_order_amount;
    }

    public void setRefund_success_order_amount(Long refund_success_order_amount) {
        this.refund_success_order_amount = refund_success_order_amount;
    }

    public Long getRefund_success_order_cnt() {
        return refund_success_order_cnt;
    }

    public void setRefund_success_order_cnt(Long refund_success_order_cnt) {
        this.refund_success_order_cnt = refund_success_order_cnt;
    }

    public Long getRefund_success_user_cnt() {
        return refund_success_user_cnt;
    }

    public void setRefund_success_user_cnt(Long refund_success_user_cnt) {
        this.refund_success_user_cnt = refund_success_user_cnt;
    }

    public Long getSale_product_cnt() {
        return sale_product_cnt;
    }

    public void setSale_product_cnt(Long sale_product_cnt) {
        this.sale_product_cnt = sale_product_cnt;
    }

    public Long getShow_count() {
        return show_count;
    }

    public void setShow_count(Long show_count) {
        this.show_count = show_count;
    }

    public Long getSuccess_user_cnt() {
        return success_user_cnt;
    }

    public void setSuccess_user_cnt(Long success_user_cnt) {
        this.success_user_cnt = success_user_cnt;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
