package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 16:30
 **/
public class CouponAttribute {

    private DirectDiscountCoupon direct_discount_coupon;

    private FullReductionCoupon full_reduction_coupon;

    public static CouponAttributeBuilder builder() {
        return new CouponAttributeBuilder();
    }

    public static class CouponAttributeBuilder {
        private DirectDiscountCoupon directDiscountCoupon;
        private FullReductionCoupon fullReductionCoupon;

        public CouponAttributeBuilder directDiscountCoupon(DirectDiscountCoupon directDiscountCoupon) {
            this.directDiscountCoupon = directDiscountCoupon;
            return this;
        }

        public CouponAttributeBuilder fullReductionCoupon(FullReductionCoupon fullReductionCoupon) {
            this.fullReductionCoupon = fullReductionCoupon;
            return this;
        }

        public CouponAttribute build() {
            CouponAttribute couponAttribute = new CouponAttribute();
            couponAttribute.setDirect_discount_coupon(directDiscountCoupon);
            couponAttribute.setFull_reduction_coupon(fullReductionCoupon);
            return couponAttribute;
        }

    }

    public DirectDiscountCoupon getDirect_discount_coupon() {
        return direct_discount_coupon;
    }

    public void setDirect_discount_coupon(DirectDiscountCoupon direct_discount_coupon) {
        this.direct_discount_coupon = direct_discount_coupon;
    }

    public FullReductionCoupon getFull_reduction_coupon() {
        return full_reduction_coupon;
    }

    public void setFull_reduction_coupon(FullReductionCoupon full_reduction_coupon) {
        this.full_reduction_coupon = full_reduction_coupon;
    }
}
