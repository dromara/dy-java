package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class QuerySignPayOrderQuery extends BaseQuery {

    /**
     * <p>开发者侧代扣单的单号，长度<=64byte，pay_order_id 与 out_pay_order_no 二选一</p> 选填
     */
    private String out_pay_order_no;
    /**
     * <p>平台侧代扣单的单号，长度<=64byte，pay_order_id 与 out_pay_order_no 二选一</p> 选填
     */
    private String pay_order_id;

    public String getOut_pay_order_no() {
        return out_pay_order_no;
    }

    public QuerySignPayOrderQuery setOut_pay_order_no(String out_pay_order_no) {
        this.out_pay_order_no = out_pay_order_no;
        return this;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public QuerySignPayOrderQuery setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public static QuerySignPayOrderQueryBuilder builder() {
        return new QuerySignPayOrderQueryBuilder();
    }

    public static final class QuerySignPayOrderQueryBuilder {
        private String out_pay_order_no;
        private String pay_order_id;
        private Integer tenantId;
        private String clientKey;

        private QuerySignPayOrderQueryBuilder() {
        }

        public QuerySignPayOrderQueryBuilder outPayOrderNo(String outPayOrderNo) {
            this.out_pay_order_no = outPayOrderNo;
            return this;
        }

        public QuerySignPayOrderQueryBuilder payOrderId(String payOrderId) {
            this.pay_order_id = payOrderId;
            return this;
        }

        public QuerySignPayOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QuerySignPayOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QuerySignPayOrderQuery build() {
            QuerySignPayOrderQuery querySignPayOrderQuery = new QuerySignPayOrderQuery();
            querySignPayOrderQuery.setOut_pay_order_no(out_pay_order_no);
            querySignPayOrderQuery.setPay_order_id(pay_order_id);
            querySignPayOrderQuery.setTenantId(tenantId);
            querySignPayOrderQuery.setClientKey(clientKey);
            return querySignPayOrderQuery;
        }
    }
}
