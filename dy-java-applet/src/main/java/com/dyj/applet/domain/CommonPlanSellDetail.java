package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 17:19
 **/
public class CommonPlanSellDetail {

    /**
     * 带货GMV，单位为分
     */
    private Long gmv;
    /**
     * 计划ID
     */
    private Long plan_id;
    /**
     * 达人佣金，单位为分
     */
    private Long talent_commission;
    /**
     * 已核销GMV，单位为分
     */
    private Long used_gmv;

    public Long getGmv() {
        return gmv;
    }

    public void setGmv(Long gmv) {
        this.gmv = gmv;
    }

    public Long getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Long plan_id) {
        this.plan_id = plan_id;
    }

    public Long getTalent_commission() {
        return talent_commission;
    }

    public void setTalent_commission(Long talent_commission) {
        this.talent_commission = talent_commission;
    }

    public Long getUsed_gmv() {
        return used_gmv;
    }

    public void setUsed_gmv(Long used_gmv) {
        this.used_gmv = used_gmv;
    }
}
