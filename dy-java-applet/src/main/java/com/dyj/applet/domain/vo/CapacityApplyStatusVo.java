package com.dyj.applet.domain.vo;

/**
 * @author danmo
 * @date 2024-07-12 11:43
 **/
public class CapacityApplyStatusVo {

    /**
     * 能力状态。1-申请中；2-申请成功；3-申请拒绝；4-能力关闭；5-能力封禁；6-可申请但未申请；7-不可申请
     */
    private Integer status;
    /**
     * 无法获得能力的原因。
     */
    private String reason;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
