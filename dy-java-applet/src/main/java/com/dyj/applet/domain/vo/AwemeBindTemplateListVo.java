package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AwemeBindTemplateInfo;

import java.util.Map;

/**
 * @author danmo
 * @date 2024-06-26 17:26
 **/
public class AwemeBindTemplateListVo {

    private Map<String, AwemeBindTemplateInfo> template_map;

    public Map<String, AwemeBindTemplateInfo> getTemplate_map() {
        return template_map;
    }

    public void setTemplate_map(Map<String, AwemeBindTemplateInfo> template_map) {
        this.template_map = template_map;
    }
}
