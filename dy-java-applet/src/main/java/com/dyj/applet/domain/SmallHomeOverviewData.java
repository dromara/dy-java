package com.dyj.applet.domain;

public class SmallHomeOverviewData {
    /**
     * 小程序app_id
     */
    private String app_id;
    /**
     * 小程序名称
     */
    private String app_name;
    /**
     * 核销订单金额(单位：分)
     */
    private Long delivery_success_amount;
    /**
     * 核销订单数
     */
    private  Integer delivery_success_order_cnt;
    /**
     * 累计主播数
     */
    private  Long live_anchors_cnt;
    /**
     * 累计直播时长(单位：秒)
     */
    private  Long live_duration;
    /**
     * 累计直播场次
     */
    private  Integer live_session_cnt;
    /**
     * 最高在线人数
     */
    private  Integer max_watch_user_cnt;
    /**
     * 支付订单金额(单位：分)
     */
    private  Long pay_amount;
    /**
     * 支付订单数
     */
    private  Integer pay_order_cnt;
    /**
     * 人均观看时长(累计观看时长/累计看播人数 (单位：秒))
     */
    private  Long per_watch_duration;

    /**
     * 已退款订单金额(单位：分)
     */
    private  Integer refund_success_amount;
    /**
     * 已退款订单数
     */
    private  Integer refund_success_order_cnt;
    /**
     * 累计观看时长(单位：秒)
     */
    private  Long watch_duration;
    /**
     * 累计看播人数
     */
    private Integer watch_user_cnt;

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public Long getDelivery_success_amount() {
        return delivery_success_amount;
    }

    public void setDelivery_success_amount(Long delivery_success_amount) {
        this.delivery_success_amount = delivery_success_amount;
    }

    public Integer getDelivery_success_order_cnt() {
        return delivery_success_order_cnt;
    }

    public void setDelivery_success_order_cnt(Integer delivery_success_order_cnt) {
        this.delivery_success_order_cnt = delivery_success_order_cnt;
    }

    public Long getLive_anchors_cnt() {
        return live_anchors_cnt;
    }

    public void setLive_anchors_cnt(Long live_anchors_cnt) {
        this.live_anchors_cnt = live_anchors_cnt;
    }

    public Long getLive_duration() {
        return live_duration;
    }

    public void setLive_duration(Long live_duration) {
        this.live_duration = live_duration;
    }

    public Integer getLive_session_cnt() {
        return live_session_cnt;
    }

    public void setLive_session_cnt(Integer live_session_cnt) {
        this.live_session_cnt = live_session_cnt;
    }

    public Integer getMax_watch_user_cnt() {
        return max_watch_user_cnt;
    }

    public void setMax_watch_user_cnt(Integer max_watch_user_cnt) {
        this.max_watch_user_cnt = max_watch_user_cnt;
    }

    public Long getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(Long pay_amount) {
        this.pay_amount = pay_amount;
    }

    public Integer getPay_order_cnt() {
        return pay_order_cnt;
    }

    public void setPay_order_cnt(Integer pay_order_cnt) {
        this.pay_order_cnt = pay_order_cnt;
    }

    public Long getPer_watch_duration() {
        return per_watch_duration;
    }

    public void setPer_watch_duration(Long per_watch_duration) {
        this.per_watch_duration = per_watch_duration;
    }

    public Integer getRefund_success_amount() {
        return refund_success_amount;
    }

    public void setRefund_success_amount(Integer refund_success_amount) {
        this.refund_success_amount = refund_success_amount;
    }

    public Integer getRefund_success_order_cnt() {
        return refund_success_order_cnt;
    }

    public void setRefund_success_order_cnt(Integer refund_success_order_cnt) {
        this.refund_success_order_cnt = refund_success_order_cnt;
    }

    public Long getWatch_duration() {
        return watch_duration;
    }

    public void setWatch_duration(Long watch_duration) {
        this.watch_duration = watch_duration;
    }

    public Integer getWatch_user_cnt() {
        return watch_user_cnt;
    }

    public void setWatch_user_cnt(Integer watch_user_cnt) {
        this.watch_user_cnt = watch_user_cnt;
    }
}
