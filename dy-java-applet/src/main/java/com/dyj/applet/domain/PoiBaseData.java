package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-06 14:58
 **/
public class PoiBaseData {

    /**
     * 日期
     */
    private String date;

    /**
     * 	详情页pv
     */
    private Integer detail_vv;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getDetail_vv() {
        return detail_vv;
    }

    public void setDetail_vv(Integer detail_vv) {
        this.detail_vv = detail_vv;
    }
}
