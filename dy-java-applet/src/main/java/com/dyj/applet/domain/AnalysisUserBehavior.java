package com.dyj.applet.domain;

public class AnalysisUserBehavior {

    /**
     * 活跃用户数，访问小程序页面的用户数，同一用户多次访问不重复计
     */
    private Long active_user_num;

    /**
     * 次均停留时长（单位：毫秒），平均每次打开小程序停留在小程序页面的总时长，即时段内所有用户总停留时长 / 总打开次数
     */
    private Double avg_stay_time;

    /**
     * 新增用户数，首次访问小程序页面的用户数，同一用户多次访问不重复计
     */
    private Long new_user_num;

    /**
     * 打开次数，启动小程序的次数
     */
    private Long open_time;

    /**
     * 人均打开次数，所选时段内，平均每人打开小程序的总次数，即时段内所有用户打开总次数 / 总活跃人数
     */
    private Double per_user_open_time;

    /**
     * 人均停留时长（单位：毫秒），人均停留在小程序页面的总时长，即时段内所有用户总停留时长 / 总活跃人数
     */
    private Double per_user_stay_time;

    /**
     * 分享次数，点击分享按钮、调起分享框的次数（包含实际未分享的次数）
     */
    private Long share_time;

    /**
     * 时间，日期格式为“2006-01-02”；小时格式为“15:04:05”
     */
    private String time;

    /**
     * 时间类型
     */
    private Integer time_type;

    /**
     * 累计用户数，所选时段最后一天为止，历史上所有启动过小程序的设备数去重
     */
    private Long total_user_num;


    public Long getActive_user_num() {
        return active_user_num;
    }

    public void setActive_user_num(Long active_user_num) {
        this.active_user_num = active_user_num;
    }

    public Double getAvg_stay_time() {
        return avg_stay_time;
    }

    public void setAvg_stay_time(Double avg_stay_time) {
        this.avg_stay_time = avg_stay_time;
    }

    public Long getNew_user_num() {
        return new_user_num;
    }

    public void setNew_user_num(Long new_user_num) {
        this.new_user_num = new_user_num;
    }

    public Long getOpen_time() {
        return open_time;
    }

    public void setOpen_time(Long open_time) {
        this.open_time = open_time;
    }

    public Double getPer_user_open_time() {
        return per_user_open_time;
    }

    public void setPer_user_open_time(Double per_user_open_time) {
        this.per_user_open_time = per_user_open_time;
    }

    public Double getPer_user_stay_time() {
        return per_user_stay_time;
    }

    public void setPer_user_stay_time(Double per_user_stay_time) {
        this.per_user_stay_time = per_user_stay_time;
    }

    public Long getShare_time() {
        return share_time;
    }

    public void setShare_time(Long share_time) {
        this.share_time = share_time;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getTime_type() {
        return time_type;
    }

    public void setTime_type(Integer time_type) {
        this.time_type = time_type;
    }

    public Long getTotal_user_num() {
        return total_user_num;
    }

    public void setTotal_user_num(Long total_user_num) {
        this.total_user_num = total_user_num;
    }
}
