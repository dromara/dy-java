package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.DoudianShop;

import java.util.List;

/**
 * @author danmo
 * @date 2024-06-12 14:15
 **/
public class DoudianShopVo {

    private List<DoudianShop> ShopIdList;

    public List<DoudianShop> getShopIdList() {
        return ShopIdList;
    }

    public void setShopIdList(List<DoudianShop> shopIdList) {
        ShopIdList = shopIdList;
    }
}
