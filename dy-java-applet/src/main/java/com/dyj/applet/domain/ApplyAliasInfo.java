package com.dyj.applet.domain;

public class ApplyAliasInfo {

    /**
     * 别名
     */
    private String alias_word;

    /**
     * 审核原因
     */
    private String audit_reason;

    /**
     * 审核状态
     */
    private Integer audit_status;

    /**
     * 创建时间
     */
    private Long ctime;

    /**
     * 更新时间
     */
    private Long utime;

    public String getAlias_word() {
        return alias_word;
    }

    public void setAlias_word(String alias_word) {
        this.alias_word = alias_word;
    }

    public String getAudit_reason() {
        return audit_reason;
    }

    public void setAudit_reason(String audit_reason) {
        this.audit_reason = audit_reason;
    }

    public Integer getAudit_status() {
        return audit_status;
    }

    public void setAudit_status(Integer audit_status) {
        this.audit_status = audit_status;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getUtime() {
        return utime;
    }

    public void setUtime(Long utime) {
        this.utime = utime;
    }
}
