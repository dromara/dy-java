package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.PoiBaseData;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 14:57
 **/
public class PoiBaseDataVo extends BaseVo {

    private List<PoiBaseData> result_list;

    public List<PoiBaseData> getResult_list() {
        return result_list;
    }

    public void setResult_list(List<PoiBaseData> result_list) {
        this.result_list = result_list;
    }
}
