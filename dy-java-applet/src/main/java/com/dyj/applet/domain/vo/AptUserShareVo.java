package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AptUserItem;
import com.dyj.applet.domain.AptUserShare;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-24 14:18
 **/
public class AptUserShareVo extends BaseVo {

    private List<AptUserShare> result_list;

    public List<AptUserShare> getResult_list() {
        return result_list;
    }

    public void setResult_list(List<AptUserShare> result_list) {
        this.result_list = result_list;
    }
}
