package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-06-17 15:35
 **/
public class AdIncome {

    /**
     *日期，格式“2006-01-02”
     */
    private String date;

    /**
     * 收入金额，单位：分
     */
    private Long income;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getIncome() {
        return income;
    }

    public void setIncome(Long income) {
        this.income = income;
    }
}
