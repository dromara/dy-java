package com.dyj.applet.domain;

public class CertificatesSkuInfo {

    /**
     * sku商品id
     */
    private String sku_id;

    /**
     * 商品名称
     */
    private String title;

    /**
     * 商品类型
     * 1 团购券
     * 2 代金券
     * 3 次卡
     */
    private Integer groupon_type;

    /**
     * 团购市场价，单位分
     */
    private Long market_price;

    /**
     * 团购售卖开始时间，时间戳，单位秒
     */
    private Long sold_start_time;

    /**
     * 商家系统（第三方）团购id
     * 选填
     */
    private String third_sku_id;

    /**
     * 商家团购账号id
     */
    private String account_id;

    /**
     * 商品spu_id
     * 选填
     */
    private String spu_id;

    /**
     * 外部商品id
     * 选填
     */
    private String out_id;

    public String getSku_id() {
        return sku_id;
    }

    public CertificatesSkuInfo setSku_id(String sku_id) {
        this.sku_id = sku_id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public CertificatesSkuInfo setTitle(String title) {
        this.title = title;
        return this;
    }

    public Integer getGroupon_type() {
        return groupon_type;
    }

    public CertificatesSkuInfo setGroupon_type(Integer groupon_type) {
        this.groupon_type = groupon_type;
        return this;
    }

    public Long getMarket_price() {
        return market_price;
    }

    public CertificatesSkuInfo setMarket_price(Long market_price) {
        this.market_price = market_price;
        return this;
    }

    public Long getSold_start_time() {
        return sold_start_time;
    }

    public CertificatesSkuInfo setSold_start_time(Long sold_start_time) {
        this.sold_start_time = sold_start_time;
        return this;
    }

    public String getThird_sku_id() {
        return third_sku_id;
    }

    public CertificatesSkuInfo setThird_sku_id(String third_sku_id) {
        this.third_sku_id = third_sku_id;
        return this;
    }

    public String getAccount_id() {
        return account_id;
    }

    public CertificatesSkuInfo setAccount_id(String account_id) {
        this.account_id = account_id;
        return this;
    }

    public String getSpu_id() {
        return spu_id;
    }

    public CertificatesSkuInfo setSpu_id(String spu_id) {
        this.spu_id = spu_id;
        return this;
    }

    public String getOut_id() {
        return out_id;
    }

    public CertificatesSkuInfo setOut_id(String out_id) {
        this.out_id = out_id;
        return this;
    }
}
