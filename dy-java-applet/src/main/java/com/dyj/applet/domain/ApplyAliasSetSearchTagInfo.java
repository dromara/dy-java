package com.dyj.applet.domain;

public class ApplyAliasSetSearchTagInfo {

    /**
     * 搜索标签审核拒绝原因
     */
    private String audit_reason;

    /**
     * 搜索标签审核状态，1-审核中；2-审核通过；3-审核拒绝
     */
    private Integer audit_status;

    /**
     * 搜索标签
     */
    private String search_tag;

    public String getAudit_reason() {
        return audit_reason;
    }

    public void setAudit_reason(String audit_reason) {
        this.audit_reason = audit_reason;
    }

    public Integer getAudit_status() {
        return audit_status;
    }

    public void setAudit_status(Integer audit_status) {
        this.audit_status = audit_status;
    }

    public String getSearch_tag() {
        return search_tag;
    }

    public void setSearch_tag(String search_tag) {
        this.search_tag = search_tag;
    }
}
