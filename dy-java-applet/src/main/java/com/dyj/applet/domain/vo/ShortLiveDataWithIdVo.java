package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.ShortLiveDataWithIdData;

import java.util.List;
import java.util.Map;

public class ShortLiveDataWithIdVo {

    /**
     * 短视频/直播间详细数据列表,每一个元素表示一个短视频/直播间数据，列表按短视频发布时间/直播间开播时间倒序排列
     */
    private List<ShortLiveDataWithIdData> data_list;

    /**
     * 在查询条件范围内发布视频/直播间的总数
     */
    private Integer sum;

    /**
     * 此字段只有在查询单个直播间时才有数据返回，用于展示跨天直播的直播间在每一天所产生的详细数据，key:每天整点时间戳，value:直播间数据
     */
    private Map<Long,ShortLiveDataWithIdData> detail_data;

    public List<ShortLiveDataWithIdData> getData_list() {
        return data_list;
    }

    public void setData_list(List<ShortLiveDataWithIdData> data_list) {
        this.data_list = data_list;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public Map<Long, ShortLiveDataWithIdData> getDetail_data() {
        return detail_data;
    }

    public void setDetail_data(Map<Long, ShortLiveDataWithIdData> detail_data) {
        this.detail_data = detail_data;
    }
}
