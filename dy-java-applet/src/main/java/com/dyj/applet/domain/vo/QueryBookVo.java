package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.BookInfo;

import java.util.List;

public class QueryBookVo {

    private List<BookInfo> book_info_list;

    public List<BookInfo> getBook_info_list() {
        return book_info_list;
    }

    public QueryBookVo setBook_info_list(List<BookInfo> book_info_list) {
        this.book_info_list = book_info_list;
        return this;
    }
}
