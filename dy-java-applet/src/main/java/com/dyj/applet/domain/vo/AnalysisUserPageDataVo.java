package com.dyj.applet.domain.vo;

import java.util.List;

public class AnalysisUserPageDataVo {

    private List<AnalysisUserPageData> page_list;

    public List<AnalysisUserPageData> getPage_list() {
        return page_list;
    }

    public void setPage_list(List<AnalysisUserPageData> page_list) {
        this.page_list = page_list;
    }

    public static class AnalysisUserPageData {
        /**
         * 页面URL
         */
        private String page;
        /**
         * 页面次均停留时长（单位：毫秒）
         */
        private Double page_avg_stay_time;
        /**
         * 页面浏览次数
         */
        private Integer page_pv;
        /**
         * 页面分享次数
         */
        private Integer page_share_time;
        /**
         * 页面访问人数
         */
        private Integer page_uv;

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public Double getPage_avg_stay_time() {
            return page_avg_stay_time;
        }

        public void setPage_avg_stay_time(Double page_avg_stay_time) {
            this.page_avg_stay_time = page_avg_stay_time;
        }

        public Integer getPage_pv() {
            return page_pv;
        }

        public void setPage_pv(Integer page_pv) {
            this.page_pv = page_pv;
        }

        public Integer getPage_share_time() {
            return page_share_time;
        }

        public void setPage_share_time(Integer page_share_time) {
            this.page_share_time = page_share_time;
        }

        public Integer getPage_uv() {
            return page_uv;
        }

        public void setPage_uv(Integer page_uv) {
            this.page_uv = page_uv;
        }
    }
}
