package com.dyj.applet.domain;

/**
 * 商户余额结算信息
 */
public class ChannelBalanceAccountSettleInfo {

    /**
     * <p>银行卡结算时，银行卡对应银行名称</p>
     */
    private String bank_name;
    /**
     * <p>银行卡结算时，银行卡号</p>
     */
    private String bankcard_no;
    /**
     * <p>支付宝结算时，支付宝账号</p>
     */
    private String settle_account;
    /**
     * <p>结算类型枚举值：</p><p>1: 银行卡结算</p><p>2: 支付宝结算</p>
     */
    private Integer settle_type;

    public String getBank_name() {
        return bank_name;
    }

    public ChannelBalanceAccountSettleInfo setBank_name(String bank_name) {
        this.bank_name = bank_name;
        return this;
    }

    public String getBankcard_no() {
        return bankcard_no;
    }

    public ChannelBalanceAccountSettleInfo setBankcard_no(String bankcard_no) {
        this.bankcard_no = bankcard_no;
        return this;
    }

    public String getSettle_account() {
        return settle_account;
    }

    public ChannelBalanceAccountSettleInfo setSettle_account(String settle_account) {
        this.settle_account = settle_account;
        return this;
    }

    public Integer getSettle_type() {
        return settle_type;
    }

    public ChannelBalanceAccountSettleInfo setSettle_type(Integer settle_type) {
        this.settle_type = settle_type;
        return this;
    }
}
