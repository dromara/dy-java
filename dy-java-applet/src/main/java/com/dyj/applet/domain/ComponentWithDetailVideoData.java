package com.dyj.applet.domain;

public class ComponentWithDetailVideoData {

    /**
     *
     * 短视频完播率
     */
    private Long CompletionRate;
    /**
     *
     * 短视频地址
     */
    private String ItemAddr;
    /**
     * 短视频封面
     */
    private String ItemCover;
    /**
     *
     * 短视频创建时间
     */
    private Long ItemCreateTime;
    /**
     * 短视频时长
     */
    private Long ItemDuration;
    /**
     * 短视频ID
     */
    private Long ItemId;

    /**
     * 短视频标题
     */
    private String ItemTitle;

    /**
     * 短视频播放次数
     */
    private Long ItemVv;

    private ComponentWithDetailData  Data;

    private LiveWithShortData.ShortInfo ShortInfo;

    public Long getCompletionRate() {
        return CompletionRate;
    }

    public void setCompletionRate(Long completionRate) {
        CompletionRate = completionRate;
    }

    public String getItemAddr() {
        return ItemAddr;
    }

    public void setItemAddr(String itemAddr) {
        ItemAddr = itemAddr;
    }

    public String getItemCover() {
        return ItemCover;
    }

    public void setItemCover(String itemCover) {
        ItemCover = itemCover;
    }

    public Long getItemCreateTime() {
        return ItemCreateTime;
    }

    public void setItemCreateTime(Long itemCreateTime) {
        ItemCreateTime = itemCreateTime;
    }

    public Long getItemDuration() {
        return ItemDuration;
    }

    public void setItemDuration(Long itemDuration) {
        ItemDuration = itemDuration;
    }

    public Long getItemId() {
        return ItemId;
    }

    public void setItemId(Long itemId) {
        ItemId = itemId;
    }

    public String getItemTitle() {
        return ItemTitle;
    }

    public void setItemTitle(String itemTitle) {
        ItemTitle = itemTitle;
    }

    public Long getItemVv() {
        return ItemVv;
    }

    public void setItemVv(Long itemVv) {
        ItemVv = itemVv;
    }

    public ComponentWithDetailData getData() {
        return Data;
    }

    public void setData(ComponentWithDetailData data) {
        Data = data;
    }

    public LiveWithShortData.ShortInfo getShortInfo() {
        return ShortInfo;
    }

    public void setShortInfo(LiveWithShortData.ShortInfo shortInfo) {
        ShortInfo = shortInfo;
    }
}
