package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class OrderCanUseQuery extends BaseQuery {

    /**
     * 抖音内部交易订单号，长度<64byte。
     */
    private String order_id;

    /**
     * 适用门店poi_id
     * 选填
     */
    private String poi_id;

    public String getOrder_id() {
        return order_id;
    }

    public OrderCanUseQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public OrderCanUseQuery setPoi_id(String poi_id) {
        this.poi_id = poi_id;
        return this;
    }

    public static OrderCanUseQueryBuilder builder() {
        return new OrderCanUseQueryBuilder();
    }

    public static final class OrderCanUseQueryBuilder {
        private String order_id;
        private String poi_id;
        private Integer tenantId;
        private String clientKey;

        private OrderCanUseQueryBuilder() {
        }

        public OrderCanUseQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public OrderCanUseQueryBuilder poiId(String poiId) {
            this.poi_id = poiId;
            return this;
        }

        public OrderCanUseQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public OrderCanUseQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public OrderCanUseQuery build() {
            OrderCanUseQuery orderCanUseQuery = new OrderCanUseQuery();
            orderCanUseQuery.setOrder_id(order_id);
            orderCanUseQuery.setPoi_id(poi_id);
            orderCanUseQuery.setTenantId(tenantId);
            orderCanUseQuery.setClientKey(clientKey);
            return orderCanUseQuery;
        }
    }
}
