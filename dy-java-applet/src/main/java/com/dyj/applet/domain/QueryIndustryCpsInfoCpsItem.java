package com.dyj.applet.domain;

/**
 * 订单 CPS 信息。
 */
public class QueryIndustryCpsInfoCpsItem {

    /**
     * 佣金，单位分
     */
    private Long commission_amount;
    /**
     * 分佣比例，万分位
     */
    private Integer commission_rate;
    /**
     * 达人抖音号
     */
    private String commission_user_douyinid;
    /**
     * 达人抖音昵称
     */
    private String commission_user_nickname;
    /**
     * 短视频/直播间 ID
     */
    private Long item_id;
    /**
     * 抖音开平侧的商品单号，只存在交易系统中
     */
    private String item_order_id;
    /**
     * 售价，单位分
     */
    private Long sell_amount;
    /**
     * 分佣类型：
     */
    private Integer source_type;
    /**
     * CPS 订单状态，交易系统下为子单状态
     */
    private Integer status;
    /**
     * cps任务的id
     */
    private String task_id;

    public Long getCommission_amount() {
        return commission_amount;
    }

    public QueryIndustryCpsInfoCpsItem setCommission_amount(Long commission_amount) {
        this.commission_amount = commission_amount;
        return this;
    }

    public Integer getCommission_rate() {
        return commission_rate;
    }

    public QueryIndustryCpsInfoCpsItem setCommission_rate(Integer commission_rate) {
        this.commission_rate = commission_rate;
        return this;
    }

    public String getCommission_user_douyinid() {
        return commission_user_douyinid;
    }

    public QueryIndustryCpsInfoCpsItem setCommission_user_douyinid(String commission_user_douyinid) {
        this.commission_user_douyinid = commission_user_douyinid;
        return this;
    }

    public String getCommission_user_nickname() {
        return commission_user_nickname;
    }

    public QueryIndustryCpsInfoCpsItem setCommission_user_nickname(String commission_user_nickname) {
        this.commission_user_nickname = commission_user_nickname;
        return this;
    }

    public Long getItem_id() {
        return item_id;
    }

    public QueryIndustryCpsInfoCpsItem setItem_id(Long item_id) {
        this.item_id = item_id;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public QueryIndustryCpsInfoCpsItem setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }

    public Long getSell_amount() {
        return sell_amount;
    }

    public QueryIndustryCpsInfoCpsItem setSell_amount(Long sell_amount) {
        this.sell_amount = sell_amount;
        return this;
    }

    public Integer getSource_type() {
        return source_type;
    }

    public QueryIndustryCpsInfoCpsItem setSource_type(Integer source_type) {
        this.source_type = source_type;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public QueryIndustryCpsInfoCpsItem setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getTask_id() {
        return task_id;
    }

    public QueryIndustryCpsInfoCpsItem setTask_id(String task_id) {
        this.task_id = task_id;
        return this;
    }
}
