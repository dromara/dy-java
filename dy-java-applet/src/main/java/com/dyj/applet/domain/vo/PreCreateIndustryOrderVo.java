package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.IndustryOrderItemOrderDetail;
import com.dyj.applet.domain.IndustryOrderItemOrderInfo;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * 生活服务交易系统->预下单->开发者发起下单返回值
 */
public class PreCreateIndustryOrderVo extends BaseVo {

    /**
     * 抖音开平侧生成的订单号
     */
    private String order_id;

    /**
     * 开发者系统生成的订单号
     */
    private String out_order_no;

    /**
     * 调起收银台的支付订单号
     */
    private String pay_order_id;

    /**
     * 调起收银台的 token
     */
    private String pay_order_token;

    /**
     * 商品 item_order 信息
     */
    private List<IndustryOrderItemOrderInfo> item_order_info_list;

    public String getOrder_id() {
        return order_id;
    }

    public PreCreateIndustryOrderVo setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public PreCreateIndustryOrderVo setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public PreCreateIndustryOrderVo setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public String getPay_order_token() {
        return pay_order_token;
    }

    public PreCreateIndustryOrderVo setPay_order_token(String pay_order_token) {
        this.pay_order_token = pay_order_token;
        return this;
    }

    public List<IndustryOrderItemOrderInfo> getItem_order_info_list() {
        return item_order_info_list;
    }

    public PreCreateIndustryOrderVo setItem_order_info_list(List<IndustryOrderItemOrderInfo> item_order_info_list) {
        this.item_order_info_list = item_order_info_list;
        return this;
    }
}
