package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AptUserLike;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-24 14:11
 **/
public class AptUserLikeVo {

    private List<AptUserLike> result_list;

    public List<AptUserLike> getResult_list() {
        return result_list;
    }

    public void setResult_list(List<AptUserLike> result_list) {
        this.result_list = result_list;
    }
}
