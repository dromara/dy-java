package com.dyj.applet.domain.vo;

/**
 * 发起分账返回值
 */
public class CreateSettleVo {

    /**
     * <p>抖音开平侧分账单 id</p>
     */
    private String settle_id;
    /**
     * <p>抖音开平侧底层分账单id</p>
     */
    private String wallet_settle_id;

    public String getSettle_id() {
        return settle_id;
    }

    public CreateSettleVo setSettle_id(String settle_id) {
        this.settle_id = settle_id;
        return this;
    }

    public String getWallet_settle_id() {
        return wallet_settle_id;
    }

    public CreateSettleVo setWallet_settle_id(String wallet_settle_id) {
        this.wallet_settle_id = wallet_settle_id;
        return this;
    }
}
