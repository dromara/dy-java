package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 14:11
 **/
public class SupplierHotelPolicyBreakFast {

    /**
     * 早餐价格(单位人民币分)
     */
    private String price;

    /**
     * 早餐类型。
     * 0 - 无早餐,
     * 1 - 早餐,
     * 2 - 自助早餐
     */
    private String type;

    public static SupplierHotelPolicyBreakFastBuilder builder() {
        return new SupplierHotelPolicyBreakFastBuilder();
    }

    public static class SupplierHotelPolicyBreakFastBuilder {
        private String price;
        private String type;
        public SupplierHotelPolicyBreakFastBuilder price(String price) {
            this.price = price;
            return this;
        }
        public SupplierHotelPolicyBreakFastBuilder type(String type) {
            this.type = type;
            return this;
        }
        public SupplierHotelPolicyBreakFast build() {
            SupplierHotelPolicyBreakFast supplierHotelPolicyBreakFast = new SupplierHotelPolicyBreakFast();
            supplierHotelPolicyBreakFast.setPrice(price);
            supplierHotelPolicyBreakFast.setType(type);
            return supplierHotelPolicyBreakFast;
        }
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
