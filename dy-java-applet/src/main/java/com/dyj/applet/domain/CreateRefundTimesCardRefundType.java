package com.dyj.applet.domain;

/**
 * 次卡退款类型
 */
public class CreateRefundTimesCardRefundType {

    /**
     * 次卡退款类型,1-退剩余次数,2-全部退
     */
    private Long times_card_refund_type;

    public Long getTimes_card_refund_type() {
        return times_card_refund_type;
    }

    public CreateRefundTimesCardRefundType setTimes_card_refund_type(Long times_card_refund_type) {
        this.times_card_refund_type = times_card_refund_type;
        return this;
    }
}
