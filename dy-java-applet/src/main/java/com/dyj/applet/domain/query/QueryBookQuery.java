package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class QueryBookQuery extends BaseQuery {

    /**
     * 抖音侧订单号，len(order_id) <= 64 byte
     * 选填
     */
    private String order_id;

    /**
     * 预约单号，len(book_id) <= 64 byte
     * 选填
     */
    private String book_id;

    public String getOrder_id() {
        return order_id;
    }

    public QueryBookQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getBook_id() {
        return book_id;
    }

    public QueryBookQuery setBook_id(String book_id) {
        this.book_id = book_id;
        return this;
    }

    public static QueryBookQueryBuilder builder() {
        return new QueryBookQueryBuilder();
    }

    public static final class QueryBookQueryBuilder {
        private String order_id;
        private String book_id;
        private Integer tenantId;
        private String clientKey;

        private QueryBookQueryBuilder() {
        }

        public QueryBookQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public QueryBookQueryBuilder bookId(String bookId) {
            this.book_id = bookId;
            return this;
        }

        public QueryBookQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryBookQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryBookQuery build() {
            QueryBookQuery queryBookQuery = new QueryBookQuery();
            queryBookQuery.setOrder_id(order_id);
            queryBookQuery.setBook_id(book_id);
            queryBookQuery.setTenantId(tenantId);
            queryBookQuery.setClientKey(clientKey);
            return queryBookQuery;
        }
    }
}
