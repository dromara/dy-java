package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.ComponentWithDetailDataExt;

import java.util.List;

public class AnalysisComponentWithDataVo {

    private Long Total;
    private List<ComponentWithDetailDataExt> DataList;

    public Long getTotal() {
        return Total;
    }

    public void setTotal(Long total) {
        Total = total;
    }

    public List<ComponentWithDetailDataExt> getDataList() {
        return DataList;
    }

    public void setDataList(List<ComponentWithDetailDataExt> dataList) {
        DataList = dataList;
    }
}
