package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class TerminateSignQuery extends BaseQuery {

    /**
     * 平台侧签约单的单号，长度<=64byte
     */
    private String auth_order_id;

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public TerminateSignQuery setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public static TerminateSignQueryBuilder builder() {
        return new TerminateSignQueryBuilder();
    }

    public static final class TerminateSignQueryBuilder {
        private String auth_order_id;
        private Integer tenantId;
        private String clientKey;

        private TerminateSignQueryBuilder() {
        }

        public TerminateSignQueryBuilder authOrderId(String authOrderId) {
            this.auth_order_id = authOrderId;
            return this;
        }

        public TerminateSignQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public TerminateSignQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public TerminateSignQuery build() {
            TerminateSignQuery terminateSignQuery = new TerminateSignQuery();
            terminateSignQuery.setAuth_order_id(auth_order_id);
            terminateSignQuery.setTenantId(tenantId);
            terminateSignQuery.setClientKey(clientKey);
            return terminateSignQuery;
        }
    }
}
