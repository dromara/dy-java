package com.dyj.applet.domain;

import java.util.List;

/**
 * 商品 item_order 信息
 */
public class IndustryOrderItemOrderInfo {

    /**
     * 商品 id
     */
    private String goods_id;

    /**
     * item_order_id 列表，id 个数与下单时对应 goods_id 的 quantity 一致
     */
    private List<String> item_order_id_list;

    /**
     * 商品 item_order 详细信息
     */
    private List<IndustryOrderItemOrderDetail> item_order_detail;

    public String getGoods_id() {
        return goods_id;
    }

    public IndustryOrderItemOrderInfo setGoods_id(String goods_id) {
        this.goods_id = goods_id;
        return this;
    }

    public List<String> getItem_order_id_list() {
        return item_order_id_list;
    }

    public IndustryOrderItemOrderInfo setItem_order_id_list(List<String> item_order_id_list) {
        this.item_order_id_list = item_order_id_list;
        return this;
    }

    public List<IndustryOrderItemOrderDetail> getItem_order_detail() {
        return item_order_detail;
    }

    public IndustryOrderItemOrderInfo setItem_order_detail(List<IndustryOrderItemOrderDetail> item_order_detail) {
        this.item_order_detail = item_order_detail;
        return this;
    }
}
