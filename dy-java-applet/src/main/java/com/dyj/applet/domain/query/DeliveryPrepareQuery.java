package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 验券准备请求值
 */
public class DeliveryPrepareQuery extends BaseQuery {

    /**
     * 用户券码 选填
     */
    private String code;
    /**
     * 从二维码解析出的 encrypted_data 选填
     */
    private String encrypted_data;

    public String getCode() {
        return code;
    }

    public DeliveryPrepareQuery setCode(String code) {
        this.code = code;
        return this;
    }

    public String getEncrypted_data() {
        return encrypted_data;
    }

    public DeliveryPrepareQuery setEncrypted_data(String encrypted_data) {
        this.encrypted_data = encrypted_data;
        return this;
    }

    public static DeliveryPrepareQueryBuilder builder() {
        return new DeliveryPrepareQueryBuilder();
    }

    public static final class DeliveryPrepareQueryBuilder {
        private String code;
        private String encrypted_data;
        private Integer tenantId;
        private String clientKey;

        private DeliveryPrepareQueryBuilder() {
        }

        public DeliveryPrepareQueryBuilder code(String code) {
            this.code = code;
            return this;
        }

        public DeliveryPrepareQueryBuilder encryptedData(String encryptedData) {
            this.encrypted_data = encryptedData;
            return this;
        }

        public DeliveryPrepareQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public DeliveryPrepareQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public DeliveryPrepareQuery build() {
            DeliveryPrepareQuery deliveryPrepareQuery = new DeliveryPrepareQuery();
            deliveryPrepareQuery.setCode(code);
            deliveryPrepareQuery.setEncrypted_data(encrypted_data);
            deliveryPrepareQuery.setTenantId(tenantId);
            deliveryPrepareQuery.setClientKey(clientKey);
            return deliveryPrepareQuery;
        }
    }
}
