package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QuerySignRefundData;

import java.util.List;

public class QuerySignRefundVo {

    /**
     * <p>退款信息列表</p>
     */
    private List<QuerySignRefundData> refund_list;

    public List<QuerySignRefundData> getRefund_list() {
        return refund_list;
    }

    public QuerySignRefundVo setRefund_list(List<QuerySignRefundData> refund_list) {
        this.refund_list = refund_list;
        return this;
    }
}
