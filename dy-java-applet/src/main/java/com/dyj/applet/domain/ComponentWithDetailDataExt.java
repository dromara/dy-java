package com.dyj.applet.domain;

public class ComponentWithDetailDataExt extends ComponentWithDetailData{

    /**
     * 组件ID
     */
    private String ComponentId;
    /**
     * 组件名称
     */
    private String ComponentName;

    public String getComponentId() {
        return ComponentId;
    }

    public void setComponentId(String componentId) {
        ComponentId = componentId;
    }

    public String getComponentName() {
        return ComponentName;
    }

    public void setComponentName(String componentName) {
        ComponentName = componentName;
    }
}
