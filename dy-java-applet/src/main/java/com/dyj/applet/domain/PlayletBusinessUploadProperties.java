package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-21 10:29
 **/
public class PlayletBusinessUploadProperties {

    /**
     * 权益生效维度类型：
     * 1: 小程序
     * 2: 剧
     */
    private Integer benefit_type;
    /**
     * 权益开始时间
     */
    private Long benefit_start_time;
    /**
     * 权益生效时长（天）
     * -1表示永久
     * 30天，365天等
     */
    private Integer benefit_duration;
    /**
     * 订单ID（motb开头的）
     */
    private String order_id;
    /**
     * 巨量商品库-短剧名称，当权益生效维度为剧且广告来源客户必传
     */
    private String playlet_name;
    /**
     * 开放平台内容库-短剧ID，当权益生效维度为剧时传
     */
    private String album_id;

    public static PlayletBusinessUploadPropertiesBuilder builder() {
        return new PlayletBusinessUploadPropertiesBuilder();
    }

    public static class PlayletBusinessUploadPropertiesBuilder {
        /**
         * 权益生效维度类型：
         * 1: 小程序
         * 2: 剧
         */
        private Integer benefitType;
        /**
         * 权益开始时间
         */
        private Long benefitStartTime;
        /**
         * 权益生效时长（天）
         * -1表示永久
         * 30天，365天等
         */
        private Integer benefitDuration;
        /**
         * 订单ID（motb开头的）
         */
        private String orderId;
        /**
         * 巨量商品库-短剧名称，当权益生效维度为剧且广告来源客户必传
         */
        private String playletName;
        /**
         * 开放平台内容库-短剧ID，当权益生效维度为剧时传
         */
        private String albumId;

        public PlayletBusinessUploadPropertiesBuilder benefitType(Integer benefitType) {
            this.benefitType = benefitType;
            return this;
        }

        public PlayletBusinessUploadPropertiesBuilder benefitStartTime(Long benefitStartTime) {
            this.benefitStartTime = benefitStartTime;
            return this;
        }

        public PlayletBusinessUploadPropertiesBuilder benefitDuration(Integer benefitDuration) {
            this.benefitDuration = benefitDuration;
            return this;
        }

        public PlayletBusinessUploadPropertiesBuilder orderId(String orderId) {
            this.orderId = orderId;
            return this;
        }

        public PlayletBusinessUploadPropertiesBuilder playletName(String playletName) {
            this.playletName = playletName;
            return this;
        }

        public PlayletBusinessUploadPropertiesBuilder albumId(String albumId) {
            this.albumId = albumId;
            return this;
        }

        public PlayletBusinessUploadProperties build() {
            PlayletBusinessUploadProperties playletBusinessUploadProperties = new PlayletBusinessUploadProperties();
            playletBusinessUploadProperties.setBenefit_type(benefitType);
            playletBusinessUploadProperties.setBenefit_start_time(benefitStartTime);
            playletBusinessUploadProperties.setBenefit_duration(benefitDuration);
            playletBusinessUploadProperties.setOrder_id(orderId);
            playletBusinessUploadProperties.setPlaylet_name(playletName);
            playletBusinessUploadProperties.setAlbum_id(albumId);
            return playletBusinessUploadProperties;
        }
    }

    public Integer getBenefit_type() {
        return benefit_type;
    }

    public void setBenefit_type(Integer benefit_type) {
        this.benefit_type = benefit_type;
    }

    public Long getBenefit_start_time() {
        return benefit_start_time;
    }

    public void setBenefit_start_time(Long benefit_start_time) {
        this.benefit_start_time = benefit_start_time;
    }

    public Integer getBenefit_duration() {
        return benefit_duration;
    }

    public void setBenefit_duration(Integer benefit_duration) {
        this.benefit_duration = benefit_duration;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getPlaylet_name() {
        return playlet_name;
    }

    public void setPlaylet_name(String playlet_name) {
        this.playlet_name = playlet_name;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }
}
