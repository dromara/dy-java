package com.dyj.applet.domain;

import java.util.List;
import java.util.Map;

/**
 * @author danmo
 * @date 2024-06-24 16:27
 **/
public class AuditTemplateContent {

    /**
     * materiel_id，通过 获取抖音号绑定所需的资质模版信息接口 获取
     */
    private String materiel_id;

    /**
     * 部分场景，需要在营业执照和执业许可证当中二选一时使用
     */
    private Map<String,AuditTemplateContent> children;

    /**
     * 资质名称，通过 获取抖音号绑定所需的资质模版信息接口 获取
     */
    private String name;

    private String desc;

    /**
     * 资质内容，上传图片时使用上传素材接口获取到的路径
     */
    private List<String> val_list;

    /**
     * 类型
     * Single_Pic=1单张图片
     * Muti_Pic=2多张图片
     * Drop_List=3下拉框
     * Radio=4单选框
     * None=5无表单
     */
    private Integer val_type;

    /**
     * 下拉栏选择项列表
     */
    private List<String> val_example;

    /**
     * 拒绝原因
     */
    private String reject_reason;

    public static class AuditTemplateContentBuilder {
        /**
         * materiel_id，通过 获取抖音号绑定所需的资质模版信息接口 获取
         */
        private String materielId;

        /**
         * 部分场景，需要在营业执照和执业许可证当中二选一时使用
         */
        private Map<String,AuditTemplateContent> children;

        /**
         * 资质名称，通过 获取抖音号绑定所需的资质模版信息接口 获取
         */
        private String name;

        private String desc;

        /**
         * 资质内容，上传图片时使用上传素材接口获取到的路径
         */
        private List<String> valList;

        /**
         * 类型
         * Single_Pic=1单张图片
         * Muti_Pic=2多张图片
         * Drop_List=3下拉框
         * Radio=4单选框
         * None=5无表单
         */
        private Integer valType;

        /**
         * 下拉栏选择项列表
         */
        private List<String> valExample;

        /**
         * 拒绝原因
         */
        private String rejectReason;

        public AuditTemplateContentBuilder materielId(String materielId) {
            this.materielId = materielId;
            return this;
        }

        public AuditTemplateContentBuilder children(Map<String,AuditTemplateContent> children) {
            this.children = children;
            return this;
        }

        public AuditTemplateContentBuilder name(String name) {
            this.name = name;
            return this;
        }

        public AuditTemplateContentBuilder desc(String desc) {
            this.desc = desc;
            return this;
        }

        public AuditTemplateContentBuilder valList(List<String> valList) {
            this.valList = valList;
            return this;
        }

        public AuditTemplateContentBuilder valType(Integer valType) {
            this.valType = valType;
            return this;
        }

        public AuditTemplateContentBuilder valExample(List<String> valExample) {
            this.valExample = valExample;
            return this;
        }

        public AuditTemplateContentBuilder rejectReason(String rejectReason) {
            this.rejectReason = rejectReason;
            return this;
        }

        public AuditTemplateContent build() {
            AuditTemplateContent auditTemplateContent = new AuditTemplateContent();
            auditTemplateContent.setMateriel_id(materielId);
            auditTemplateContent.setChildren(children);
            auditTemplateContent.setName(name);
            auditTemplateContent.setDesc(desc);
            auditTemplateContent.setVal_list(valList);
            auditTemplateContent.setVal_type(valType);
            auditTemplateContent.setVal_example(valExample);
            auditTemplateContent.setReject_reason(rejectReason);
            return auditTemplateContent;
        }
    }

    public static AuditTemplateContentBuilder builder() {
        return new AuditTemplateContentBuilder();
    }

    public String getMateriel_id() {
        return materiel_id;
    }

    public void setMateriel_id(String materiel_id) {
        this.materiel_id = materiel_id;
    }

    public Map<String, AuditTemplateContent> getChildren() {
        return children;
    }

    public void setChildren(Map<String, AuditTemplateContent> children) {
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<String> getVal_list() {
        return val_list;
    }

    public void setVal_list(List<String> val_list) {
        this.val_list = val_list;
    }

    public Integer getVal_type() {
        return val_type;
    }

    public void setVal_type(Integer val_type) {
        this.val_type = val_type;
    }

    public List<String> getVal_example() {
        return val_example;
    }

    public void setVal_example(List<String> val_example) {
        this.val_example = val_example;
    }

    public String getReject_reason() {
        return reject_reason;
    }

    public void setReject_reason(String reject_reason) {
        this.reject_reason = reject_reason;
    }
}
