package com.dyj.applet.domain.vo;

/**
 * 查询分账返回值
 */
public class IndustryQuerySettleVo {

    /**
     * 佣金，单位分
     */
    private Long commission;
    /**
     * 开发者自定义透传字段，长度 <= 2048 字节，不支持二进制数据
     */
    private String cp_extra;
    /**
     * 抖音开平侧交易订单 id，长度 <= 64 字节，由数字、ASCII 字符组成
     */
    private String order_id;
    /**
     * 开发者侧交易订单 id，长度 <= 64 字节，由数字、ASCII 字符组成
     */
    private String out_order_no;
    /**
     * 开发者侧分账单 id，长度 <= 64字节，由数字、ASCII 字符组成
     */
    private String out_settle_no;
    /**
     * 手续费，单位分
     */
    private Long rake;
    /**
     * 分账金额，单位分
     */
    private Long settle_amount;
    /**
     * 分账详情
     */
    private String settle_detail;
    /**
     * 抖音开平侧分账单id，长度 <= 64 字节，由数字、ASCII 字符组成
     */
    private String settle_id;
    /**
     * 分账状态：
     */
    private String settle_status;
    /**
     * 分账时间，13 位时间戳，单位毫秒
     */
    private Long settle_time;

    public Long getCommission() {
        return commission;
    }

    public IndustryQuerySettleVo setCommission(Long commission) {
        this.commission = commission;
        return this;
    }

    public String getCp_extra() {
        return cp_extra;
    }

    public IndustryQuerySettleVo setCp_extra(String cp_extra) {
        this.cp_extra = cp_extra;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public IndustryQuerySettleVo setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getOut_order_no() {
        return out_order_no;
    }

    public IndustryQuerySettleVo setOut_order_no(String out_order_no) {
        this.out_order_no = out_order_no;
        return this;
    }

    public String getOut_settle_no() {
        return out_settle_no;
    }

    public IndustryQuerySettleVo setOut_settle_no(String out_settle_no) {
        this.out_settle_no = out_settle_no;
        return this;
    }

    public Long getRake() {
        return rake;
    }

    public IndustryQuerySettleVo setRake(Long rake) {
        this.rake = rake;
        return this;
    }

    public Long getSettle_amount() {
        return settle_amount;
    }

    public IndustryQuerySettleVo setSettle_amount(Long settle_amount) {
        this.settle_amount = settle_amount;
        return this;
    }

    public String getSettle_detail() {
        return settle_detail;
    }

    public IndustryQuerySettleVo setSettle_detail(String settle_detail) {
        this.settle_detail = settle_detail;
        return this;
    }

    public String getSettle_id() {
        return settle_id;
    }

    public IndustryQuerySettleVo setSettle_id(String settle_id) {
        this.settle_id = settle_id;
        return this;
    }

    public String getSettle_status() {
        return settle_status;
    }

    public IndustryQuerySettleVo setSettle_status(String settle_status) {
        this.settle_status = settle_status;
        return this;
    }

    public Long getSettle_time() {
        return settle_time;
    }

    public IndustryQuerySettleVo setSettle_time(Long settle_time) {
        this.settle_time = settle_time;
        return this;
    }
}
