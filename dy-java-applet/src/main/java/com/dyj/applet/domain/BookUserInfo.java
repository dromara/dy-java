package com.dyj.applet.domain;

/**
 * 预约接口
 * 用户信息
 */
public class BookUserInfo {

    /**
     * 使用人名称
     */
    private String name;

    /**
     * 电话号码
     */
    private String phone;

    /**
     * 身份证号码
     */
    private String id_card_no;

    public String getName() {
        return name;
    }

    public BookUserInfo setName(String name) {
        this.name = name;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public BookUserInfo setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getId_card_no() {
        return id_card_no;
    }

    public BookUserInfo setId_card_no(String id_card_no) {
        this.id_card_no = id_card_no;
        return this;
    }
}
