package com.dyj.applet.domain;

import java.util.List;

public class AnalysisUserSceneData {

    /**
     * 次均停留时长，（单位：毫秒）
     */
    private Double scene_avg_stay_time;

    /**
     * 场景值
     */
    private String scene_id;

    /**
     * 场景值名称
     */
    private String scene_name;

    /**
     * 新增用户数
     */
    private Long scene_new_uv;

    /**
     * 打开次数
     */
    private Long scene_open_pv;

    /**
     * 场景访问次数
     */
    private Long scene_pv;

    /**
     * 活跃用户数
     */
    private Long scene_uv;

    /**
     * 次均停留时长列表详情
     */
    private List<SceneAvgStayTime> scene_avg_stay_time_list;

    /**
     * 新增用户数列表详情
     */
    private List<SceneNewUv> scene_new_uv_list;

    /**
     * 打开次数列表详情
     */
    private List<SceneOpenPv> scene_open_pv_list;

    /**
     * 场景访问次数列表详情
     */
    private List<ScenePv> scene_pv_list;

    /**
     * 活跃用户数列表详情
     */
    private List<SceneUv> scene_uv_list;

    public Double getScene_avg_stay_time() {
        return scene_avg_stay_time;
    }

    public void setScene_avg_stay_time(Double scene_avg_stay_time) {
        this.scene_avg_stay_time = scene_avg_stay_time;
    }

    public String getScene_id() {
        return scene_id;
    }

    public void setScene_id(String scene_id) {
        this.scene_id = scene_id;
    }

    public String getScene_name() {
        return scene_name;
    }

    public void setScene_name(String scene_name) {
        this.scene_name = scene_name;
    }

    public Long getScene_new_uv() {
        return scene_new_uv;
    }

    public void setScene_new_uv(Long scene_new_uv) {
        this.scene_new_uv = scene_new_uv;
    }

    public Long getScene_open_pv() {
        return scene_open_pv;
    }

    public void setScene_open_pv(Long scene_open_pv) {
        this.scene_open_pv = scene_open_pv;
    }

    public Long getScene_pv() {
        return scene_pv;
    }

    public void setScene_pv(Long scene_pv) {
        this.scene_pv = scene_pv;
    }

    public Long getScene_uv() {
        return scene_uv;
    }

    public void setScene_uv(Long scene_uv) {
        this.scene_uv = scene_uv;
    }

    public List<SceneAvgStayTime> getScene_avg_stay_time_list() {
        return scene_avg_stay_time_list;
    }

    public void setScene_avg_stay_time_list(List<SceneAvgStayTime> scene_avg_stay_time_list) {
        this.scene_avg_stay_time_list = scene_avg_stay_time_list;
    }

    public List<SceneNewUv> getScene_new_uv_list() {
        return scene_new_uv_list;
    }

    public void setScene_new_uv_list(List<SceneNewUv> scene_new_uv_list) {
        this.scene_new_uv_list = scene_new_uv_list;
    }

    public List<SceneOpenPv> getScene_open_pv_list() {
        return scene_open_pv_list;
    }

    public void setScene_open_pv_list(List<SceneOpenPv> scene_open_pv_list) {
        this.scene_open_pv_list = scene_open_pv_list;
    }

    public List<ScenePv> getScene_pv_list() {
        return scene_pv_list;
    }

    public void setScene_pv_list(List<ScenePv> scene_pv_list) {
        this.scene_pv_list = scene_pv_list;
    }

    public List<SceneUv> getScene_uv_list() {
        return scene_uv_list;
    }

    public void setScene_uv_list(List<SceneUv> scene_uv_list) {
        this.scene_uv_list = scene_uv_list;
    }

    public static  class SceneAvgStayTime{
        /**
         * 次均停留时长（单位：毫秒）
         */
        private Double scene_avg_stay_time;
        /**
         * 时间，日期格式为“2006-01-02”；小时格式为“15:04:05”
         */
        private String time;
        /**
         * 时间格式；1-日期格式“2006-01-02”；2-小时格式“15:04:05”。end_time与start_time间隔超过24小时，将采用日期格式返回
         */
        private Integer time_type;

        public Double getScene_avg_stay_time() {
            return scene_avg_stay_time;
        }

        public void setScene_avg_stay_time(Double scene_avg_stay_time) {
            this.scene_avg_stay_time = scene_avg_stay_time;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public Integer getTime_type() {
            return time_type;
        }

        public void setTime_type(Integer time_type) {
            this.time_type = time_type;
        }
    }

    public static  class SceneNewUv{
        /**
         * 新增用户数
         */
        private Long scene_new_uv;
        /**
         * 时间，日期格式为“2006-01-02”；小时格式为“15:04:05”
         */
        private String time;
        /**
         * 时间格式；1-日期格式“2006-01-02”；2-小时格式“15:04:05”。end_time与start_time间隔超过24小时，将采用日期格式返回
         */
        private Integer time_type;

        public Long getScene_new_uv() {
            return scene_new_uv;
        }

        public void setScene_new_uv(Long scene_new_uv) {
            this.scene_new_uv = scene_new_uv;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public Integer getTime_type() {
            return time_type;
        }

        public void setTime_type(Integer time_type) {
            this.time_type = time_type;
        }
    }

    public static  class SceneOpenPv{
        /**
         * 打开次数
         */
        private Long scene_open_pv;
        /**
         * 时间，日期格式为“2006-01-02”；小时格式为“15:04:05”
         */
        private String time;
        /**
         * 时间格式；1-日期格式“2006-01-02”；2-小时格式“15:04:05”。end_time与start_time间隔超过24小时，将采用日期格式返回
         */
        private Integer time_type;

        public Long getScene_open_pv() {
            return scene_open_pv;
        }

        public void setScene_open_pv(Long scene_open_pv) {
            this.scene_open_pv = scene_open_pv;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public Integer getTime_type() {
            return time_type;
        }

        public void setTime_type(Integer time_type) {
            this.time_type = time_type;
        }
    }

    public static class ScenePv {
        /**
         *场景访问次数
         */
        private Long scene_pv;
        /**
         * 时间，日期格式为“2006-01-02”；小时格式为“15:04:05”
         */
        private String time;
        /**
         * 时间格式；1-日期格式“2006-01-02”；2-小时格式“15:04:05”。end_time与start_time间隔超过24小时，将采用日期格式返回
         */
        private Integer time_type;

        public Long getScene_pv() {
            return scene_pv;
        }

        public void setScene_pv(Long scene_pv) {
            this.scene_pv = scene_pv;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public Integer getTime_type() {
            return time_type;
        }

        public void setTime_type(Integer time_type) {
            this.time_type = time_type;
        }
    }

    public static class SceneUv {
        /**
         * 活跃用户数
         */
        private Long scene_uv;
        /**
         * 时间，日期格式为“2006-01-02”；小时格式为“15:04:05”
         */
        private String time;
        /**
         * 时间格式；1-日期格式“2006-01-02”；2-小时格式“15:04:05”。end_time与start_time间隔超过24小时，将采用日期格式返回
         */
        private Integer time_type;

        public Long getScene_uv() {
            return scene_uv;
        }

        public void setScene_uv(Long scene_uv) {
            this.scene_uv = scene_uv;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public Integer getTime_type() {
            return time_type;
        }

        public void setTime_type(Integer time_type) {
            this.time_type = time_type;
        }
    }
}
