package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 信用订单查询请求值
 */
public class QueryAuthOrderQuery extends BaseQuery {

    /**
     * <p>进行<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/server/management-capacity/auth-deposit/auth/create-auth-order" target="_blank" rel="nofollow" class="syl-link">信用下单</a>之后，可获取信用单平台订单号（auth_order_id），此处auth_order_id 与 out_auth_order_no 二选一</p> 选填
     */
    private String auth_order_id;
    /**
     * <p>信用单开发者的单号，长度<=64byte，</p><p>auth_order_id 与 out_auth_order_no 二选一</p> 选填
     */
    private String out_auth_order_no;

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public QueryAuthOrderQuery setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public String getOut_auth_order_no() {
        return out_auth_order_no;
    }

    public QueryAuthOrderQuery setOut_auth_order_no(String out_auth_order_no) {
        this.out_auth_order_no = out_auth_order_no;
        return this;
    }

    public static final QueryAuthOrderQueryBuilder builder() {
        return new QueryAuthOrderQueryBuilder();
    }

    public static final class QueryAuthOrderQueryBuilder {
        private String auth_order_id;
        private String out_auth_order_no;
        private Integer tenantId;
        private String clientKey;

        private QueryAuthOrderQueryBuilder() {
        }

        public QueryAuthOrderQueryBuilder authOrderId(String authOrderId) {
            this.auth_order_id = authOrderId;
            return this;
        }

        public QueryAuthOrderQueryBuilder outAuthOrderNo(String outAuthOrderNo) {
            this.out_auth_order_no = outAuthOrderNo;
            return this;
        }

        public QueryAuthOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryAuthOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryAuthOrderQuery build() {
            QueryAuthOrderQuery queryAuthOrderQuery = new QueryAuthOrderQuery();
            queryAuthOrderQuery.setAuth_order_id(auth_order_id);
            queryAuthOrderQuery.setOut_auth_order_no(out_auth_order_no);
            queryAuthOrderQuery.setTenantId(tenantId);
            queryAuthOrderQuery.setClientKey(clientKey);
            return queryAuthOrderQuery;
        }
    }
}
