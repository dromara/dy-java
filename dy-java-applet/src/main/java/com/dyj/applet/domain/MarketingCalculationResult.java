package com.dyj.applet.domain;

import java.util.List;

/**
 * 算价结果
 */
public class MarketingCalculationResult {

    /**
     * 算价类型
     * 1：计算到goods层
     * 2：计算到item层
     */
    private Integer calculation_type;

    /**
     * 订单总价格，单位: 分
     */
    private Long total_amount;

    /**
     * 订单总优惠价格，单位分，订单最终价格 = total_amount - total_discount_amount
     */
    private Long total_discount_amount;

    /**
     * 订单算价结果信息
     */
    private MarketingOrderCalculationResultInfo order_calculation_result_info;

    /**
     * 商品算价结果信息
     */
    private List<MarketingGoodsCalculationResultInfo> goods_calculation_result_info;

    /**
     * 每一份商品算价结果信息 选填
     */
    private List<MarketingItemCalculationResultInfo> item_calculation_result_info;

    public Integer getCalculation_type() {
        return calculation_type;
    }

    public MarketingCalculationResult setCalculation_type(Integer calculation_type) {
        this.calculation_type = calculation_type;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public MarketingCalculationResult setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public Long getTotal_discount_amount() {
        return total_discount_amount;
    }

    public MarketingCalculationResult setTotal_discount_amount(Long total_discount_amount) {
        this.total_discount_amount = total_discount_amount;
        return this;
    }

    public MarketingOrderCalculationResultInfo getOrder_calculation_result_info() {
        return order_calculation_result_info;
    }

    public MarketingCalculationResult setOrder_calculation_result_info(MarketingOrderCalculationResultInfo order_calculation_result_info) {
        this.order_calculation_result_info = order_calculation_result_info;
        return this;
    }

    public List<MarketingGoodsCalculationResultInfo> getGoods_calculation_result_info() {
        return goods_calculation_result_info;
    }

    public MarketingCalculationResult setGoods_calculation_result_info(List<MarketingGoodsCalculationResultInfo> goods_calculation_result_info) {
        this.goods_calculation_result_info = goods_calculation_result_info;
        return this;
    }

    public List<MarketingItemCalculationResultInfo> getItem_calculation_result_info() {
        return item_calculation_result_info;
    }

    public MarketingCalculationResult setItem_calculation_result_info(List<MarketingItemCalculationResultInfo> item_calculation_result_info) {
        this.item_calculation_result_info = item_calculation_result_info;
        return this;
    }
}
