package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.AnalysisLiveRoomData;
import com.dyj.applet.domain.AnalysisLiveRoomOverviewData;

import java.util.List;

public class AnalysisLiveRoomDataVo {

    /**
     * 直播数据列表详情
     */
    private List<AnalysisLiveRoomData> live_data_list;

    /**
     * 直播数据总览
     */
    private List<AnalysisLiveRoomOverviewData> live_data_overview;

    public List<AnalysisLiveRoomData> getLive_data_list() {
        return live_data_list;
    }

    public void setLive_data_list(List<AnalysisLiveRoomData> live_data_list) {
        this.live_data_list = live_data_list;
    }

    public List<AnalysisLiveRoomOverviewData> getLive_data_overview() {
        return live_data_overview;
    }

    public void setLive_data_overview(List<AnalysisLiveRoomOverviewData> live_data_overview) {
        this.live_data_overview = live_data_overview;
    }
}
