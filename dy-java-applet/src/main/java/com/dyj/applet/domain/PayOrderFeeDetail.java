package com.dyj.applet.domain;

public class PayOrderFeeDetail {
    /**
     * <p>扣款项目金额，须>0</p>
     */
    private Long amount;
    /**
     * <p>扣款项目描述，长度<=512字节</p>
     */
    private String description;
    /**
     * <p>扣款项目数量，须>0</p>
     */
    private Long quantity;
    /**
     * <p>扣款项目名称，长度<=256字节</p>
     */
    private String title;

    public Long getAmount() {
        return amount;
    }

    public PayOrderFeeDetail setAmount(Long amount) {
        this.amount = amount;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public PayOrderFeeDetail setDescription(String description) {
        this.description = description;
        return this;
    }

    public Long getQuantity() {
        return quantity;
    }

    public PayOrderFeeDetail setQuantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public PayOrderFeeDetail setTitle(String title) {
        this.title = title;
        return this;
    }
}
