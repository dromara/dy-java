package com.dyj.applet.domain;

import java.util.List;

/**
 * 每个item的预约信息，详见ItemBookInfo
 */
public class ItemBookInfo {

    /**
     * 预约门店的poiId，实际存储的是int64类型的值
     */
    private String poi_id;

    /**
     * 预约门店的名称，参考商铺同步接口中的店铺名称（name）
     */
    private String shop_name;

    /**
     * 预约门店的外部店铺id，参考商铺同步接口中的接入方店铺id（supplier_ext_id）
     */
    private String ext_shop_id;

    /**
     * 商品id
     */
    private String goods_id;

    /**
     * 预约的开始时间（ms），13位毫秒时间戳
     */
    private Long book_start_time;

    /**
     *
     * 预约的结束时间（ms），13位毫秒时间戳
     * 注意：需满足 当前时间< book_start_time < book_end_time，并且book_end_time必须是180天之内
     */
    private Long book_end_time;

    /**
     * 用户信息，详见UserInfo
     * 选填
     */
    private List<BookUserInfo> user_info_list;

    /**
     * 预约的商品SKU信息，需要加价时必填，详见BookSkuInfo
     * •同一个预约单的所有book_sku_info信息需要完全相同
     * 选填
     */
    private BookSkuInfo book_sku_info;

    /**
     * 预售单的item_order_id，如果不指定的话，会自动分配。
     * 选填
     */
    private String item_order_id;

    public String getPoi_id() {
        return poi_id;
    }

    public ItemBookInfo setPoi_id(String poi_id) {
        this.poi_id = poi_id;
        return this;
    }

    public String getShop_name() {
        return shop_name;
    }

    public ItemBookInfo setShop_name(String shop_name) {
        this.shop_name = shop_name;
        return this;
    }

    public String getExt_shop_id() {
        return ext_shop_id;
    }

    public ItemBookInfo setExt_shop_id(String ext_shop_id) {
        this.ext_shop_id = ext_shop_id;
        return this;
    }

    public String getGoods_id() {
        return goods_id;
    }

    public ItemBookInfo setGoods_id(String goods_id) {
        this.goods_id = goods_id;
        return this;
    }

    public Long getBook_start_time() {
        return book_start_time;
    }

    public ItemBookInfo setBook_start_time(Long book_start_time) {
        this.book_start_time = book_start_time;
        return this;
    }

    public Long getBook_end_time() {
        return book_end_time;
    }

    public ItemBookInfo setBook_end_time(Long book_end_time) {
        this.book_end_time = book_end_time;
        return this;
    }

    public List<BookUserInfo> getUser_info_list() {
        return user_info_list;
    }

    public ItemBookInfo setUser_info_list(List<BookUserInfo> user_info_list) {
        this.user_info_list = user_info_list;
        return this;
    }

    public BookSkuInfo getBook_sku_info() {
        return book_sku_info;
    }

    public ItemBookInfo setBook_sku_info(BookSkuInfo book_sku_info) {
        this.book_sku_info = book_sku_info;
        return this;
    }

    public String getItem_order_id() {
        return item_order_id;
    }

    public ItemBookInfo setItem_order_id(String item_order_id) {
        this.item_order_id = item_order_id;
        return this;
    }
}
