package com.dyj.applet.domain.vo;

import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 商户提现结果查询请求值
 */
public class QueryWithdrawOrderQuery extends BaseTransactionMerchantQuery {

    /**
     * <p>小程序的 app_id；除服务商为自己提现外，其他情况必填</p>
     */
    private String app_id;
    /**
     * <p>提现渠道枚举值:</p><ul><li>alipay: 支付宝</li><li>wx: 微信</li><li>hz: 抖音支付</li><li>yzt: 担保支付企业版聚合账户</li></ul>
     */
    private String channel_type;
    /**
     * <p>进件完成返回的商户号</p>
     */
    private String merchant_uid;
    /**
     * <p>外部单号（开发者侧）；唯一标识一笔提现请求</p>
     */
    private String out_order_id;
    /**
     * <p>小程序第三方平台应用 id。服务商发起提现请求的必填</p> 选填
     */
    private String thirdparty_id;

    public String getApp_id() {
        return app_id;
    }

    public QueryWithdrawOrderQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public QueryWithdrawOrderQuery setChannel_type(String channel_type) {
        this.channel_type = channel_type;
        return this;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public QueryWithdrawOrderQuery setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
        return this;
    }

    public String getOut_order_id() {
        return out_order_id;
    }

    public QueryWithdrawOrderQuery setOut_order_id(String out_order_id) {
        this.out_order_id = out_order_id;
        return this;
    }

    public String getThirdparty_id() {
        return thirdparty_id;
    }

    public QueryWithdrawOrderQuery setThirdparty_id(String thirdparty_id) {
        this.thirdparty_id = thirdparty_id;
        return this;
    }

    public static QueryWithdrawOrderQueryBuilder builder() {
        return new QueryWithdrawOrderQueryBuilder();
    }

    public static final class QueryWithdrawOrderQueryBuilder {
        private String app_id;
        private String channel_type;
        private String merchant_uid;
        private String out_order_id;
        private String thirdparty_id;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType;
        private Integer tenantId;
        private String clientKey;

        private QueryWithdrawOrderQueryBuilder() {
        }

        public QueryWithdrawOrderQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public QueryWithdrawOrderQueryBuilder channelType(String channelType) {
            this.channel_type = channelType;
            return this;
        }

        public QueryWithdrawOrderQueryBuilder merchantUid(String merchantUid) {
            this.merchant_uid = merchantUid;
            return this;
        }

        public QueryWithdrawOrderQueryBuilder outOrderId(String outOrderId) {
            this.out_order_id = outOrderId;
            return this;
        }

        public QueryWithdrawOrderQueryBuilder thirdpartyId(String thirdpartyId) {
            this.thirdparty_id = thirdpartyId;
            return this;
        }

        public QueryWithdrawOrderQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public QueryWithdrawOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryWithdrawOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryWithdrawOrderQuery build() {
            QueryWithdrawOrderQuery queryWithdrawOrderQuery = new QueryWithdrawOrderQuery();
            queryWithdrawOrderQuery.setApp_id(app_id);
            queryWithdrawOrderQuery.setChannel_type(channel_type);
            queryWithdrawOrderQuery.setMerchant_uid(merchant_uid);
            queryWithdrawOrderQuery.setOut_order_id(out_order_id);
            queryWithdrawOrderQuery.setThirdparty_id(thirdparty_id);
            queryWithdrawOrderQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            queryWithdrawOrderQuery.setTenantId(tenantId);
            queryWithdrawOrderQuery.setClientKey(clientKey);
            return queryWithdrawOrderQuery;
        }
    }
}
