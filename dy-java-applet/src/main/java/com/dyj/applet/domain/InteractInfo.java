package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-28 14:34
 **/
public class InteractInfo {

    /**
     * 互动数据是否达到目标值
     */
    private Boolean completed;

    /**
     * 代表当前阶段值。互动数值为x，阶段数为stage_count，阶段值的计算方式为：x除以stage_count，并向下取整。
     * eg：视频有51个点赞，stage_count为10，stage为5
     */
    private Integer stage;

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }
}
