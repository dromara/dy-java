package com.dyj.applet.domain.query;

import com.dyj.applet.domain.PathData;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class UpdateMerchantPathQuery extends BaseQuery {

    /**
     * 商家绑定的小程序类型,0-核销工具，1-混合双开
     */
    private Integer bind_biz_type;

    /**
     * 商家Id
     */
    private String account_id;

    /**
     * 要设置的跳转链接列表
     */
    private List<PathData> path_data_list;

    public Integer getBind_biz_type() {
        return bind_biz_type;
    }

    public UpdateMerchantPathQuery setBind_biz_type(Integer bind_biz_type) {
        this.bind_biz_type = bind_biz_type;
        return this;
    }

    public String getAccount_id() {
        return account_id;
    }

    public UpdateMerchantPathQuery setAccount_id(String account_id) {
        this.account_id = account_id;
        return this;
    }

    public List<PathData> getPath_data_list() {
        return path_data_list;
    }

    public UpdateMerchantPathQuery setPath_data_list(List<PathData> path_data_list) {
        this.path_data_list = path_data_list;
        return this;
    }

    public static UpdateMerchantPathQueryBuilder builder() {
        return new UpdateMerchantPathQueryBuilder();
    }

    public static final class UpdateMerchantPathQueryBuilder {
        private Integer bind_biz_type;
        private String account_id;
        private List<PathData> path_data_list;
        private Integer tenantId;
        private String clientKey;

        private UpdateMerchantPathQueryBuilder() {
        }

        public UpdateMerchantPathQueryBuilder bindBizType(Integer bindBizType) {
            this.bind_biz_type = bindBizType;
            return this;
        }

        public UpdateMerchantPathQueryBuilder accountId(String accountId) {
            this.account_id = accountId;
            return this;
        }

        public UpdateMerchantPathQueryBuilder pathDataList(List<PathData> pathDataList) {
            this.path_data_list = pathDataList;
            return this;
        }

        public UpdateMerchantPathQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public UpdateMerchantPathQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public UpdateMerchantPathQuery build() {
            UpdateMerchantPathQuery updateMerchantPathQuery = new UpdateMerchantPathQuery();
            updateMerchantPathQuery.setBind_biz_type(bind_biz_type);
            updateMerchantPathQuery.setAccount_id(account_id);
            updateMerchantPathQuery.setPath_data_list(path_data_list);
            updateMerchantPathQuery.setTenantId(tenantId);
            updateMerchantPathQuery.setClientKey(clientKey);
            return updateMerchantPathQuery;
        }
    }
}
