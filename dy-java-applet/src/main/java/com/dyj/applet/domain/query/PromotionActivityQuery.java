package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class PromotionActivityQuery<T> extends BaseQuery {

    private T promotion_activity;

    public T getPromotion_activity() {
        return promotion_activity;
    }

    public PromotionActivityQuery<T> setPromotion_activity(T promotion_activity) {
        this.promotion_activity = promotion_activity;
        return this;
    }

    public static <T> promotionActivityQueryBuilder<T> builder(){
        return new promotionActivityQueryBuilder<T>();
    }


    public static final class promotionActivityQueryBuilder <T> {
        private T promotion_activity;
        private Integer tenantId;
        private String clientKey;

        private promotionActivityQueryBuilder() {
        }

        public promotionActivityQueryBuilder<T> promotionActivity(T promotion_activity) {
            this.promotion_activity = promotion_activity;
            return this;
        }

        public promotionActivityQueryBuilder<T> tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public promotionActivityQueryBuilder<T> clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public PromotionActivityQuery<T> build() {
            PromotionActivityQuery<T> promotionActivityQuery = new PromotionActivityQuery<T>();
            promotionActivityQuery.setPromotion_activity(promotion_activity);
            promotionActivityQuery.setTenantId(tenantId);
            promotionActivityQuery.setClientKey(clientKey);
            return promotionActivityQuery;
        }
    }
}
