package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class ClosePayOrderQuery extends BaseQuery {

    /**
     * 调用发起扣款接口后，会得到扣款单平台订单号（pay_order_id），此处关闭扣款的接口需要传递该订单号，用来关闭对应的订单。
     */
    private String pay_order_id;

    public String getPay_order_id() {
        return pay_order_id;
    }

    public ClosePayOrderQuery setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public static ClosePayOrderQueryBuilder builder() {
        return new ClosePayOrderQueryBuilder();
    }

    public static final class ClosePayOrderQueryBuilder {
        private String pay_order_id;
        private Integer tenantId;
        private String clientKey;

        private ClosePayOrderQueryBuilder() {
        }

        public ClosePayOrderQueryBuilder payOrderId(String payOrderId) {
            this.pay_order_id = payOrderId;
            return this;
        }

        public ClosePayOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public ClosePayOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public ClosePayOrderQuery build() {
            ClosePayOrderQuery closePayOrderQuery = new ClosePayOrderQuery();
            closePayOrderQuery.setPay_order_id(pay_order_id);
            closePayOrderQuery.setTenantId(tenantId);
            closePayOrderQuery.setClientKey(clientKey);
            return closePayOrderQuery;
        }
    }
}
