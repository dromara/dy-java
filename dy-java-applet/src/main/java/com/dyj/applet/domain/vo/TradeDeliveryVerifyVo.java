package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.DeliveryVerifyVerifyResult;
import com.dyj.applet.domain.TradeDeliveryVerifyVerifyResult;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * 生活服务交易系统->核销->抖音码->验券
 */
public class TradeDeliveryVerifyVo {

    /**
     * 数组，每个券的验券结果
     */
    private List<TradeDeliveryVerifyVerifyResult> verify_results;

    public List<TradeDeliveryVerifyVerifyResult> getVerify_results() {
        return verify_results;
    }

    public TradeDeliveryVerifyVo setVerify_results(List<TradeDeliveryVerifyVerifyResult> verify_results) {
        this.verify_results = verify_results;
        return this;
    }
}
