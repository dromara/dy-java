package com.dyj.applet.domain;

import java.util.List;

/**
 * 生活服务交易系统->营销算价->查询营销算价data
 */
public class QueryAndCalculateMarketingResult {

    /**
     * 商品维度营销查询结果，包含商品信息以及该商品下可用/不可用营销信息
     */
    private List<MarketingGoodsMarketingResult> goods_marketing_result;

    /**
     * 订单维度营销查询结果，包含订单总价以及订单维度可用/不可用营销 选填
     */
    private MarketingOrderMarketingResult order_marketing_result;

    /**
     * 算价结果 选填
     */
    private MarketingCalculationResult calculation_result;

    public List<MarketingGoodsMarketingResult> getGoods_marketing_result() {
        return goods_marketing_result;
    }

    public QueryAndCalculateMarketingResult setGoods_marketing_result(List<MarketingGoodsMarketingResult> goods_marketing_result) {
        this.goods_marketing_result = goods_marketing_result;
        return this;
    }

    public MarketingOrderMarketingResult getOrder_marketing_result() {
        return order_marketing_result;
    }

    public QueryAndCalculateMarketingResult setOrder_marketing_result(MarketingOrderMarketingResult order_marketing_result) {
        this.order_marketing_result = order_marketing_result;
        return this;
    }

    public MarketingCalculationResult getCalculation_result() {
        return calculation_result;
    }

    public QueryAndCalculateMarketingResult setCalculation_result(MarketingCalculationResult calculation_result) {
        this.calculation_result = calculation_result;
        return this;
    }
}
