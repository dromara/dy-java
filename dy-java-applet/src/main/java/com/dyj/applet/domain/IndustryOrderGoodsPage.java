package com.dyj.applet.domain;

/**
 * 商品详情页
 */
public class IndustryOrderGoodsPage {

    /**
     * 订单详情页跳转路径，没有前导的“/”，长度 <= 512byte
     */
    private String path;

    /**
     * 订单详情页路径参数，自定义的 json 结构，
     * 序列化成字符串存入该字段，平台不限制，
     * 但是写入的内容需要能够保证生成访问订单详情的 schema 能正确跳转到小程序内部的订单详情页，
     * 长度 <= 512byte
     * 选填
     */
    private String params;

    public String getPath() {
        return path;
    }

    public IndustryOrderGoodsPage setPath(String path) {
        this.path = path;
        return this;
    }

    public String getParams() {
        return params;
    }

    public IndustryOrderGoodsPage setParams(String params) {
        this.params = params;
        return this;
    }
}
