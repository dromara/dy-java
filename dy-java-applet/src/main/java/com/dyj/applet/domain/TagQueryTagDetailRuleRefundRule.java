package com.dyj.applet.domain;

/**
 * 标签组对应规则信息退款规则
 */
public class TagQueryTagDetailRuleRefundRule {


    /**
     * <p>业务状态，枚举值：</p><p>"FULFILL_WAIT" ：待履约</p><p>"FULFILLING" ：履约中</p><p>"FULFILL_DONE" ：履约完成</p><p>"PAY_SUCCESS"：支付成功</p>
     */
    private String biz_status;
    /**
     * <p>是否需要商家审核，枚举值:</p><p>1：需要审核；</p><p>2：无需审核；</p>
     */
    private Integer merchant_audit_type;
    /**
     * <p>退款类型，枚举值：</p><p>1：不可退；</p><p>2：协商退;</p><p>3：全额退;</p>
     */
    private Integer refund_type;

    public String getBiz_status() {
        return biz_status;
    }

    public TagQueryTagDetailRuleRefundRule setBiz_status(String biz_status) {
        this.biz_status = biz_status;
        return this;
    }

    public Integer getMerchant_audit_type() {
        return merchant_audit_type;
    }

    public TagQueryTagDetailRuleRefundRule setMerchant_audit_type(Integer merchant_audit_type) {
        this.merchant_audit_type = merchant_audit_type;
        return this;
    }

    public Integer getRefund_type() {
        return refund_type;
    }

    public TagQueryTagDetailRuleRefundRule setRefund_type(Integer refund_type) {
        this.refund_type = refund_type;
        return this;
    }
}
