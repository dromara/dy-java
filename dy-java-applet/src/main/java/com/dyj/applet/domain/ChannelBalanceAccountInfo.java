package com.dyj.applet.domain;

/**
 * 商户余额余额信息
 */
public class ChannelBalanceAccountInfo {

    /**
     * <p>冻结准备金余额；CNY、单位分</p>
     */
    private Long freeze_balance;
    /**
     * <p>在途余额；CNY、单位分</p>
     */
    private Long online_balance;
    /**
     * <p>可提现余额；CNY、单位分</p>
     */
    private Long withdrawable_balacne;

    public Long getFreeze_balance() {
        return freeze_balance;
    }

    public ChannelBalanceAccountInfo setFreeze_balance(Long freeze_balance) {
        this.freeze_balance = freeze_balance;
        return this;
    }

    public Long getOnline_balance() {
        return online_balance;
    }

    public ChannelBalanceAccountInfo setOnline_balance(Long online_balance) {
        this.online_balance = online_balance;
        return this;
    }

    public Long getWithdrawable_balacne() {
        return withdrawable_balacne;
    }

    public ChannelBalanceAccountInfo setWithdrawable_balacne(Long withdrawable_balacne) {
        this.withdrawable_balacne = withdrawable_balacne;
        return this;
    }
}
