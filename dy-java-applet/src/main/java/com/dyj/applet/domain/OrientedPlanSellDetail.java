package com.dyj.applet.domain;

public class OrientedPlanSellDetail {

    /**
     * 计划的带货GMV，单位分
     */
    private Long gmv;

    /**
     * 计划关联的投稿总数（包括短视频和直播间）
     */
    private Integer media_cnt;

    /**
     * 计划的已核销GMV，单位分
     */
    private Long used_gmv;

    private OrientedPlanTalent plan_info;
}
