package com.dyj.applet.domain;

/**
 * 次卡信息
 */
public class TimesCardInfo {

    /**
     * 总次数
     */
    private Long total_times;

    /**
     * 可用次数
     */
    private Long usable_times;

    public Long getTotal_times() {
        return total_times;
    }

    public TimesCardInfo setTotal_times(Long total_times) {
        this.total_times = total_times;
        return this;
    }

    public Long getUsable_times() {
        return usable_times;
    }

    public TimesCardInfo setUsable_times(Long usable_times) {
        this.usable_times = usable_times;
        return this;
    }
}
