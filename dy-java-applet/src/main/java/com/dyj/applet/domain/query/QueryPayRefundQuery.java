package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class QueryPayRefundQuery extends BaseQuery {

    /**
     * <p>退款单开发者侧退款单号，长度<=64byte</p><ul><li><span style="color: #1C1F23;"><span style="font-size: 14px;">pay_refund_id和</span></span>out_pay_refund_no两个参数必须要有一个</li></ul> 选填
     */
    private String out_pay_refund_no;
    /**
     * <p>退款单平台订单号，长度<=64byte</p><ul><li><span style="color: #1C1F23;"><span style="background-color: #2E3238C;"><span style="font-size: 14px;">pay_refund_id和</span></span></span>out_pay_refund_no两个参数必须要有一个</li></ul> 选填
     */
    private String pay_refund_id;

    public String getOut_pay_refund_no() {
        return out_pay_refund_no;
    }

    public QueryPayRefundQuery setOut_pay_refund_no(String out_pay_refund_no) {
        this.out_pay_refund_no = out_pay_refund_no;
        return this;
    }

    public String getPay_refund_id() {
        return pay_refund_id;
    }

    public QueryPayRefundQuery setPay_refund_id(String pay_refund_id) {
        this.pay_refund_id = pay_refund_id;
        return this;
    }

    public static QueryPayRefundQueryBuilder builder() {
        return new QueryPayRefundQueryBuilder();
    }

    public static final class QueryPayRefundQueryBuilder {
        private String out_pay_refund_no;
        private String pay_refund_id;
        private Integer tenantId;
        private String clientKey;

        private QueryPayRefundQueryBuilder() {
        }

        public QueryPayRefundQueryBuilder outPayRefundNo(String outPayRefundNo) {
            this.out_pay_refund_no = outPayRefundNo;
            return this;
        }

        public QueryPayRefundQueryBuilder payRefundId(String payRefundId) {
            this.pay_refund_id = payRefundId;
            return this;
        }

        public QueryPayRefundQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryPayRefundQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryPayRefundQuery build() {
            QueryPayRefundQuery queryPayRefundQuery = new QueryPayRefundQuery();
            queryPayRefundQuery.setOut_pay_refund_no(out_pay_refund_no);
            queryPayRefundQuery.setPay_refund_id(pay_refund_id);
            queryPayRefundQuery.setTenantId(tenantId);
            queryPayRefundQuery.setClientKey(clientKey);
            return queryPayRefundQuery;
        }
    }
}
