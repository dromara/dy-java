package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.PayOrderFeeDetail;

import java.util.List;

public class QueryPayOrderVo {

    /**
     * <p>小程序 app_id</p>
     */
    private String app_id;
    /**
     * <p>信用单平台订单号，长度<=64byte</p>
     */
    private String auth_order_id;
    /**
     * <p>渠道支付单</p> 选填
     */
    private String channel_pay_id;
    /**
     * <p>开发者自定义收款商户号</p> 选填
     */
    private String merchant_uid;
    /**
     * <p>下单结果回调地址，https开头</p> 选填
     */
    private String notify_url;
    /**
     * <p>扣款单开发者的单号，长度<=64byte</p>
     */
    private String out_pay_order_no;
    /**
     * <p>支付渠道枚举（<strong>扣款成功时才有</strong>）：</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">1：微信</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">2：支付宝</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">3：银行卡</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">10：抖音支付</li></ul> 选填
     */
    private Integer pay_channel;
    /**
     * <p>扣款单平台订单号，长度<=64byte</p>
     */
    private String pay_order_id;
    /**
     * <p>支付成功时间，时间单位为毫秒</p> 选填
     */
    private Long pay_time;
    /**
     * <p>扣款单状态</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">"SUCCESS" （扣款成功）</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">"PROCESSING" (订单处理中)</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">"FAIL" (扣款失败)</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">"CLOSED" (订单已关单)</li></ul>
     */
    private String status;
    /**
     * <p>扣款金额，总的扣款金额不能超过押金</p>
     */
    private Long total_amount;
    /**
     *
     */
    private List<PayOrderFeeDetail> fee_detail_list;

    public String getApp_id() {
        return app_id;
    }

    public QueryPayOrderVo setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getAuth_order_id() {
        return auth_order_id;
    }

    public QueryPayOrderVo setAuth_order_id(String auth_order_id) {
        this.auth_order_id = auth_order_id;
        return this;
    }

    public String getChannel_pay_id() {
        return channel_pay_id;
    }

    public QueryPayOrderVo setChannel_pay_id(String channel_pay_id) {
        this.channel_pay_id = channel_pay_id;
        return this;
    }

    public String getMerchant_uid() {
        return merchant_uid;
    }

    public QueryPayOrderVo setMerchant_uid(String merchant_uid) {
        this.merchant_uid = merchant_uid;
        return this;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public QueryPayOrderVo setNotify_url(String notify_url) {
        this.notify_url = notify_url;
        return this;
    }

    public String getOut_pay_order_no() {
        return out_pay_order_no;
    }

    public QueryPayOrderVo setOut_pay_order_no(String out_pay_order_no) {
        this.out_pay_order_no = out_pay_order_no;
        return this;
    }

    public Integer getPay_channel() {
        return pay_channel;
    }

    public QueryPayOrderVo setPay_channel(Integer pay_channel) {
        this.pay_channel = pay_channel;
        return this;
    }

    public String getPay_order_id() {
        return pay_order_id;
    }

    public QueryPayOrderVo setPay_order_id(String pay_order_id) {
        this.pay_order_id = pay_order_id;
        return this;
    }

    public Long getPay_time() {
        return pay_time;
    }

    public QueryPayOrderVo setPay_time(Long pay_time) {
        this.pay_time = pay_time;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public QueryPayOrderVo setStatus(String status) {
        this.status = status;
        return this;
    }

    public Long getTotal_amount() {
        return total_amount;
    }

    public QueryPayOrderVo setTotal_amount(Long total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    public List<PayOrderFeeDetail> getFee_detail_list() {
        return fee_detail_list;
    }

    public QueryPayOrderVo setFee_detail_list(List<PayOrderFeeDetail> fee_detail_list) {
        this.fee_detail_list = fee_detail_list;
        return this;
    }
}
