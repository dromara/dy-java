package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

public class TagQueryQuery extends BaseQuery {

    /**
     * 商品类型，枚举值:
     * 101: "号卡商品",
     * 102: "通信定制类商品(彩铃)",
     * 103: "话费/宽带充值类商品",
     * 201: "通用咨询类商品",
     * 202: "代写文书",
     * 301: "虚拟工具类商品",
     * 401: "内容消费类商品"
     */
    private Integer goods_type;

    public Integer getGoods_type() {
        return goods_type;
    }

    public TagQueryQuery setGoods_type(Integer goods_type) {
        this.goods_type = goods_type;
        return this;
    }

    public static TagQueryQueryBuilder builder() {
        return new TagQueryQueryBuilder();
    }

    public static final class TagQueryQueryBuilder {
        private Integer goods_type;
        private Integer tenantId;
        private String clientKey;

        private TagQueryQueryBuilder() {
        }

        public TagQueryQueryBuilder goodsType(Integer goodsType) {
            this.goods_type = goodsType;
            return this;
        }

        public TagQueryQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public TagQueryQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public TagQueryQuery build() {
            TagQueryQuery tagQueryQuery = new TagQueryQuery();
            tagQueryQuery.setGoods_type(goods_type);
            tagQueryQuery.setTenantId(tenantId);
            tagQueryQuery.setClientKey(clientKey);
            return tagQueryQuery;
        }
    }
}
