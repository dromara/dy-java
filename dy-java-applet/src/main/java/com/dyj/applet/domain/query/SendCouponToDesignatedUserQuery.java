package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

/**
 * 开发者接口发券请求值
 */
public class SendCouponToDesignatedUserQuery extends UserInfoQuery {


    /**
     * 活动id
     */
    private String activity_id;
    /**
     * appId
     */
    private String app_id;
    /**
     *  选填 接口发券幂等键，给单用户发放多张券时必传
     */
    private String merchant_coupon_id;

    public String getActivity_id() {
        return activity_id;
    }

    public SendCouponToDesignatedUserQuery setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public SendCouponToDesignatedUserQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getMerchant_coupon_id() {
        return merchant_coupon_id;
    }

    public SendCouponToDesignatedUserQuery setMerchant_coupon_id(String merchant_coupon_id) {
        this.merchant_coupon_id = merchant_coupon_id;
        return this;
    }

    public static SendCouponToDesignatedUserQueryBuilder builder(){
        return new SendCouponToDesignatedUserQueryBuilder();
    }

    public static final class SendCouponToDesignatedUserQueryBuilder {
        private String activity_id;
        private String app_id;
        private String merchant_coupon_id;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private SendCouponToDesignatedUserQueryBuilder() {
        }

        public SendCouponToDesignatedUserQueryBuilder activityId(String activityId) {
            this.activity_id = activityId;
            return this;
        }

        public SendCouponToDesignatedUserQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public SendCouponToDesignatedUserQueryBuilder merchantCouponId(String merchantCouponId) {
            this.merchant_coupon_id = merchantCouponId;
            return this;
        }

        public SendCouponToDesignatedUserQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public SendCouponToDesignatedUserQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public SendCouponToDesignatedUserQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SendCouponToDesignatedUserQuery build() {
            SendCouponToDesignatedUserQuery sendCouponToDesignatedUserQuery = new SendCouponToDesignatedUserQuery();
            sendCouponToDesignatedUserQuery.setActivity_id(activity_id);
            sendCouponToDesignatedUserQuery.setApp_id(app_id);
            sendCouponToDesignatedUserQuery.setMerchant_coupon_id(merchant_coupon_id);
            sendCouponToDesignatedUserQuery.setOpen_id(open_id);
            sendCouponToDesignatedUserQuery.setTenantId(tenantId);
            sendCouponToDesignatedUserQuery.setClientKey(clientKey);
            return sendCouponToDesignatedUserQuery;
        }
    }
}
