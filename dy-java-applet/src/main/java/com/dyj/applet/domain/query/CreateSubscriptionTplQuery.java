package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class CreateSubscriptionTplQuery extends BaseQuery {

    /**
     * <p><span style="color: #171A1C;">服务类目id，多个id用英文逗号隔开。可以通过</span><a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/server/category/query-app-categories" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">获取已设置的服务类目接口</a><span style="color: #171A1C;">获取小程序的服务类目，只能是已经审核通过的服务类目</span></p>
     */
    private String category_ids;
    /**
     * 订阅消息类型
     */
    private Integer classification;
    /**
     * 模板标题，2~8 个字符限制
     */
    private String title;
    /**
     * 模板适用的宿主APP列表，douyin-抖音；douyin_lite-抖音lite；toutiao-今日头条
     */
    private List<String> host_list;
    /**
     * 关键词列表，最多添加10个关键词，每个关键词2-8个字符
     */
    private List<String> keyword_list;

    public String getCategory_ids() {
        return category_ids;
    }

    public CreateSubscriptionTplQuery setCategory_ids(String category_ids) {
        this.category_ids = category_ids;
        return this;
    }

    public Integer getClassification() {
        return classification;
    }

    public CreateSubscriptionTplQuery setClassification(Integer classification) {
        this.classification = classification;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public CreateSubscriptionTplQuery setTitle(String title) {
        this.title = title;
        return this;
    }

    public List<String> getHost_list() {
        return host_list;
    }

    public CreateSubscriptionTplQuery setHost_list(List<String> host_list) {
        this.host_list = host_list;
        return this;
    }

    public List<String> getKeyword_list() {
        return keyword_list;
    }

    public CreateSubscriptionTplQuery setKeyword_list(List<String> keyword_list) {
        this.keyword_list = keyword_list;
        return this;
    }

    public static CreateSubscriptionTplQueryBuilder builder() {
        return new CreateSubscriptionTplQueryBuilder();
    }

    public static final class CreateSubscriptionTplQueryBuilder {
        private String category_ids;
        private Integer classification;
        private String title;
        private List<String> host_list;
        private List<String> keyword_list;
        private Integer tenantId;
        private String clientKey;

        private CreateSubscriptionTplQueryBuilder() {
        }

        public CreateSubscriptionTplQueryBuilder categoryIds(String categoryIds) {
            this.category_ids = categoryIds;
            return this;
        }

        public CreateSubscriptionTplQueryBuilder classification(Integer classification) {
            this.classification = classification;
            return this;
        }

        public CreateSubscriptionTplQueryBuilder title(String title) {
            this.title = title;
            return this;
        }

        public CreateSubscriptionTplQueryBuilder hostList(List<String> hostList) {
            this.host_list = hostList;
            return this;
        }

        public CreateSubscriptionTplQueryBuilder keywordList(List<String> keywordList) {
            this.keyword_list = keywordList;
            return this;
        }

        public CreateSubscriptionTplQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateSubscriptionTplQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CreateSubscriptionTplQuery build() {
            CreateSubscriptionTplQuery createSubscriptionTplQuery = new CreateSubscriptionTplQuery();
            createSubscriptionTplQuery.setCategory_ids(category_ids);
            createSubscriptionTplQuery.setClassification(classification);
            createSubscriptionTplQuery.setTitle(title);
            createSubscriptionTplQuery.setHost_list(host_list);
            createSubscriptionTplQuery.setKeyword_list(keyword_list);
            createSubscriptionTplQuery.setTenantId(tenantId);
            createSubscriptionTplQuery.setClientKey(clientKey);
            return createSubscriptionTplQuery;
        }
    }
}
