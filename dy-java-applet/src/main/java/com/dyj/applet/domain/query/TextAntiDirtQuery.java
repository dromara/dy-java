package com.dyj.applet.domain.query;

import com.dyj.applet.domain.TextAntiDirtContent;
import com.dyj.common.domain.query.BaseQuery;

import java.util.List;

public class TextAntiDirtQuery extends BaseQuery {

    /**
     * 检测任务列表
     */
    private List<TextAntiDirtContent> tasks;

    public List<TextAntiDirtContent> getTasks() {
        return tasks;
    }

    public TextAntiDirtQuery setTasks(List<TextAntiDirtContent> tasks) {
        this.tasks = tasks;
        return this;
    }

    public static TextAntiDirtQueryBuilder builder() {
        return new TextAntiDirtQueryBuilder();
    }

    public static final class TextAntiDirtQueryBuilder {
        private List<TextAntiDirtContent> tasks;
        private Integer tenantId;
        private String clientKey;

        public static TextAntiDirtQueryBuilder aTextAntiDirtQuery() {
            return new TextAntiDirtQueryBuilder();
        }

        public TextAntiDirtQueryBuilder tasks(List<TextAntiDirtContent> tasks) {
            this.tasks = tasks;
            return this;
        }

        public TextAntiDirtQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public TextAntiDirtQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public TextAntiDirtQuery build() {
            TextAntiDirtQuery textAntiDirtQuery = new TextAntiDirtQuery();
            textAntiDirtQuery.setTasks(tasks);
            textAntiDirtQuery.setTenantId(tenantId);
            textAntiDirtQuery.setClientKey(clientKey);
            return textAntiDirtQuery;
        }
    }
}
